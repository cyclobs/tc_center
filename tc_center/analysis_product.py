import logging

import geo_shapely
import xarray as xr
import numpy as np
from shapely.wkt import loads
import pandas as pd
import numpy.ma as ma
from tc_center.fix import get_vmax
from tc_center.polar import to_polar, to_north_ref, to_aeqd, find_nearest_2d, \
    to_cylone_dir, to_cyclone_cartesian, rotate_dataset
from tc_center.functions import mask_outlier_points, spd2cat, api_req, shape_from_array, circle_poly_from_rad
from tc_center.quality import quality_flags
from cyclobs_utils import rmax_ck22, km_to_nmi, ms_to_knots, knots_to_ms, cyclone_speed_dir_angle_to_north
from scipy.optimize import curve_fit
from scipy.signal import find_peaks
from scipy.ndimage import label, generate_binary_structure, binary_dilation
from scipy import ndimage
import owi
from shapely.geometry import LineString, Point, MultiPoint
import os
from ._version import __version__

version = __version__

logger = logging.getLogger(__name__)


def radius_for_wind(polar_ds, wind_mean, wind_knots, idx_max):
    """
    Finds inside a polar projected dataset the radius at which the mean of the wind speed equals wind_knots.
    The index of the found radius will always be superior to idx_max.
    If the number of valid points to compute the mean is below 75% then np.nan is returned instead of the radius
    and its associated wind speed.

    Parameters
    ----------
        polar_ds: xarray.Dataset Dataset containing SAR data in polar projection.
        wind_mean: xarray.DataArray wind_speed variable meaned along theta axis in m/s.
        wind_knots: int or float of the wind for which to find the radius. In knots
        idx_max: int index above which the found radius index must be.

    Returns
    -------
    tuple
        the wind_speed (m/s) at the found radius (m) and the corresponding radius.
    """
    wind_ms = wind_knots * 0.514444

    wind_masked = np.ma.asarray(wind_mean.values)
    wind_masked[:idx_max] = np.ma.masked
    idx = np.nanargmin(np.abs(wind_masked - wind_ms))

    logger.debug(f"Target wind m/s: {wind_ms}, found wind value : {wind_mean.isel(rad=idx).item()}")

    count_total = polar_ds.isel(rad=idx).wind_speed.values.shape[0]
    wind_nan_count = np.sum(np.where(np.isnan(polar_ds.isel(rad=idx).wind_speed.values), 1, 0))
    percent_invalid = wind_nan_count / count_total * 100

    logger.debug(f"Total count points at {polar_ds.isel(rad=idx)['rad'].item()}m : {count_total}. "
                 f"Number of invalid point at same distance : {wind_nan_count}. "
                 f"Percent invalid : {percent_invalid}")

    # if percent_invalid > 75:
    #    return np.nan, np.nan

    return int(wind_mean.isel(rad=idx).item()), int(wind_mean.isel(rad=idx)["rad"].item()), 100 - percent_invalid


def compute_radii(ds_polar_north, tc_oriented=False, eye_ok=True):
    """
        Finds radii for each quadrant (all, NE, SE, SW, NW) and for 64, 50 and 34 wind speed (in knots).

        Parameters
        ----------
            ds_polar_north: xarray.Dataset SAR dataset in polar projection.

        Returns
        -------
            tuple(dict(quadrant: list(vmax, rmax, rad64, rad50, rad34)), dict(quadrant: list(vmax, rmax, rad64, rad50, rad34)))
                radii data in meters and nmi
    """
    ds_polar_north = ds_polar_north.squeeze().copy()
    # Compute and add radii to xarray dataset
    if tc_oriented:
        quadrants = {"all": slice(0, 360), "FL": slice(0, 90), "RL": slice(90, 180), "RR": slice(180, 270),
                     "FR": slice(270, 360)}
    else:
        quadrants = {"all": slice(0, 360), "NW": slice(0, 90), "SW": slice(90, 180), "SE": slice(180, 270),
                     "NE": slice(270, 360)}
    radii_quad = {}
    radii_nmi_quad = {}
    percent_valid_quad = {}
    for quad, sl in quadrants.items():
        logger.debug(f"Current quad {quad}")
        # Select the data in the current quadrant.
        theta = ds_polar_north.coords["theta"].values
        theta_ok = theta[(theta > sl.start) & (theta < sl.stop)]
        # Cannot use ds_polar_north.sel(theta=sl) because of xarray error probably linked to the fact that the
        # dataset contains a datetime dimension. The error is : "KeyError: 90"
        polar_quad = ds_polar_north.sel(theta=theta_ok)

        # Set data to nan where mask indicates other than valid data.
        # invalid_x, invalid_y = np.where(polar_quad["mask_flag"].values > 0)

        # wind_masked = polar_quad["wind_speed"]
        wind_masked = xr.where(polar_quad["mask_flag"] > 0, np.nan, polar_quad["wind_speed"])

        # Mean the wind_speed.
        wind_mean = wind_masked.mean(dim="theta")

        if eye_ok:
            # Find the vmax and rmax
            # The mean array contains only nan values. Mean cannot be computed.
            if np.count_nonzero(np.isnan(wind_mean.values)) == wind_mean.values.shape[0]:
                vmax = -1
                vmax_kts = -1
                rmax = -1
                rmax_nmi = -1
                percent_valid = -1
                logger.info(f"For quad {quad}, wind mean contains only nans. No radii will be computed.")
            else:
                vmax = wind_mean.max(dim="rad", skipna=True).item()
                vmax_kts = vmax * 1.94384
                idx_max = np.where(wind_mean.values == vmax)[0][0]
                rmax = wind_mean.isel(rad=idx_max)["rad"].item()
                rmax_nmi = rmax * 0.000539957
                logger.debug(f"Vmax: {vmax} idx_max: {idx_max} rmax: {rmax} "
                             f"Rad shape: {polar_quad.coords['rad'].values.shape}")

                # Percent invalid for rmax
                count_total = polar_quad.isel(rad=idx_max).wind_speed.values.shape[0]
                wind_nan_count = np.sum(np.where(np.isnan(polar_quad.isel(rad=idx_max).wind_speed.values), 1, 0))
                percent_valid = 100 - (wind_nan_count / count_total * 100)

            # Loops to find the radii and store them into a dict containing lists.
            wind_vals = [64, 50, 34]
            radii = [vmax, rmax]
            radii_nmi = [vmax_kts, rmax_nmi]
            percent_valid_radii = [percent_valid, percent_valid]
            for w in wind_vals:
                if vmax_kts < w or vmax_kts == -1:
                    wind_r = -1
                    radius_w = -1
                    radius_nmi = -1
                    percent_valid = -1
                else:
                    wind_r, radius_w, percent_valid = radius_for_wind(polar_quad, wind_mean, w, idx_max)
                    radius_nmi = radius_w * 0.000539957
                radii.append(radius_w)
                radii_nmi.append(radius_nmi)
                percent_valid_radii.append(percent_valid)
            radii_quad[quad] = radii
            radii_nmi_quad[quad] = radii_nmi
            percent_valid_quad[quad] = percent_valid_radii
        else:
            radii_quad[quad] = [-1, -1, -1, -1, -1]
            radii_nmi_quad[quad] = [-1, -1, -1, -1, -1]
            percent_valid_quad[quad] = [-1, -1, -1, -1, -1]

    logger.debug(f"Radii_ms: {radii_quad}")
    logger.debug(f"Radii_nmi: {radii_nmi_quad}")
    logger.debug(f"Percent valid: {percent_valid_quad}")

    return radii_quad, radii_nmi_quad, percent_valid_quad


def recursive_vmax_window(ds: xr.Dataset, range_list: list, rmax_ck22: float, rmaxs: list, vmaxs: list,
                          vmax_range_dict: dict = None, rmax_range_dict: dict = None,
                          window_count_range_dict: dict = None, std_range_dict: dict = None,
                          t_start=0, t_stop=360):
    """
    Recursive function that computes vmax and rmax for different window size. Rmax computed using a previous window
    size are used in the next window (which is expected to be smaller) as a reference to select the right vmax peak.

    The amount of recursion is defined using range_list parameter. Range_list is a list of int defining the window size
    to use at each recursive_vmax_window iteration. The function exits when all window size have been iterated.
    A final vmax and rmax are then computed using the last rmaxs and vmaxs list obtained using the last window size.

    Parameters
    ----------
    ds: xarray.Dataset the dataset (polar form) to compute the vmax on
    range_list: list of int defining the window size to use at each recursive_vmax_window iteration.
    rmax_ck22: float rmax_ck22 in meters obtained from atcf or ibtracs rmax.
    rmaxs: list of int Rmax reference that will be use to chose the vmax peak. For calling this function, it can typically
        be the RMAX computed on the global meaned profile.
    vmaxs: list of int Vmax list that will be used internally in the recursion process. You still must give a list with
        the same size as rmaxs parameter
    vmax_range_dict: list Used internally for recursivity : the list grows at each iteration step and contains the
        percentiled reduced vmax value for each window range (corresponding to range_list)
    rmax_range_dict: list Used internally for recursivity : the list grows at each iteration step and contains the
        percentiled reduced rmax value for each window range (corresponding to range_list)
    t_start: int Defines at which theta must start each iteration on the polar product. Useful to work only on a subset
        of the polar product. Values possible between 0 and 358.
    t_stop: int Defines at which theta must stop each iteration on the polar product. Useful to work only on a subset
        of the polar product. Values possible between 0 and 360. Must be above t_start parameter

    Returns
    -------
    vmax, rmax
        float: vmax in (m/s), float: rmax in meters
    """
    if vmax_range_dict is None or rmax_range_dict is None or window_count_range_dict is None or std_range_dict:
        vmax_range_dict = {}
        rmax_range_dict = {}
        window_count_range_dict = {}
        std_range_dict = {}

    if len(range_list) > 0:
        t_range = range_list.pop(0)
        new_rmaxs = []
        new_vmaxs = []

        i = 0
        # Take a theta window and slide it of 1° for each iteration
        for t in range(t_start, t_stop):
            if np.isnan(vmaxs[i]):
                logger.debug(f"Theta {t} is skipped because vmax is nan")
                new_rmaxs.append(np.nan)
                new_vmaxs.append(np.nan)
                i += 1
                continue
            s1 = (t - t_range / 2) % 360
            s2 = (t + t_range / 2) % 360

            theta = ds.coords["theta"].values
            theta_ok = theta[(theta > s1) & (theta < s2)]

            # Select the current theta
            ds_tw = ds.sel(theta=theta_ok)

            # Select area with rad<1.5*rmax_ck22 and check if there is enough data to compute a vmax
            ds_check = ds.sel(theta=theta_ok, rad=slice(0, 1.5 * rmax_ck22))
            count_total = ds_check.wind_speed.values.flatten().shape[0]
            count_nan = np.sum(np.where(np.isnan(ds_check.wind_speed.values.flatten()), 1, 0))
            if count_total == 0:
                percent_valid = 0
            else:
                percent_valid = 100 - (count_nan / count_total * 100)

            if percent_valid < 70:
                logger.debug(
                    f"Skipped theta ({s1, s2}), because only {percent_valid}% of valid data with rad<1.5 * rmax_ck22"
                    f" count_total : {count_total}, count_nan: {count_nan}")
                new_rmaxs.append(np.nan)
                new_vmaxs.append(np.nan)
                i += 1
                continue

            # Compute the mean of the theta window. This gives a 2D profile (distance from center vs wind speed)
            tw_wind_mean = ds_tw.wind_speed.mean(dim="theta")
            peaks_idx, _ = find_peaks(tw_wind_mean.values)
            logger.debug(f"Peaks indexes: {peaks_idx}")

            rmax_peaks = tw_wind_mean.isel(rad=peaks_idx)
            if len(rmax_peaks) == 0:
                logger.debug(
                    f"Skipped theta ({s1, s2}), because no peaks or associated rmax has been found.")
                new_rmaxs.append(np.nan)
                new_vmaxs.append(np.nan)
                i += 1
                continue

            logger.debug(f"Rmax available for that peaks: {rmax_peaks.coords['rad'].values}")
            try:
                closest_rmax = rmax_peaks.sel(rad=rmaxs[i], method="nearest")
            except KeyError as e:
                logger.debug(f"No rmax found near {rmaxs[i]} for peaks {rmax_peaks.values}")
                new_rmaxs.append(np.nan)
                new_vmaxs.append(np.nan)
                i += 1
                continue
            rmax = closest_rmax.coords["rad"].item()

            # Get the vmax for the rmax closest to reference rmax
            vmax = closest_rmax.item()

            logger.debug(f"Reference rmax: {rmaxs[i]}, closest found rmax: {rmax}, "
                         f"associated vmax: {vmax}")

            new_rmaxs.append(rmax)
            new_vmaxs.append(vmax)
            i += 1

        df_curr = pd.DataFrame({"vmax": new_vmaxs, "rmax": new_rmaxs})
        df_curr = df_curr[~pd.isna(df_curr["vmax"])].reset_index()
        if len(df_curr.index) > 0:
            # The final vmax
            vmax = df_curr["vmax"].quantile(q=0.99)

            # Find existing closest vmax and take the corresponding rmax
            rmax = df_curr.iloc[(df_curr['vmax'] - vmax).abs().argsort()[0]]["rmax"]

            vmax_range_dict[t_range] = vmax
            rmax_range_dict[t_range] = rmax
            window_count_range_dict[t_range] = len(df_curr.index)
            std_range_dict[t_range] = df_curr["vmax"].std()
        else:
            vmax_range_dict[t_range] = np.nan
            rmax_range_dict[t_range] = np.nan
            window_count_range_dict[t_range] = 0
            std_range_dict[t_range] = np.nan
        return recursive_vmax_window(ds, range_list, rmax_ck22, new_rmaxs, new_vmaxs, vmax_range_dict, rmax_range_dict,
                                     window_count_range_dict, std_range_dict,
                                     t_start=t_start, t_stop=t_stop)
    else:
        df = pd.DataFrame({"vmax": vmaxs, "rmax": rmaxs})
        df = df[~pd.isna(df["vmax"])].reset_index()
        if len(df.index) > 0:
            # The final vmax
            vmax = df["vmax"].quantile(q=0.99)

            # Find existing closest vmax and take the corresponding rmax
            rmax = df.iloc[(df['vmax'] - vmax).abs().argsort()[0]]["rmax"]
            window_count = len(df.index)
            std = df["vmax"].std()
        else:
            vmax = np.nan
            rmax = np.nan
            window_count = 0
            std = np.nan
        return vmax, rmax, window_count, std, vmax_range_dict, rmax_range_dict, window_count_range_dict, std_range_dict


def compute_vmax(polar_ds: xr.Dataset, rmax_ck22: float, t_start=0, t_stop=360, range_list=None):
    """
    Compute Vmax and Rmax from centered polar product, using a sliding window.
    
    Parameters
    ----------
    polar_ds: xarray.Dataset Centered polar product
    rmax_ck22: float rmax in meters, used to check if there is enough data to compute a vmax for each window.
    
    Returns
    -------
    float, float, int
        Vmax (in m/s), rmax (in meters), the number of valid windows used to compute the final vmax    
    """

    if range_list is None:
        range_list = [90, 30, 10]
    ds = polar_ds.squeeze()
    # Mask outlier points (small areas with high gradients)
    l, mask_flt = mask_outlier_points(ds["lon"].values, ds["lat"].values, ma.array(ds["wind_speed"].values), [])
    ds["wind_speed"] = xr.DataArray(data=l[0], dims=["rad", "theta"])

    # Compute Vmax and Rmax on global profile
    ds_t_mean = ds.mean(dim="theta")
    vmax_all = ds_t_mean.wind_speed.max().item()
    idx_max_all = np.where(ds_t_mean.wind_speed.values == vmax_all)[0][0]
    # Find the corresponding rmax using the index
    rmax_all = ds_t_mean.isel(rad=idx_max_all)["rad"].item()

    vmax, rmax, vmax_window_count, vmax_std, vmax_range_list, rmax_range_list, \
    vmax_range_window_count, vmax_std_range_dict = recursive_vmax_window(ds, range_list, rmax_ck22 * 1000,
                                                                         np.full(t_stop - t_start, rmax_all).tolist(),
                                                                         np.full(t_stop - t_start, vmax_all).tolist(),
                                                                         t_start=t_start, t_stop=t_stop)

    return vmax, rmax, vmax_window_count, vmax_std, vmax_range_list, rmax_range_list, vmax_range_window_count, \
           vmax_std_range_dict


def compute_radii_new(ds_polar_north, rmax_ck22=None, tc_oriented=False, eye_ok=True, t_range=10):
    ds_polar_north = ds_polar_north.squeeze().copy()
    # Compute and add radii to xarray dataset
    if tc_oriented:
        quadrants = {"all": slice(0, 360), "FL": slice(0, 90), "RL": slice(90, 180), "RR": slice(180, 270),
                     "FR": slice(270, 360)}
    else:
        quadrants = {"all": slice(0, 360), "NW": slice(0, 90), "SW": slice(90, 180), "SE": slice(180, 270),
                     "NE": slice(270, 360)}

    l, mask_flt = mask_outlier_points(ds_polar_north["lon"].values, ds_polar_north["lat"].values,
                                      ma.array(ds_polar_north["wind_speed"].values), [])
    # l[0] is wind_speed array
    # Set data to nan where mask indicates other than valid data.
    wind_masked = xr.where(ds_polar_north["mask_flag"] > 0, np.nan, l[0])
    ds_polar_north["wind_speed"] = xr.DataArray(data=wind_masked, dims=["rad", "theta"])

    radii_quad = {}
    radii_nmi_quad = {}
    percent_valid_quad = {}
    for quad, sl in quadrants.items():
        logger.debug(f"Current quad {quad}")
        # if rmax_ck22 is available use best vmax function
        if rmax_ck22 is not None:
            logger.debug("Rmax_ck22 available: using compute_vmax function")
            vmax, rmax, vmax_percent_quality, _, _, _, _, _ = compute_vmax(ds_polar_north, rmax_ck22,
                                                                           t_start=sl.start, t_stop=sl.stop,
                                                                           range_list=[30, 10])
            logger.debug(f"For quadrant {quad}, vmax: {vmax}, rmax: {rmax}")
        else:
            logger.debug("Rmax_ck22 not available: compute vmax using mean profile")
            # no rmax : degraded mode
            # Mean the wind_speed.
            wind_mean = ds_polar_north.wind_speed.mean(dim="theta")
            vmax = wind_mean.max(dim="rad", skipna=True).item()
            # vmax_kts = vmax * 1.94384
            idx_max = np.where(wind_mean.values == vmax)[0][0]
            rmax = wind_mean.isel(rad=idx_max)["rad"].item()
            # rmax_nmi = rmax * 0.000539957
            logger.debug(f"For quadrant {quad}, vmax: {vmax}, rmax: {rmax}")

        theta = ds_polar_north.coords["theta"].values
        theta_ok = theta[(theta >= sl.start) & (theta < sl.stop)]
        polar_quad = ds_polar_north.sel(theta=theta_ok)
        polar_quad_ex_rmax = ds_polar_north.sel(theta=theta_ok, rad=slice(rmax, None))
        count_total = polar_quad.wind_speed.values.flatten().shape[0]
        count_nan = np.sum(np.where(np.isnan(polar_quad.wind_speed.values.flatten()), 1, 0))
        percent_valid = 100 - (count_nan / count_total * 100)

        if eye_ok:
            # Loops to find the radii and store them into a dict containing lists.
            wind_vals = [64, 50, 34]
            radii = [vmax, rmax]
            radii_nmi = [ms_to_knots(vmax), km_to_nmi(rmax / 1000)]
            percent_valid_radii = [vmax_percent_quality, vmax_percent_quality]
            for w in wind_vals:
                logger.debug(f"Quad: {quad}, Rad: {w}")

                # Find indexes where winds above w (radii value)
                wind_above = np.nonzero(polar_quad.wind_speed.values >= knots_to_ms(w))
                winds_1 = np.zeros_like(polar_quad.wind_speed.values)
                # Replace wind speed value by 1 for those locations
                winds_1[wind_above] = 1
                # Dilate 1 so that close areas of wind above limit gets connected to the main one.
                wind_expanded = binary_dilation(winds_1, iterations=2).astype(winds_1.dtype)
                # Neighbour structure so that adjacent pixels gets connected by "label" function (even diagonal ones)
                neighborhood = generate_binary_structure(2, 2)

                # Find contigus areas
                labels, num_labels = label(wind_expanded, structure=neighborhood)

                if num_labels == 0:
                    rad_w = np.nan
                    percent_theta_repr = 0
                else:
                    # get the area (nr. of pixels) of each labeled patch
                    sizes = ndimage.sum(winds_1, labels, range(1, num_labels + 1))

                    # To get the indices of all the min/max patches.
                    map = np.where(sizes == sizes.max())[0] + 1
                    mip = np.where(sizes == sizes.min())[0] + 1

                    # Get feature with biggest amount of pixels
                    max_index = np.zeros(num_labels + 1, np.uint8)
                    max_index[map] = 1
                    max_feature = max_index[labels]
                    idx_max = np.nonzero(max_feature)

                    # idx_max[1] gives the index on the theta grid. It helps to found out how widespread the
                    # wind area above the radii is.
                    theta_represented = np.unique(idx_max[1]).shape[0]
                    percent_theta_repr = (theta_represented / (sl.stop - sl.start)) * 100

                    # Get the maximum radius from the feature with the biggest size.
                    rad_w = np.max(polar_quad.coords["rad"].values[idx_max[0]])
                    cur_rad = rad_w
                    next_rad = rad_w
                    percent_ok_rad = 0
                    max_percent_rad = 0
                    max_perc_assoc_rad = 0
                    accept_percent_rad = 25
                    while percent_ok_rad < accept_percent_rad and (next_rad != 0 and next_rad != rmax):
                        cur_rad = next_rad
                        polar_cur_rad = polar_quad.sel(rad=cur_rad)
                        count_rad_tot = polar_cur_rad.wind_speed.values.flatten().shape[0]
                        count_above = (polar_cur_rad.wind_speed.values >= knots_to_ms(w)).sum()
                        percent_ok_rad = (count_above / count_rad_tot) * 100
                        if percent_ok_rad > max_percent_rad:
                            max_percent_rad = percent_ok_rad
                            max_perc_assoc_rad = cur_rad
                        next_rad = cur_rad - 1000
                        #print(percent_ok_rad, count_rad_tot, count_above)

                    if max_percent_rad >= accept_percent_rad:
                        rad_w = cur_rad
                    else:
                        rad_w = max_perc_assoc_rad


                ## OLD SECTION TO REMOVE ##
                #rads = np.array([])
                #for t in range(sl.start, sl.stop):
                #    continue
                #    s1 = (t - t_range / 2) % 360
                #    s2 = (t + t_range / 2) % 360
                #
                #    theta = ds_polar_north.coords["theta"].values
                #    theta_ok = theta[(theta > s1) & (theta < s2)]
                #    # select window for current theta and rad above quadrant rmax
                #    ds_t = ds_polar_north.sel(theta=theta_ok, rad=slice(rmax, None))
                #
                #    ds_w_mean = ds_t.wind_speed.mean(dim="theta")
                #
                #    # indexes of wind speed above the radii
                #    idx_r_sup = np.nonzero(ds_w_mean.values >= knots_to_ms(w))[0]
                #    ba = np.nonzero(ds_w_mean.values >= knots_to_ms(w))
                #    # Indexes of wind speed below the radii. The radii can't be well estimated without enough points
                #    # below the radii
                #    idx_r_less_count = np.count_nonzero(ds_w_mean.values < knots_to_ms(w))
                #    count_ds_w = ds_w_mean.values.flatten().shape[0]
                #
                #    if count_ds_w == 0:
                #        percent_less = 0
                #    else:
                #        percent_less = idx_r_less_count / count_ds_w * 100
                #    # idx_nan = np.nonzero(~np.isnan(ds_w_mean.values))[0]
                #    # mask_nonan = np.isin(idx_r, idx_nan)
                #    # idx_r = idx_r[mask_nonan]
                #
                #    if len(idx_r_sup) != 0:  # and percent_less > 10:
                #        # max radius for those wind speeds
                #        # print(w, quad, ds_w_mean.coords["rad"].values[idx_r])
                #        rad = np.quantile(ds_w_mean.coords["rad"].values[idx_r_sup], 0.9)
                #        # rad = np.max(ds_w_mean.coords["rad"].values[idx_r_sup])
                #        rads = np.append(rads, rad)
                #############

                # if rads.shape[0] >= 1:
                #    rad_w = np.quantile(rads, 0.95)
                # else:
                #    rad_w = np.nan
                radii.append(rad_w)
                radii_nmi.append(km_to_nmi(rad_w / 1000))

                idx_r_quad_less_count = np.count_nonzero(polar_quad_ex_rmax.wind_speed.values < knots_to_ms(w))
                count_quad = polar_quad_ex_rmax.wind_speed.values.flatten().shape[0]

                if count_quad == 0:
                    percent_less_quad = 0
                else:
                    # Percent of wind speed data pixels below the estimated radii (w)
                    percent_less_quad = idx_r_quad_less_count / count_quad * 100

                # we consider that at least 40% of percent_less_quad is equivalent to 100% of quality
                quality_from_below_percent = min((percent_less_quad / 40) * 100, 100)

                # Compare quantity of available pixels below radii to quantity of pixel in a quadrant
                # quantity_less_percent = idx_r_quad_less_count / count_quad

                # Flag de qualité : quantité de vent inférieur au Radii, nombre de rayon
                # percent_rad : for how many theta (°) a radii estimation could be performed in the quadrant, in percent
                # percent_rad = (rads.shape[0] / (sl.stop - sl.start)) * 100
                # logger.debug(f"Percent_less_quad: {percent_less_quad} %")

                # logger.debug(f"Percent rad: {percent_rad} %")
                logger.debug(f"Quality_from_below_percent: {quality_from_below_percent} %")
                logger.debug(f"Percent valid quad : {percent_valid} %")
                # Mixing the two values (0.2 quantile)
                quality_percent = np.quantile([quality_from_below_percent, percent_theta_repr], 0.2)
                # quality_percent = quality_from_below_percent
                # quality_percent = (quality_from_below_percent + percent_rad) / 2
                logger.debug(f"Quality_percent: {quality_percent}")
                percent_valid_radii.append(quality_percent)

            radii_quad[quad] = radii
            radii_nmi_quad[quad] = radii_nmi
            percent_valid_quad[quad] = percent_valid_radii
        else:
            radii_quad[quad] = [-1, -1, -1, -1, -1]
            radii_nmi_quad[quad] = [-1, -1, -1, -1, -1]
            percent_valid_quad[quad] = [-1, -1, -1, -1, -1]

    logger.debug(f"Radii_ms: {radii_quad}")
    logger.debug(f"Radii_nmi: {radii_nmi_quad}")
    logger.debug(f"Percent valid: {percent_valid_quad}")

    return radii_quad, radii_nmi_quad, percent_valid_quad


def add_wind_direction_components(ds: xr.Dataset, along_track: bool, dims: list):
    """
    Uses wind_streaks_orientation and wind_from_direction dataset variables to compute meridional (or along_track) and
    zonal (across_track) wind direction components. The components are added to the dataset, which is returned

    Parameters
    ----------
    ds: xarray.Dataset dataset to use for computation
    along_track: bool Indicates if the dataset is oriented following cyclone propagation or not. If True, the computed
        variables will be named using "along" and "across" prefixes
    dims: list dims to use to add the computed variables

    Returns
    -------
        ds: xarray.Dataset
            The dataset containing the added variables (wind direction components)
    """
    if along_track:
        prefix_zon = "across"
        prefix_merid = "along"
    else:
        prefix_zon = "zonal"
        prefix_merid = "meridional"

    vars_compo = ["wind_streaks_orientation", "wind_from_direction"]

    for var_name in vars_compo:
        if var_name in ds.data_vars:
            ds[f"{prefix_zon}_{var_name}_component"] = xr.DataArray(np.cos(np.deg2rad(360 - ds[var_name].values - 90)),
                                                                    dims=dims)  # zonal
            ds[f"{prefix_merid}_{var_name}_component"] = xr.DataArray(
                np.sin(np.deg2rad(360 - ds[var_name].values - 90)),
                dims=dims)  # merid

    return ds


def cart2polar(x, y, unit="radian"):
    """ Create a polar grid from a cartesian grid.

    Parameters
    ----------
    x: ndarray
        2D array containing the longitude values converted to km distances from the cyclone center.

    y: ndarray
        2D array containing the latitude values converted to km distances from the cyclone center.

    Returns
    -------
        r: ndarray
            2D array containing the radius
        theta: ndarray
            2D array containing the angles (unit depending on parameter 'unit')
    """

    r = np.sqrt(x ** 2 + y ** 2)
    theta = np.arctan2(y, x)

    if unit == "degree":
        theta = np.rad2deg(theta)

    return r, theta


def radial_tangential_compo(wind_across_angle, wind_along_angle, theta):
    cos_theta = np.cos(theta)
    sin_theta = np.sin(theta)

    wind_along_angle_tr = wind_along_angle
    wind_across_angle_tr = wind_across_angle

    # if len(wind_across_angle.shape) == 3:
    #    axes_tr = [0, 2, 1]
    #    wind_along_angle_tr = np.transpose(wind_along_angle, axes=axes_tr)
    #    wind_across_angle_tr = np.transpose(wind_across_angle, axes=axes_tr)
    # elif len(wind_across_angle.shape) != 2:
    #    logger.error("Unsupported number of axis")
    #    raise ValueError("Unsupported number of axis")

    # Radial and tangential wind values
    radial = -wind_across_angle_tr * cos_theta - wind_along_angle_tr * sin_theta
    tangential = -wind_across_angle_tr * sin_theta + wind_along_angle_tr * cos_theta

    # if len(wind_across_angle.shape) == 3:
    #    axes_tr = [0, 2, 1]
    #    radial = np.transpose(radial, axes=axes_tr)
    #    tangential = np.transpose(tangential, axes=axes_tr)
    # elif len(wind_across_angle.shape) != 2:
    #    logger.error("Unsupported number of axis")
    #    raise ValueError("Unsupported number of axis")

    return radial, tangential


def add_theta(ds: xr.Dataset, dims: tuple, theta_long_name_attr: str, theta_description_attr: str):
    """
    Add theta to cartesian dataset (with azimutal equidistant grid)

    Parameters
    ----------
    ds: xarray.Dataset The dataset to modify and to take data from
    dims: tuple xarray dimension to use for the new variables

    Returns
    -------
    xarray.Dataset
        The modified dataset
    """

    x_grid, y_grid = np.meshgrid(ds.coords["x"].values, ds.coords["y"].values)
    r, theta = cart2polar(x_grid, y_grid, unit="degree")
    theta = np.mod(theta, 360)
    theta = theta - 90
    theta = np.mod(theta, 360)
    ds["theta"] = (dims, theta[np.newaxis, ...])
    ds["theta"] = ds["theta"].assign_attrs(long_name=theta_long_name_attr, description=theta_description_attr,
                                           units="degrees")
    ds["rad"] = (dims, r[np.newaxis, ...])
    ds["rad"] = ds["rad"].assign_attrs(long_name="Distance from storm center", units="meters")

    return ds


def add_inflow_angle(ds: xr.Dataset, cartesian: bool, dims: list):
    """
    Compute inflow angle from wind direction (for wind_streaks_orientation  and wind_from_direction variables) and
    adds it, along with radial and tangential components, to the dataset.

    Parameters
    ----------
    ds: xarray.Dataset The dataset to modify and to take data from
    cartesian: bool Indicates if the dataset is in cartesian or polar projection. If polar, theta coordinates will be
        used. If cartesian, a theta will be computed for x and y coordinates.
    dims: list dimensions to use for adding new DataArray to Dataset.

    Returns
    -------
    xarray.Dataset
        The Dataset with new variables added

    """
    if not cartesian:
        theta = ds.coords["theta"].values
        theta = np.deg2rad(theta)
    else:
        theta = ds["theta"].values
        theta = np.deg2rad(theta)

    vars_compo = ["wind_streaks_orientation", "wind_from_direction"]

    for var_name in vars_compo:
        if var_name in ds.data_vars:
            wd_ac_tc = ds[f"across_{var_name}_component"].values
            wd_al_tc = ds[f"along_{var_name}_component"].values

            radial, tangential = radial_tangential_compo(wd_ac_tc, wd_al_tc, theta)

            # Inflow angle
            alpha = np.arctan(radial / tangential)

            ds[f"radial_{var_name}_component"] = xr.DataArray(radial, dims=dims)
            ds[f"tangential_{var_name}_component"] = xr.DataArray(tangential, dims=dims)
            ds[f"inflow_angle_{var_name}"] = xr.DataArray(np.degrees(alpha), dims=dims)

    return ds


def funcfit(X, aA_alpha0, bA_alpha0, aA_alpha1, bA_alpha1, aP_alpha1, bP_alpha1):
    r, phi = X
    A_alpha0 = aA_alpha0 * r + bA_alpha0
    A_alpha1 = aA_alpha1 * r + bA_alpha1
    P_alpha1 = aP_alpha1 * r + bP_alpha1

    func = A_alpha0 + A_alpha1 * np.cos(phi - P_alpha1)
    return func


def add_parametrized_inflow(ds: xr.Dataset, cartesian: bool, rotation_angle: float, dims: list):
    """
    Compute parametrized inflow angle and wind direction components based on real inflow angle and
    from wind speed. Computed variables are added to the dataset

    Parameters
    ----------
    ds: xarray.Dataset the dataset to which variables are added and from which the inflow angle is taken. Must contain
        wind_speed and inflow_angle_wind_streaks_orientation variables.
    cartesian: bool Indicates if the dataset is in polar projection or cartesian projection. If True x and y coordinates
        will be used to compute rad and theta coordinates. If False, theta and rad coordinates are directly used.
    rotation_angle: float The rotation that have been applied to the dataset to match cyclone direction propagation
    dims: list dims for the new variables that will be added. basically ["time", "x", "y"] or ["time", "theta", "rad"]

    Returns
    -------
    xarray.Dataset
        The dataset containing additionnal variables:

    """
    if "wind_streaks_orientation" not in ds.data_vars:
        return ds

    _alpha = np.radians(ds[f"inflow_angle_wind_streaks_orientation"].squeeze().values)
    alpha_nan_count = np.count_nonzero(np.isnan(_alpha))
    if alpha_nan_count != _alpha.flatten().shape[0]:
        _wdstd = ds["wind_speed"].squeeze().std(skipna=True).item()
        # if cartesian:
        #    x = ds.coords["x"].values
        #    y = ds.coords["y"].values
        #    x, y = np.meshgrid(x, y)
        #    _r = np.sqrt(x ** 2. + y ** 2.)
        #    _phi = np.arctan2(y, x)
        # else:
        #    _phi, _r = np.meshgrid(ds.coords["rad"].values, ds.coords["theta"].values, indexing="ij")

        if not cartesian:
            _r, _phi = np.meshgrid(ds.coords["rad"].values, ds.coords["theta"].values, indexing="ij")
            _phi = np.deg2rad(_phi)
        else:
            theta = ds["theta"].squeeze().values
            r = ds["rad"].squeeze().values
            _phi = np.deg2rad(theta)
            _r = r

        # Define the coefficients of the inflow angle fit
        filtre = (~np.isnan(_alpha)) & (_wdstd < 40)

        p0 = np.mean(_alpha[filtre]), 0, 0, 0, 0, 0
        print()
        res = curve_fit(funcfit, (_r[filtre], _phi[filtre]), _alpha[filtre], p0)
        alpha_fit = funcfit((_r, _phi), res[0][0], res[0][1], res[0][2], res[0][3], res[0][4], res[0][5])

        lat_mean_sign = np.sign(ds["lat"].squeeze().mean(skipna=True).item())
        across_fit = lat_mean_sign * np.cos(alpha_fit + _phi + np.pi / 2.)
        heading_fit = lat_mean_sign * np.sin(alpha_fit + _phi + np.pi / 2.)

        radial_fit, tangential_fit = radial_tangential_compo(across_fit, heading_fit, _phi)

        mer_interp_fit = (-np.sin(np.radians(-rotation_angle)) * across_fit + np.cos(
            np.radians(-rotation_angle)) * heading_fit) * 1.
        zon_interp_fit = (np.cos(np.radians(-rotation_angle)) * across_fit + np.sin(
            np.radians(-rotation_angle)) * heading_fit) * 1.  # clockwise
    else:
        _alpha_sq = _alpha.squeeze()
        alpha_fit = _alpha_sq
        radial_fit = _alpha_sq
        tangential_fit = _alpha_sq
        across_fit = _alpha_sq
        heading_fit = _alpha_sq
        mer_interp_fit = _alpha_sq
        zon_interp_fit = _alpha_sq

    ds["inflow_angle_parametrized"] = xr.DataArray(np.degrees(alpha_fit[np.newaxis, :]), dims=dims, attrs={
        "long_name": "Parametrized inflow angle",
        "units": "degrees"
    })

    ds["radial_wind_parametrized"] = xr.DataArray(radial_fit[np.newaxis, :], dims=dims, attrs={
        "long_name": "Parametrized radial wind streaks orientation component"
    })

    ds["tangential_wind_parametrized"] = xr.DataArray(tangential_fit[np.newaxis, :], dims=dims, attrs={
        "long_name": "Parametrized tangential wind streaks orientation component"
    })

    ds["across_wind_parametrized"] = xr.DataArray(across_fit[np.newaxis, :], dims=dims, attrs={
        "long_name": "Parametrized across track wind streaks orientation component",
    })

    ds["along_wind_parametrized"] = xr.DataArray(heading_fit[np.newaxis, :], dims=dims, attrs={
        "long_name": "Parametrized along track wind streaks orientation component",
    })

    ds["meridional_interp_parametrized"] = xr.DataArray(mer_interp_fit[np.newaxis, :], dims=dims, attrs={
        "long_name": "Parametrized meridional wind streaks orientation component",
    })

    ds["zonal_interp_parametrized"] = xr.DataArray(zon_interp_fit[np.newaxis, :], dims=dims, attrs={
        "long_name": "Parametrized zonal wind streaks orientation component",
    })

    return ds


def add_attributes(ds, sat_file_sw, lon_center, lat_center, sar_vmax, sar_rmax, vmax_std, vmax_window_count,
                   vmax_range_dict,
                   rmax_range_dict, vmax_range_window_count, vmax_std_range_dict, storm_name=None,
                   atcf_filename=None, sid=None,
                   atcf_vmax=None, atcf_rmw=None, ibtracs_vmax=None, ibtracs_rmax=None,
                   basin=None, mission=None,
                   rmax_ck22=None,
                   lon_eye_lwind=None, lat_eye_lwind=None, lon_eye_hwind=None,
                   lat_eye_hwind=None,
                   lon_eye_fg=None, lat_eye_fg=None, status=None, distance_to_coast=None, distance_to_acq=None,
                   percent_outside=None,
                   percent_inside_island=None, percent_non_usable=None,
                   eye_shape=None, lwind_contour=None, hwind_contour=None,
                   radii_quad=None, radii_nmi_quad=None, percent_valid_quad=None,
                   tco_radii_quad=None, tco_radii_nmi_quad=None, tco_percent_valid_quad=None,
                   cyclone_speed=None,
                   cyclone_speed_std=None,
                   deg_rotation=None, deg_rotation_std=None, quality_flags=None):
    """
    Adds various information to xarray Dataset. Most info come from eye center computation.

    Parameters
    ----------
        ds: xarray.Dataset Dataset to add attributes and variables to
        storm_name: str Cyclone name
        sid: str Storm ID
        atcf_filename: str name of atcf file associated to cyclone
        sat_file_sw: str name of satellite file used as a source for this dataset
        atcf_vmax: float atcf vmax associated to acquisition (m/s)
        atcf_rmw: float atcf rmw associated to acquisition (km)
        basin: str Basin in which reside the acquisition
        mission: str Satellite mission that acquired the data
        lon_center: float longitude of cyclone eye center
        lat_center: float latitude of cyclone eye_center
        lon_eye_lwind: float longitude of low wind area barycenter
        lat_eye_lwind: float latitude of low wind area barycenter
        lon_eye_hwind: float longitude of high wind area barycenter
        lat_eye_hwind: float latitude of high wind area barycenter
        lon_eye_fg: float longitude of first guess center position (actually is actf point position)
        lat_eye_fg: float latitude of first guess center poistion (actually is actf point poisition)
        center_status: CenterStatus indicates if center is outside or inside bounding box
        distance_to_coast: float Smallest distance between coast and acquisition bounding box in meters
        distance_to_acq: float distance between atcf track point and acquisition bounding box in meters
        percent_outside: float
        percent_inside_island: float
        percent_non_usable: float
        eye_shape: shapely.geometry.LineString Eye shape contour found by the eye center research procedure
        lwind_contour: shapely.geometry.LineString Area within which low wind barycenter is searched
        hwind_contour: shapely.geometry.LineString Area within which high wind barycenter is searched
        radii_quad: dict contains wind radii data in meters. One of the output of compute_radii
        radii_nmi_quad: dict contains wind radii data in nmi. One of the output of compute_radii
        percent_valid_quad: dict contains percent of valid data used to compute radii. One of the output of compute_radii
        tco_radii_quad: dict contains TC oriented wind radii data in meters. One of the output of compute_radii
        tco_radii_nmi_quad: dict contains TC oriented wind radii data in nmi. One of the output of compute_radii
        tco_percent_valid_quad: dict contains TC oriented per   cent of valid data used to compute radii. One of the output of compute_radii
        cyclone_speed: float The cyclone propagation speed
        cyclone_speed_std: float The cyclone propagation speed standard deviation
        deg_rotation: float or None degree of rotation applied to dataset that are rotated to match cyclone
            propagation direction
        deg_rotation_std: float rotation angle standard deviation
        quality_flags: dict Contains xarray.DataArray to be added to the Dataset, with all quality flag infos

        Returns
        -------
            xarray.Dataset
                The modified dataset
    """

    ds.attrs["TCVA_version"] = version

    if storm_name is not None:
        ds.attrs["Storm name"] = storm_name

    if sid is not None:
        ds.attrs["Storm ID"] = sid
    if atcf_filename is not None:
        ds.attrs["Track source file"] = atcf_filename

    ds.attrs["Source satellite file"] = os.path.basename(sat_file_sw)

    if atcf_vmax is not None:
        ds["track_vmax"] = xr.DataArray(data=float(atcf_vmax),
                                        attrs={"long_name": "Closest ATCF Track point VMAX", "units": "m/s"})

        ds["cyclone_category"] = xr.DataArray(data=spd2cat(atcf_vmax, unit="m/s"),
                                              attrs={"long_name": "Cyclone category based on track VMAX"})

    if ibtracs_vmax is not None:
        ds["ibtracs_vmax"] = xr.DataArray(data=float(ibtracs_vmax),
                                        attrs={"long_name": "Closest IBTrACS point VMAX", "units": "m/s"})

    if atcf_rmw is not None:
        ds["track_rmax"] = xr.DataArray(data=float(atcf_rmw),
                                        attrs={"long_name": "Closest ATCF Track point RMAX", "units": "km"})

    if ibtracs_rmax is not None:
        ds["ibtracs_rmax"] = xr.DataArray(data=float(ibtracs_rmax),
                                        attrs={"long_name": "Closest IBTrACS Track point RMAX", "units": "km"})

    if rmax_ck22 is not None:
        ds["rmax_ck22"] = xr.DataArray(data=round(rmax_ck22, 2),
                                       attrs={"long_name": "Rmax computed following D. Chavas and J. Knaff model.",
                                              "description": "rmax_ck22 computation method has been implemented "
                                                             "following research produced by D. Chavas and J. Knaff, "
                                                             "DOI:10.1175/WAF-D-21-0103.1",
                                              "unit": "km"})

    if basin is not None:
        ds.attrs["Basin"] = basin
        # ds["basin"] = xr.DataArray(data=basin, attrs={"long_name": "Basin short name",
        #                                              "description": "Cyclone's basin short name at time"
        #                                                             "of acquisition."})

    if mission is not None:
        ds.attrs["Satellite mission"] = mission
        # ds["mission"] = xr.DataArray(data=basin, attrs={"long_name": "Satellite mission"})

    if status is not None:
        ds.attrs["Status"] = str(status)

    if eye_shape is not None:
        ds["eye_shape"] = xr.DataArray(data=eye_shape.wkt, attrs={"long_name": "Eye shape (WKT)",
                                                                  "description": "Polygon WKT representation of "
                                                                                 "the eye shape."})

    ds["eye_center"] = xr.DataArray(data=Point(lon_center, lat_center).wkt,
                                    attrs={"long_name": "TC eye center (WKT)",
                                           "description": "Point WKT representation of the TC eye center."})

    if lon_eye_lwind is not None and lat_eye_lwind:
        ds["low_wind_area_barycenter"] = xr.DataArray(
            data=Point(lon_eye_lwind, lat_eye_lwind).wkt,
            attrs={"long_name": "Low wind speed area barycenter (WKT)",
                   "description": "Point WKT representation of the low wind speed barycenter found by the "
                                  "eye detection algorithm."})

    if lon_eye_hwind is not None and lat_eye_hwind is not None:
        ds["high_wind_area_barycenter"] = xr.DataArray(
            data=Point(lon_eye_hwind, lat_eye_hwind).wkt,
            attrs={"long_name": "High wind area barycenter (WKT)",
                   "description": "Point WKT representation of the high wind speed barycenter found "
                                  "by the eye detection algorithm."
                   })

    if lon_eye_fg is not None and lat_eye_fg is not None:
        ds["interpolated_track_point"] = xr.DataArray(
            data=Point(lon_eye_fg, lat_eye_fg).wkt,
            attrs={"long_name": "Closest interpolated track point (WKT)",
                   "description": "Point WKT representation of the location of the closest ATCF interpolated "
                                  "track point"})

    if quality_flags is not None:
        for flg in quality_flags:
            ds[flg] = quality_flags[flg]
            logger.info(f"Flag value : {flg} -> {ds[flg].item()}")

    if distance_to_coast is not None:
        ds["dist_to_coast"] = xr.DataArray(data=distance_to_coast,
                                           attrs={"long_name": "Distance of track point to nearest coast.",
                                                  "description": "Distance of track point to nearest coast. "
                                                                 "A negative value means track point is "
                                                                 "inside land.",
                                                  "units": "meters"})

    if distance_to_acq is not None:
        ds["distance_to_acquisition"] = xr.DataArray(data=distance_to_acq,
                                                     attrs={"long_name": "Distance of track point to nearest"
                                                                         " acquisition border.",
                                                            "description": "Distance of closest interpolated ATCF"
                                                                           " track point to nearest"
                                                                           " acquisition border.",
                                                            "units": "meters"})

    if percent_outside is not None:
        ds["percent_outside"] = xr.DataArray(data=percent_outside,
                                             attrs={"long_name": "Area within 100km around track point outside"
                                                                 " acquisition bounding box",
                                                    "units": "percents"})

    if percent_inside_island is not None:
        ds["percent_inside_island"] = xr.DataArray(data=percent_inside_island,
                                                   attrs={"long_name": "Area of 100km around track point inside"
                                                                       " acquisition bounding box and on land",
                                                          "units": "percents"},
                                                   )

    if percent_non_usable is not None:
        ds["percent_non_usable"] = xr.DataArray(data=percent_non_usable, attrs={
            "long_name": "Sum of percent_outside and percent_inside_island, which is the area of 100km around track point"
                         "which is unusable for analysis", "units": "percents"
        })

    if cyclone_speed is not None:
        ds["cyclone_speed"] = xr.DataArray(data=cyclone_speed, attrs={
            "long_name": "Estimated cyclone propagation speed",
            "units": "m/s"
        })

    if cyclone_speed_std is not None:
        ds["cyclone_speed_std"] = xr.DataArray(data=cyclone_speed_std, attrs={
            "long_name": "Error estimation of cyclone propagation speed",
            "units": "m/s"
        })

    if deg_rotation is not None:
        ds["rotation_angle"] = xr.DataArray(data=deg_rotation, attrs={
            "long_name": "Angle of rotation performed on the acquisition to match the cyclone propagation direction",
            "unit": "degrees"
        })
        ds["rotation_angle_std"] = xr.DataArray(data=deg_rotation_std, attrs={
            "long_name": "Standard deviation of angle of rotation",
        })

    ds["vmax"] = xr.DataArray(data=sar_vmax, attrs={"long_name": "Maximum sustained wind speed", "units": "m/s",
                                                    "description": "Maximum sustained wind speed"
                                                    })
    ds["rmax"] = xr.DataArray(data=sar_rmax, attrs={"long_name": "Radius of the max sustained wind speed",
                                                    "units": "meters"})
    ds["vmax_percent_valid"] = xr.DataArray(data=round((vmax_window_count / 360) * 100),
                                            attrs={"long_name": "Percentage of valid angles used to "
                                                                "compute vmax and rmax.",
                                                   "units": "percent"})

    ds[f"vmax_std"] = xr.DataArray(data=round(vmax_std, 3),
                                   attrs={"long_name": f"Standard deviation of vmax list used "
                                                       f"to compute final vmax.",
                                          "units": "m/s"})

    for k, v in vmax_range_dict.items():
        ds[f"vmax_{str(k)}"] = xr.DataArray(data=v, attrs={"long_name": "Maximum sustained wind speed "
                                                                        f"for a sliding window of {str(k)}°",
                                                           "units": "m/s",
                                                           "description": "Maximum sustained wind speed"})

    for k, v in rmax_range_dict.items():
        ds[f"rmax_{str(k)}"] = xr.DataArray(data=v, attrs={"long_name": "Radius of the max sustained wind speed "
                                                                        f"for a sliding window of {str(k)}°",
                                                           "units": "m/s"})

    for k, v in vmax_range_window_count.items():
        ds[f"vmax_{str(k)}_percent_valid"] = xr.DataArray(data=round((v / 360) * 100),
                                                          attrs={"long_name": "Percentage of valid angles used to "
                                                                              f"compute vmax and rmax, for a {v}° "
                                                                              f"window.",
                                                                 "units": "percent"})

    for k, v in vmax_std_range_dict.items():
        ds[f"vmax_{str(k)}_std"] = xr.DataArray(data=round(v, 3),
                                                attrs={"long_name": f"Standard deviation of vmax list used "
                                                                    f"to compute vmax for {k} window.",
                                                       "units": "m/s"})

    # ds["vmax_window_count"] = xr.DataArray(data=vmax_window_count,
    #                                       attrs={"long_name": "Number of window used to compute SAR vmax"})

    if radii_quad is not None and radii_nmi_quad is not None and percent_valid_quad is not None:
        radii_vmax, radii_rmax = get_vmax(radii_nmi_quad, percent_valid_quad)
        ds["vmax_old"] = xr.DataArray(data=radii_vmax,
                                      attrs={"long_name": "Maximum sustained wind speed", "units": "knots",
                                             "description": "Maximum sustained wind speed"
                                             })
        ds["rmax_old"] = xr.DataArray(data=radii_rmax, attrs={"long_name": "Radius of the max sustained wind speed",
                                                              "units": "nmi"})
        # Add radii to xarray dataset
        quadrants = {"all": slice(0, 360), "NE": slice(0, 90), "SE": slice(90, 180), "SW": slice(180, 270),
                     "NW": slice(270, 360)}
        for quad, sl in quadrants.items():
            ds[f"geo_radii_ms_{quad}"] = xr.DataArray(data=radii_quad[quad], dims="dim_wind_radii", attrs={
                "long_name": f"Vmax, Rmax, R64, R50, R34 for {quad} quadrant(s) wind speed mean for north oriented data.",
                "units": "m/s meters meters meters meters",
                "description": f"Computed vmax and radii for {quad} north oriented quadrant(s) (NW, NE, SW, SE). "
                               f"5 values are available : "
                               f"Vmax : vmax on the quadrant {quad}, "
                               f"Rmax : radius of vmax, "
                               f"R64 : radius for 34 knots winds, "
                               f"R50 : radius for 50 knots winds, "
                               f"R34 : radius for 64 knots winds. "
                               f"NaN means that a correct vmax, rmax or radius could not be computed."
            })

            ds[f"geo_radii_kts_{quad}"] = xr.DataArray(data=radii_nmi_quad[quad], dims="dim_wind_radii", attrs={
                "long_name": f"Vmax, Rmax, R64, R50, R34 for {quad} quadrant(s) wind speed mean for north oriented data.",
                "units": "knots n.mi n.mi n.mi n.mi",
                "description": f"Computed vmax and radii for {quad} quadrant(s). 5 values are available : "
                               f"Vmax : vmax on the quadrant {quad}, "
                               f"Rmax : radius of vmax in quadrant {quad}, "
                               f"R64 : radius for 34 knots winds, "
                               f"R50 : radius for 50 knots winds, "
                               f"R34 : radius for 64 knots winds. "
                               f"NaN means that a correct vmax, rmax or radius could not be computed."
            })

            ds[f"geo_percent_valid_radii_{quad}"] = xr.DataArray(data=percent_valid_quad[quad], dims="dim_wind_radii",
                                                                 attrs={
                                    "long_name": f"Quality flag values in percent for each item, "
                                    f"5 values available representing quality for each : "
                                    f"Vmax, Rmax, R64, R50, R34 for {quad} quadrant(s) for north oriented data."
                                    f"Values below 40% indicates a bad quality radii.",
                                    "units": "percent"
                                                                 })

    if tco_radii_quad is not None and tco_radii_nmi_quad is not None and tco_percent_valid_quad is not None:
        # Add radii to xarray dataset
        quadrants = {"all": slice(0, 360), "FR": slice(0, 90), "RR": slice(90, 180), "RL": slice(180, 270),
                     "FL": slice(270, 360)}
        for quad, sl in quadrants.items():
            ds[f"tco_radii_ms_{quad}"] = xr.DataArray(data=tco_radii_quad[quad], dims="dim_wind_radii", attrs={
                "long_name": f"Vmax, Rmax, R64, R50, R34 for {quad} quadrant(s) wind speed mean for TC oriented data.",
                "units": "m/s meters meters meters meters",
                "description": f"Computed vmax and radii for {quad} quadrant(s). 5 values are available : "
                               f"Vmax : vmax on the quadrant {quad}, "
                               f"Rmax : radius of vmax on the quadrant {quad}, "
                               f"R64 : radius for 34 knots winds, "
                               f"R50 : radius for 50 knots winds, "
                               f"R34 : radius for 64 knots winds. "
                               f"NaN means that a correct radius cannot be found for that "
                               f"wind speed."
            })

            ds[f"tco_radii_kts_{quad}"] = xr.DataArray(data=tco_radii_nmi_quad[quad], dims="dim_wind_radii", attrs={
                "long_name": f"Vmax, Rmax, R64, R50, R34 for {quad} quadrant(s) wind speed mean for TC oriented data.",
                "units": "knots n.mi n.mi n.mi n.mi",
                "description": f"Computed vmax and radii for {quad} quadrant(s). 5 values are available : "
                               f"Vmax : vmax on the wind profile, "
                               f"Rmax : radius of maximum wind speed, "
                               f"R64 : radius for 34 knots winds, "
                               f"R50 : radius for  50 knots winds, "
                               f"R34 : radius for 64 knots winds. "
                               f"NaN means that a correct radius could not be found for that "
                               f"wind speed."
            })

            ds[f"tco_percent_valid_radii_{quad}"] = xr.DataArray(data=tco_percent_valid_quad[quad],
                                                                 dims="dim_wind_radii", attrs={
                    "long_name": f"Quality flag values in percent for each radii, "
                                 f"5 values available representing "
                                 f"quality for each : "
                                 f"Vmax, Rmax, R64, R50, R34 for {quad} quadrant(s) for TC oriented data."
                                 f"Values below 40% indicates a bad quality radii.",
                    "units": "percent"
                })
    # ds.attrs["TC center status"] = status
    # ds.attrs["Area within 100km around track point outside acquisition bounding box in percent"] = percent_outside
    # ds.attrs["Area of 100km around track point inside acquisition bounding and on land in percent"] = percent_inside_island

    # ds.attrs["% of area of 100km around track point inside " \
    #                     "acquisition bounding and on land or outside of bounding box"] = percent_inside_island

    if lwind_contour is not None:
        ds["low_wind_research"] = xr.DataArray(
            data=lwind_contour.wkt,
            attrs={"long_name": "WKT shape of area within which low wind speed area has been searched.",
                   "description": "Polygon WKT representation of the research area within which a stable low wind speed"
                                  "area is researched, to deduce a low wind speed barycenter. Computed by the eye "
                                  "detection algorithm."})

    if hwind_contour is not None:
        ds["high_wind_research"] = xr.DataArray(
            data=hwind_contour.wkt,
            attrs={"long_name": "WKT shape of area within which "
                                "high wind speed pixels are considered.",
                   "description": "Polygon WKT representation of the research area within which high wind speed pixels"
                                  " are considered to compute a high wind speed barycenter. Computed by the eye"
                                  " detection algorithm."})

    return ds


def save_products(ds_gd, ds_polar_north, ds_cyclone_gd, ds_cyclone, ds_attr_polar_north_gd, ds_attr_cyclone, base_path,
                  analysis_group=None):
    logger.info("Saving to netcdf...")
    outfile_gd = f"{base_path}_geogr_gd.nc"
    outfile = f"{base_path}_geogr_polar.nc"
    outfile_cyclone = f"{base_path}_cyclone_polar.nc"
    outfile_cyclone_gd = f"{base_path}_cyclone_gd.nc"

    logger.info(f"Writing netCDF product to {outfile_gd}")

    ds_gd.to_netcdf(path=outfile_gd, unlimited_dims=["time"])
    if analysis_group is not None:
        ds_attr_polar_north_gd.to_netcdf(path=outfile_gd, group=analysis_group, mode="a")
    else:
        ds_attr_polar_north_gd.to_netcdf(path=outfile_gd, mode="a")

    logger.info(f"Writing netCDF product to {outfile}")
    ds_polar_north.to_netcdf(path=outfile, unlimited_dims=["time"])
    if analysis_group is not None:
        ds_attr_polar_north_gd.to_netcdf(path=outfile, group=analysis_group, mode="a")
    else:
        ds_attr_polar_north_gd.to_netcdf(path=outfile, mode="a")

    if ds_cyclone is not None:
        logger.info(f"Writing netCDF product to {outfile_cyclone}")
        ds_cyclone.to_netcdf(path=outfile_cyclone, unlimited_dims=["time"])
        if analysis_group is not None:
            ds_attr_cyclone.to_netcdf(path=outfile_cyclone, group=analysis_group, mode="a")
        else:
            ds_attr_cyclone.to_netcdf(path=outfile_cyclone, mode="a")

    if ds_cyclone_gd is not None:
        logger.info(f"Writing netCDF product to {outfile_gd}")
        ds_cyclone_gd.to_netcdf(path=outfile_cyclone_gd, unlimited_dims=["time"])
        if analysis_group is not None:
            ds_attr_cyclone.to_netcdf(path=outfile_cyclone_gd, group=analysis_group, mode="a")
        else:
            ds_attr_cyclone.to_netcdf(path=outfile_cyclone_gd, mode="a")

    logger.info("Done saving.")


def center_products_opt(sat_file, lon_center, lat_center, path, direction=None,
                        api_url="https://cyclobs.ifremer.fr"):
    """
    Save netCDF product from given sat_file and lon/lat.

    Parameters
    ----------
    sat_file: str Satellite path, L2C
    lon_center: float longitude (degrees) of eye center
    lat_center: float latitude (degrees) of eye center
    resolution: int resolution to use for satellite file input.
    path: str Save path
    direction: float cyclone direction from north. If not given, the CycLobs API will be requested
    api_url: str optional. Base URL to request API.

    Returns
    -------

    """
    # sat_file_sw = owi.L2CtoLight(sat_file, resolution=resolution, mode="sw")
    sat_file_sw = sat_file
    sat_date = owi.getDateFromFname(os.path.basename(sat_file))

    storm_name = None
    atcf_filename = None
    atcf_vmax = None
    sid = None
    dir_std = None
    speed = None
    speed_std = None

    if direction is None:
        sat_id = "-".join(sat_file_sw.split("-")[6:8])
        logger.debug(f"Satellite ID: {sat_id}")

        sat_api_data = api_req(sat_id, api_url, partial=True)
        if len(sat_api_data.index) == 0:
            logger.warning("Couldn't find API data for that cyclone. Some attributes will be filled with placeholders.")
        else:
            storm_name = sat_api_data.iloc[0]["cyclone_name"]
            atcf_filename = sat_api_data.iloc[0]["track_file"]
            atcf_vmax = float(sat_api_data.iloc[0]["vmax (m/s)"])
            sid = sat_api_data.iloc[0]["sid"]
            direction, dir_std, speed, speed_std = cyclone_speed_dir_angle_to_north(sid, sat_date)

    center_products(sat_file, lon_center, lat_center, path=path, direction=direction,
                    dir_std=dir_std, speed=speed, speed_std=speed_std, sid=sid, storm_name=storm_name,
                    atcf_filename=atcf_filename, atcf_vmax=atcf_vmax, eye_ok=True, sat_file_light=True)


def center_products(sat_file: str, lon_center: float, lat_center: float, rmax_ck22=None, resolution=None, path=None,
                    direction=None,
                    dir_std=None, speed=None, speed_std=None,
                    storm_name=None, atcf_filename=None, atcf_vmax=None, sid=None, eye_ok=True, sat_file_light=False):
    """
    Creates 4 xarray datasets using source sat_file and lon/lat center.

    Parameters
    ----------
    sat_file: str the SAR satellite file path, L2C
    lon_center: float longitude of eye center
    lat_center: float latitude of eye center
    rmax_ck22: float Rmax CK22 computed using ibtracs or atcf data. Optional
    resolution: int resolution to use for satellite input file
    path: str path to save generated products. Optional. If none, nothing is saved on disk.
    direction: float cyclone direction relative to the north. Optional. direction, dir_std, speed and speed_std must
        be provided together, if provided.
    dir_std: float standard deviation of direction calculation. Optional.
    speed: float cyclone translation speed. Optional
    speed_std: float standard deviation of cyclone translation speed calculation. Optional
    storm_name: str storm name. Will be added to the generated NETCDF products. Optional
    atcf_filename: str atcf filename used as the track source. Optional.
    atcf_vmax: float atcf vmax of the closest colcoated atcf point. Optional.
    sid: str storm id. Optional
    eye_ok: bool is the eye inside or outside the acquisition bounding box.
    sat_file_light: Specifies if the given sat_file is already in the nclight sw format or if a path conversion
        must occur


    Returns
    -------
        ds_gd : xarray.Dataset cartesian dataset, north oriented
        ds_cyclone_gd : xarray.Dataset cartsian dataset, cyclone oriented
        ds_polar_north: xarray.Dataset polar dataset, north oriented
        ds_cyclone: xarray.Dataset polar dataset, cyclone oriented
        radii_quad: dict computed radii for all+4 quadrants (m/s and km)
        radii_nmi_quad: dict computed radii for all+4 quadrants (knots and nmi)
        percent_valid_quad: dict percent of valid data for each computed radii (all+4 quadrants)
        tco_radii_quad: dict computed radii for all+4 quadrants, cyclone oriented (m/s and km)
        tco_radii_nmi_quad: dict computed radii for all+4 quadrants: cyclone oriented (knots and nmi)
        tco_percent_valid_quad: percent of valid data for each computed radii (all+4 quadrants) cyclone oriented.
    """
    if not sat_file_light:
        sat_file_sw = owi.L2CtoLight(sat_file, resolution=resolution, mode="sw")
    else:
        sat_file_sw = sat_file

    # Opening source dataset
    ds = xr.open_dataset(sat_file_sw, mask_and_scale=False)

    logger.info("Creating aeqd gridded dataset...")
    # Projecting data to a 1000x1000 grid in azimutal equidistant projection, centered on cyclone center
    ds_gd, x_center, y_center = to_aeqd(ds, lon_center, lat_center, x_size=1000, y_size=1000)
    ds_gd.attrs = {}

    # Creating another grid for the polar product, that will be reduced to a 360x500 grid (1080 is a multiple of 360
    # which is needed for downsampling
    ds_gd_pol, x_center_pol, y_center_pol = to_aeqd(ds, lon_center, lat_center, x_size=1000, y_size=1080,
                                                    resolution=1000)
    logger.info("Done creating aeqd gridded dataset.")

    logger.info("Converting dataset to polar...")
    # Converting dataset to polar grid
    ds_polar_north = to_polar(ds_gd_pol, x_center_pol, y_center_pol, lon_center, lat_center, reduce_grid=(3, 2),
                              max_polar_index_radius=500, theta_len=360, force_pix_dist=1000)

    logger.info("Done converting dataset to polar.")

    # Getting acquisition timestamp
    sat_date = owi.getDateFromFname(os.path.basename(sat_file))
    logger.debug(f"Satellite acq date {sat_date}")

    if path is not None:
        sat_id = "-".join(sat_file_sw.split("-")[6:8])
        logger.debug(f"Satellite ID: {sat_id}")

    if direction is not None:
        logger.info("Rotating polar dataset...")
        # Create new polar dataset resulting of rotation of polar north orientated dataset,
        # to match the cyclone propagation direction
        ds_cyclone = to_cylone_dir(ds_polar_north, direction, south=lat_center < 0)
        logger.info("Done rotating polar dataset..")

        logger.info("Rotating cartesian dataset...")
        # Create a new cartesian dataset, resulting of rotation of cartesian north orientated dataset,
        # to match the cyclone propagation direction
        ds_cyclone_gd = rotate_dataset(ds_gd, direction, south=lat_center < 0)
        logger.info("Done rotating cartesian dataset.")

    logger.info("Adding wind direction related variables...")

    if direction is not None:
        # Adding zonal and meridional components, inflow angle...
        ds_cyclone = add_wind_direction_components(ds_cyclone, along_track=True, dims=["time", "rad", "theta"])
        ds_cyclone_gd = add_wind_direction_components(ds_cyclone_gd, along_track=True, dims=["time", "x", "y"])
        ds_cyclone_gd = add_theta(ds_cyclone_gd, dims=("time", "y", "x"),
                                  theta_long_name_attr="Angle from cyclone propagation direction",
                                  theta_description_attr="Angle from cyclone propagation direction "
                                                         "(0° points to cyclone direction), "
                                                         "trigonometrically oriented")
        ds_cyclone = add_inflow_angle(ds_cyclone, cartesian=False, dims=["time", "rad", "theta"])
        ds_cyclone_gd = add_inflow_angle(ds_cyclone_gd, cartesian=True, dims=["time", "x", "y"])
        ds_cyclone = add_parametrized_inflow(ds_cyclone, cartesian=False, rotation_angle=direction,
                                             dims=["time", "rad", "theta"])
        ds_cyclone_gd = add_parametrized_inflow(ds_cyclone_gd, cartesian=True, rotation_angle=direction,
                                                dims=["time", "x", "y"])
    else:
        ds_cyclone = None
        ds_cyclone_gd = None

    ds_gd = add_wind_direction_components(ds_gd, along_track=False, dims=["time", "x", "y"])
    ds_gd = add_theta(ds_gd, dims=("time", "y", "x"), theta_long_name_attr="Angle from north",
                      theta_description_attr="Angle from north (0° points to the north), "
                                             "trigonometrically oriented")

    ds_polar_north = add_wind_direction_components(ds_polar_north, along_track=False, dims=["time", "rad", "theta"])

    logger.info("Done adding wind direction related variables.")

    logger.info("Computing radii using polar north dataset...")
    # Compute radii
    radii_quad, radii_nmi_quad, percent_valid_quad = compute_radii_new(ds_polar_north, rmax_ck22, eye_ok=eye_ok)

    sar_vmax, sar_rmax, window_count, vmax_std, vmax_range_dict, rmax_range_dict, \
    window_count_range_dict, vmax_std_range_dict = compute_vmax(ds_polar_north, radii_quad["all"][1])

    # Compute tco radii
    if direction is not None:
        tco_radii_quad, tco_radii_nmi_quad, tco_percent_valid_quad = compute_radii_new(ds_cyclone, rmax_ck22=rmax_ck22,
                                                                                       tc_oriented=True,
                                                                                       eye_ok=eye_ok)
    else:
        tco_radii_quad = None
        tco_radii_nmi_quad = None
        tco_percent_valid_quad = None
    logger.info("Done computing radii using polar north dataset.")

    # Closing source dataset
    ds.close()

    if path is not None:
        ds_attr_polar_north_gd = xr.Dataset()
        ds_attr_polar_north_gd = add_attributes(ds_attr_polar_north_gd, sat_file_sw,
                                                lon_center, lat_center, sar_vmax=sar_vmax, vmax_std=vmax_std,
                                                sar_rmax=sar_rmax, vmax_window_count=window_count,
                                                vmax_range_dict=vmax_range_dict,
                                                rmax_range_dict=rmax_range_dict,
                                                vmax_range_window_count=window_count_range_dict,
                                                vmax_std_range_dict=vmax_std_range_dict,
                                                storm_name=storm_name,
                                                sid=sid,
                                                atcf_filename=atcf_filename,
                                                atcf_vmax=atcf_vmax,
                                                rmax_ck22=rmax_ck22,
                                                cyclone_speed=speed,
                                                cyclone_speed_std=speed_std, radii_quad=radii_quad,
                                                radii_nmi_quad=radii_nmi_quad, percent_valid_quad=percent_valid_quad,
                                                tco_radii_quad=tco_radii_quad, tco_radii_nmi_quad=tco_radii_nmi_quad,
                                                tco_percent_valid_quad=tco_percent_valid_quad
                                                )

        if direction is not None:
            ds_attr_cyclone = xr.Dataset()
            ds_attr_cyclone = add_attributes(ds_attr_cyclone, sat_file_sw, lon_center, lat_center, sar_vmax=sar_vmax,
                                             vmax_std=vmax_std,
                                             sar_rmax=sar_rmax, vmax_window_count=window_count,
                                             vmax_range_dict=vmax_range_dict,
                                             rmax_range_dict=rmax_range_dict,
                                             vmax_range_window_count=window_count_range_dict,
                                             vmax_std_range_dict=vmax_std_range_dict,
                                             storm_name=storm_name, sid=sid, atcf_filename=atcf_filename,
                                             atcf_vmax=atcf_vmax, cyclone_speed=speed,
                                             rmax_ck22=rmax_ck22,
                                             cyclone_speed_std=speed_std,
                                             deg_rotation=direction, deg_rotation_std=dir_std, radii_quad=radii_quad,
                                             radii_nmi_quad=radii_nmi_quad, percent_valid_quad=percent_valid_quad,
                                             tco_radii_quad=tco_radii_quad, tco_radii_nmi_quad=tco_radii_nmi_quad,
                                             tco_percent_valid_quad=tco_percent_valid_quad
                                             )
        else:
            ds_attr_cyclone = None

        sid_name = ""
        if sid is not None:
            sid_name = "_" + sid
        base_path = os.path.join(path,
                                 f"{os.path.basename(sat_file_sw).replace('.nc', '').replace('-cm-', '-ca-')}{sid_name}")
        save_products(ds_gd, ds_polar_north, ds_cyclone_gd, ds_cyclone, ds_attr_polar_north_gd, ds_attr_cyclone,
                      base_path, analysis_group=None)

    return ds_gd, ds_cyclone_gd, ds_polar_north, ds_cyclone, radii_quad, radii_nmi_quad, percent_valid_quad, \
           tco_radii_quad, tco_radii_nmi_quad, tco_percent_valid_quad


def center_products_analysis(sat_file, sat_file_format, path, center_status, percent_outside, percent_inside_island,
                             percent_non_usable, dist_to_coast, dist_to_acq,
                             lon_center_final, lat_center_final, lons_eye_shape, lats_eye_shape,
                             lon_eye_lwind, lat_eye_lwind, lon_eye_fg, lat_eye_fg, lon_eye_hwnd,
                             lat_eye_hwnd, dist_search_lwind, dist_search_hwind,
                             status, storm_name, track_source_name,
                             atcf_point, atcf_vmax, atcf_rmw, sid, atcf_filename, resolution, eye_ok,
                             rads34, other_track_point, other_track_vmax, other_track_rmw,
                             other_track_source_name, cyclone_direction,
                             cyclone_direction_std, cyclone_speed, cyclone_speed_std, no_netcdf=False,
                             basin=None, mission=None,
                             **kwargs):
    """
    Re-project the SAR data to polar, add variables containing various scalar data (processing intermediate values,
    radii, etc) and saves the xarray Dataset to .nc file.

    Parameters
    ==========
    sat_file: str Path to input SAR file (l2c format or sw format)
    sat_file_format: str format of file pointed by sat_file (can be l2c or sw)

    """

    if sat_file_format == "l2c":
        sat_file_sw = owi.L2CtoLight(sat_file, resolution=resolution, mode="sw")
    else:
        sat_file_sw = sat_file

    # Compute cyclone direction relative to north and cyclone propagation speed.
    direction, dir_std, speed, speed_std = cyclone_direction, cyclone_direction_std, cyclone_speed, cyclone_speed_std

    # Compute R34 from atcf API data
    r34 = np.mean(rads34)

    if other_track_point is not None:
        rmax_c = rmax_ck22(other_track_point, other_track_vmax, r34)
    else:
        rmax_c = rmax_ck22(atcf_point, atcf_vmax, r34)

    ds_gd, ds_cyclone_gd, ds_polar_north, ds_cyclone, radii_quad, radii_nmi_quad, \
    percent_valid_quad, tco_radii_quad, tco_radii_nmi_quad, tco_percent_valid_quad = \
        center_products(sat_file_sw, lon_center_final, lat_center_final, rmax_ck22=rmax_c, resolution=resolution,
                        direction=direction, dir_std=dir_std,
                        speed=speed, speed_std=speed_std, atcf_filename=atcf_filename,
                        atcf_vmax=atcf_vmax, storm_name=storm_name, sid=sid, eye_ok=eye_ok, sat_file_light=True)

    logger.info("Computing shapes from eye center research procedure results, to be added to dataset...")
    # Compute shapely shapes to add them to dataset attributes
    eye_coords = []
    for c_i in range(0, len(lons_eye_shape)):
        eye_coords.append((lons_eye_shape[c_i], lats_eye_shape[c_i]))
    eye_multi = MultiPoint(eye_coords)
    eye_convex = eye_multi.convex_hull
    eye_shape = eye_convex

    # lwind_contour = shape_from_array(msk_lwind, lon_array, lat_array)
    lwind_contour = circle_poly_from_rad(lon_eye_hwnd, lat_eye_hwnd, dist_search_lwind * 1000)

    # hwind_contour = shape_from_array(msk_hwind, lon_array, lat_array)
    hwind_contour = circle_poly_from_rad(lon_eye_fg, lat_eye_fg, dist_search_hwind * 1000)
    logger.info("Done computing shapes.")

    # sar_vmax, sar_rmax_nmi = get_vmax(radii_nmi_quad, percent_valid_quad)

    # Compute vmax
    sar_vmax, sar_rmax, vmax_win_count, vmax_std, vmax_range_dict, rmax_range_dict, \
    vmax_range_win_count, vmax_std_range_dict = compute_vmax(ds_polar_north, rmax_c)

    sar_rmax_nmi = km_to_nmi(sar_rmax)

    # No need to compute quality flags if product has a failed status.
    if status == 0:
        if other_track_point and other_track_vmax and other_track_source_name:
            track_point_quality = other_track_point
            track_vmax_quality = other_track_vmax
            track_source_quality = other_track_source_name
        else:
            track_point_quality = atcf_point
            track_vmax_quality = atcf_vmax
            track_source_quality = track_source_name

        q_flags = quality_flags(sar_rmax_nmi, sar_vmax,
                                lon_center_final, lat_center_final,
                                lons_eye_shape, lats_eye_shape, lon_eye_fg, lat_eye_fg, lon_eye_lwind, lat_eye_lwind,
                                lon_eye_hwnd, lat_eye_hwnd, dist_search_hwind,
                                kwargs["bounding_box"],
                                center_status,
                                track_point=track_point_quality,
                                track_vmax=track_vmax_quality,
                                track_source_name=track_source_quality,
                                rmax_ck22=rmax_c
                                )
        final_flag = q_flags["center_quality_flag"].item()
    else:
        q_flags = None
        final_flag = None

    logger.info("Adding attributes to the 4 datasets...")
    # Add attributes to all 4 dataset
    ds_attr_polar_north_gd = xr.Dataset()
    ds_attr_polar_north_gd = add_attributes(ds_attr_polar_north_gd, sat_file_sw, lon_center_final, lat_center_final,
                                            sar_vmax=sar_vmax, sar_rmax=sar_rmax, vmax_std=vmax_std,
                                            vmax_window_count=vmax_win_count,
                                            vmax_range_dict=vmax_range_dict,
                                            rmax_range_dict=rmax_range_dict,
                                            vmax_range_window_count=vmax_range_win_count,
                                            vmax_std_range_dict=vmax_std_range_dict,
                                            storm_name=storm_name, sid=sid, atcf_filename=atcf_filename,
                                            atcf_vmax=atcf_vmax, atcf_rmw=atcf_rmw, ibtracs_vmax=other_track_vmax,
                                            ibtracs_rmax=other_track_rmw,
                                            rmax_ck22=rmax_c,
                                            basin=basin, mission=mission,
                                            lon_eye_lwind=lon_eye_lwind, lat_eye_lwind=lat_eye_lwind,
                                            lon_eye_hwind=lon_eye_hwnd, lat_eye_hwind=lat_eye_hwnd,
                                            lon_eye_fg=lon_eye_fg, lat_eye_fg=lat_eye_fg,
                                            status=status, distance_to_coast=dist_to_coast,
                                            distance_to_acq=dist_to_acq,
                                            percent_outside=percent_outside,
                                            percent_inside_island=percent_inside_island,
                                            percent_non_usable=percent_non_usable,
                                            eye_shape=eye_shape, lwind_contour=lwind_contour,
                                            hwind_contour=hwind_contour,
                                            radii_quad=radii_quad, radii_nmi_quad=radii_nmi_quad,
                                            percent_valid_quad=percent_valid_quad,
                                            tco_radii_quad=tco_radii_quad, tco_radii_nmi_quad=tco_radii_nmi_quad,
                                            tco_percent_valid_quad=tco_percent_valid_quad,
                                            cyclone_speed=speed, cyclone_speed_std=speed_std,
                                            quality_flags=q_flags)

    ds_attr_cyclone = xr.Dataset()
    ds_attr_cyclone = add_attributes(ds_attr_cyclone, sat_file_sw, lon_center_final, lat_center_final,
                                     sar_vmax=sar_vmax, sar_rmax=sar_rmax, vmax_std=vmax_std,
                                     vmax_window_count=vmax_win_count,
                                     vmax_range_dict=vmax_range_dict,
                                     rmax_range_dict=rmax_range_dict,
                                     vmax_range_window_count=vmax_range_win_count,
                                     vmax_std_range_dict=vmax_std_range_dict,
                                     storm_name=storm_name, sid=sid, atcf_filename=atcf_filename,
                                     atcf_vmax=atcf_vmax, atcf_rmw=atcf_rmw, ibtracs_vmax=other_track_vmax,
                                     ibtracs_rmax=other_track_rmw, rmax_ck22=rmax_c,
                                     basin=basin, mission=mission,
                                     lon_eye_lwind=lon_eye_lwind, lat_eye_lwind=lat_eye_lwind,
                                     lon_eye_hwind=lon_eye_hwnd, lat_eye_hwind=lat_eye_hwnd,
                                     lon_eye_fg=lon_eye_fg, lat_eye_fg=lat_eye_fg,
                                     status=status, distance_to_coast=dist_to_coast,
                                     distance_to_acq=dist_to_acq,
                                     percent_outside=percent_outside, percent_inside_island=percent_inside_island,
                                     percent_non_usable=percent_non_usable,
                                     eye_shape=eye_shape, lwind_contour=lwind_contour, hwind_contour=hwind_contour,
                                     radii_quad=radii_quad, radii_nmi_quad=radii_nmi_quad,
                                     percent_valid_quad=percent_valid_quad,
                                     tco_radii_quad=tco_radii_quad, tco_radii_nmi_quad=tco_radii_nmi_quad,
                                     tco_percent_valid_quad=tco_percent_valid_quad,
                                     cyclone_speed=speed,
                                     cyclone_speed_std=speed_std,
                                     deg_rotation=direction, deg_rotation_std=dir_std,
                                     quality_flags=q_flags)

    logger.info("Done adding attributes to the 4 datasets.")

    if no_netcdf is False:
        base_path = os.path.join(path, f"{os.path.basename(sat_file_sw).replace('.nc', '').replace('-cm-', '-ca-')}_{sid}")
        save_products(ds_gd, ds_polar_north, ds_cyclone_gd, ds_cyclone, ds_attr_polar_north_gd, ds_attr_cyclone,
                      base_path)

    return sar_vmax, sar_rmax, radii_quad, radii_nmi_quad, percent_valid_quad, final_flag
