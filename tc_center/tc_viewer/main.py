import glob
import os
import holoviews as hv
import panel as pn
import param
import xarray as xr
import numpy as np
from collections import OrderedDict
from holoviews.operation.datashader import rasterize

hv.extension('bokeh')
renderer = hv.renderer('bokeh')

def_data_path = "/home/datawork-cersat-public/cache/project/sarwing/users/tcevaer/tc_center/hector/product"
#def_data_path = "/home/datawork-cersat-public/cache/public/ftp/project/cyclobs/data/tc_center_aeqd/product"
#def_data_path = "/home/datawork-cersat-public/cache/project/sarwing/users/tcevaer/tc_center/plots/product"
#def_data_path = "/home/datawork-cersat-public/cache/public/ftp/project/cyclobs/data/tc_center2/product/"

class TCViewer(param.Parameterized):
    data_path = param.String(default=def_data_path)
    _nc_files = glob.glob(os.path.join(def_data_path, "*.nc"))
    _f_names = [os.path.basename(f_path) for f_path in _nc_files]
    idx = 0
    for i in range(len(_f_names)):
        if "_gd" in _f_names[i] and "cyclone" not in _f_names[i]:
            idx = i

    file_select = param.ObjectSelector(default=_f_names[idx], objects=_f_names)
    step_image = param.Integer(default=5)
    step_quiver = param.Integer(default=15)
    _plot_width = 500
    _plot_height = 500


    def quiver_plot(self, x, y, var, mag=1, step=20):
        mag = np.ones(var.shape) * mag
        return hv.VectorField((x[::step, ::step], y[::step, ::step],
                               var[::step, ::step],
                               mag[::step, ::step])).opts(width=self._plot_width, height=self._plot_height)


    def image_plot(self, x, y, var, step=5):
        return hv.Image((x[::step], y[::step], var[::step, ::step])).opts(width=self._plot_width,
                                                                          height=self._plot_height,
                                                                          colorbar=True)


    def quadmesh_plot(self, x, y, var, step=5):
        return hv.QuadMesh((x[::step, ::step], y[::step, ::step], var[::step, ::step]))\
            .opts(width=self._plot_width, height=self._plot_height)


    @param.depends("data_path", watch=True)
    def update_files(self):
        print("update path")
        print(self.data_path)
        _nc_files = glob.glob(os.path.join(self.data_path, "*.nc"))
        _f_names = [os.path.basename(f_path) for f_path in _nc_files]

        self.param["file_select"].objects = _f_names
        self.file_select = _f_names[0]

    @param.depends("file_select", "step_image", "step_quiver")
    def view(self):
        full_file_path = os.path.join(self.data_path, self.file_select)
        gridded = False

        u_quiver = []
        v_quiver = []
        var_quiver = []
        var_img = ["wind_speed"]

        if "_gd" in self.file_select and "cyclone" not in self.file_select:
            gridded = True
            u_quiver.extend(["zonal_wind_streaks_orientation_component"])
            v_quiver.extend(["meridional_wind_streaks_orientation_component"])

        cyclone_ref = False
        if "cyclone" in self.file_select:
            cyclone_ref = True
            u_quiver.extend(["across_wind_streaks_orientation_component", "radial_wind_streaks_orientation_component",
                             "across_wind_parametrized", "radial_wind_parametrized"])
            v_quiver.extend(["along_wind_streaks_orientation_component",
                             "tangential_wind_streaks_orientation_component", "along_wind_parametrized", "tangential_wind_parametrized"])
            #var_quiver.extend(["inflow_angle_wind_streaks_orientation", "inflow_angle_fit"])
            var_img.extend(["inflow_angle_wind_streaks_orientation", "inflow_angle_parametrized"])

        print("Open file", self.file_select)
        ds = xr.open_dataset(full_file_path).squeeze()

        if "_gd" in self.file_select:
            coord_x = ds.coords["x"].values
            coord_y = ds.coords["y"].values
            coord_x_2d, coord_y_2d = np.meshgrid(coord_x, coord_y)
            dim = 1
        else:
            coord_x_2d = ds["lon"].values
            coord_y_2d = ds["lat"].values
            dim = 2

        print("Compute angles")
        angles = OrderedDict()
        for idx_compo in range(len(u_quiver)):
            print(idx_compo)
            u_name = u_quiver[idx_compo]
            v_name = v_quiver[idx_compo]
            print(u_name, v_name)
            angle = (np.pi / 2.) - np.arctan2(ds[u_name].values / 1, ds[v_name].values / 1)
            name_angle = "_".join(u_name.split("_")[0:3]) + " " + "_".join(v_name.split("_")[0:3])
            angles[name_angle] = angle


        col_quiv = pn.Column()
        col_img = pn.Column()

        print("Quiver plots")
        plots_quiver = []
        for ang_name in angles:
            quiv = self.quiver_plot(coord_x_2d, coord_y_2d, angles[ang_name], step=self.step_quiver).opts(title=ang_name)
            col_quiv.append(quiv)

        for quiv_name in var_quiver:
            quiv = self.quiver_plot(coord_x_2d, coord_y_2d, ds[quiv_name].values, step=self.step_quiver)\
                .opts(title=quiv_name)
            col_quiv.append(quiv)

        print("Image plots")
        img_plots = []
        for var_name in var_img:
            if dim == 1:
                p = self.image_plot(coord_x, coord_y, ds[var_name].values, step=self.step_image)
            else:
                print("quadmesh", np.count_nonzero(~np.isnan(ds[var_name].values)))
                p = self.quadmesh_plot(coord_x_2d, coord_y_2d, ds[var_name].values, step=self.step_image)

            col_img.append(p.opts(title=var_name))

        print("Return...")
        return pn.Row(col_img, col_quiv)


viewer = TCViewer()
pn.Row(viewer.param, viewer.view).servable()






