from scipy import ndimage
import cv2
from affine import Affine
from pyproj import Geod
import time
import netCDF4
import numpy as np
import xarray as xr
import rioxarray as rxr
import geo_shapely
from pyproj.transformer import Transformer
from shapely.geometry import Point
import logging

from skimage.measure import block_reduce

logger = logging.getLogger(__name__)


def find_nearest_2d(lon, lat, lonc, latc):
    """
    Search for the index for values that are the closest to lonc and latc in the
    arrays lon and lat.

    Parameters
    ----------
    lon numpy.ndarray 2D numpy array containing longitude
    lat numpy.ndarray 2D numpy array containing latitude
    lonc float corresponding to the searched coordinate in longitude array
    latc float corresponding to the searched coordinate in latitude array

    Returns
    -------
    int, int
        Return the found indexes
    """

    abslat = np.abs(lat - latc)
    abslon = np.abs(lon - lonc)
    c = np.maximum(abslon, abslat)

    ([xloc], [yloc]) = np.where(c == np.min(c))

    return xloc, yloc


def pixel_distance(ds, xloc, yloc):
    lon1 = ds.lon.values[xloc, yloc]
    lat1 = ds.lat.values[xloc, yloc]

    lon2 = ds.lon.values[xloc + 1, yloc]
    lat2 = ds.lat.values[xloc + 1, yloc]
    geod = Geod(ellps='WGS84')
    faz, baz, dist = geod.inv(lon1, lat1, lon2, lat2)

    return dist


def custom_meshgrid(theta, rad, data_shape):
    st = time.time()
    coord_theta = np.zeros(data_shape)
    coord_rad = np.zeros(data_shape)
    for i in range(0, data_shape[0]):
        for j in range(0, data_shape[1]):
            coord_theta[i, j] = theta[i]
            coord_rad[i, j] = rad[j]

    # print("Time taken ", time.time() - st)
    return coord_theta, coord_rad


def var_orientation(var, teta_shape, flip=False):
    if flip:
        var[:] = np.flip(var, axis=0)
    else:
        # Get the index in theta array corresponding to the 90° angle. (as theta[idx_90] =~ 90)
        idx_270 = int(int(teta_shape[0]) * 3/4)

        # Roll the lon and lat arrays of 90° + azim_angle to match the correct orientation.
        # var[:] = np.roll(var, idx_90 + idx_azim, axis=0)
        # Since we used aeqd gridded product before switching to polar, there is no more need
        # to roll using idx_azim.
        var[:] = np.roll(var, idx_270, axis=0)

    return var


def to_aeqd(ds_sw, lon_center, lat_center, x_size, y_size, resolution=1000):
    """
    """

    meta_over = {
        'x': {
            'standard_name': "projection_x_coordinate",
            'long_name': 'Easting',
            'units': 'm'
        },
        'y': {
            'standard_name': "projection_y_coordinate",
            'long_name': 'Northing',
            'units': 'm'
        },
    }
    ds_squeezed = ds_sw.squeeze()

    # Find indexes in data array of the lon/lat center
    xloc, yloc = find_nearest_2d(ds_squeezed.lon, ds_squeezed.lat, lon_center, lat_center)

    lon_center = float(ds_sw.lon[0, xloc, yloc])
    lat_center = float(ds_sw.lat[0, xloc, yloc])


    # projection on aeqd . (will be intermediate crs if crs_out is not None)
    # get aeqd_crs at image center
    crs = geo_shapely.get_aeqd_crs(Point(lon_center, lat_center))

    # convert lon/lat in crs
    fromlonlat = Transformer.from_crs("epsg:4326", crs, always_xy=True)  # always_xy=False is by default in pyproj 6+
    x_proj, y_proj = fromlonlat.transform(ds_sw.lon.values, ds_sw.lat.values)
    y_proj = y_proj.astype(float)
    x_proj = x_proj.astype(float)

    x_center, y_center = fromlonlat.transform(lon_center, lat_center)

    resolution = 1000  # in meters

    # compute grid size
    #x_size = 1000
    #y_size = 1000

    # grid coordinates
    # do not use linspace : do it manualy, so computing inverse is easy
    # x = np.linspace(-(x_size-1)/2.,(x_size-1)/2.,num=x_size) * resolution
    # y = np.linspace(-(y_size-1)/2.,(y_size-1)/2.,num=y_size) * resolution
    x = (np.arange(x_size) - int((x_size - 1) / 2)) * resolution
    y = (np.arange(y_size) - int((y_size - 1) / 2)) * resolution

    # so given an x (resp y), in projection meters, we can have grid index with x / resolution
    x_idx = np.round((x_proj / resolution) + (x_size - 1) / 2).astype(int)
    y_idx = np.round((y_proj / resolution) + (y_size - 1) / 2).astype(int)

    # Filtering out of range indexes
    mask_idx = (x_idx < 0) | (x_idx > x_size - 1) | (y_idx < 0) | (y_idx > y_size - 1)
    x_idx = x_idx[~mask_idx]
    y_idx = y_idx[~mask_idx]

    # add x_proj and y_proj to original ds_sw, in valid coordinates (ie rounded at resolution)
    # ds_sw['x_proj'] = xr.DataArray(x_proj, dims=['time','Y', 'X'])
    # ds_sw['y_proj'] = xr.DataArray(y_proj, dims=['time','Y', 'X'])

    # get lon/lat for each grid cell
    # x_grid,y_grid = np.meshgrid(x,y)
    x_grid, y_grid = np.meshgrid(x, y)
    tolonlat = Transformer.from_crs(crs, "epsg:4326", always_xy=True)
    lon, lat = tolonlat.transform(x_grid, y_grid)

    # add time dim
    lon = lon.reshape([1] + list(lon.shape)).astype(np.float32)
    lat = lat.reshape([1] + list(lat.shape)).astype(np.float32)

    # generate dataarray
    da_lon = xr.DataArray(lon, coords=[ds_sw.time, y, x], dims=['time', 'y', 'x'])
    da_lat = xr.DataArray(lat, coords=[ds_sw.time, y, x], dims=['time', 'y', 'x'])

    # xarray dataset
    ds_gd = xr.Dataset({'lon': da_lon, 'lat': da_lat})
    ds_gd.attrs = ds_sw.attrs

    # if resolution is too small, some gaps will occur in result
    # so we find gaps to be able to fill them later
    np_var = np.zeros(da_lon.shape[1:])
    ##print("shp", np_var.shape)
    # fill with 1 where the data will go
    np_var[y_idx, x_idx] = 1
    # mask is boolean grid where we need to interp
    mask = np_var == 0
    # xx and yy are all index
    xx, yy = np.meshgrid(np.arange(np_var.shape[1]), np.arange(np_var.shape[0]))

    # x_valid and y_valid are index of valid data
    # x_valid = xx[~mask]
    # y_valid = yy[~mask]
    # binary_closing and data to find isolated points that need interpolation
    mask_interp = ndimage.binary_closing(~mask) & mask
    # coordinates where interpolation is needed
    x_interp = xx[mask_interp]
    y_interp = yy[mask_interp]

    # variable loop
    for var in list((set(ds_sw.coords.keys()) | set(ds_sw.keys())).difference(set(['time']))):
        # set up dataarray. dtype is forced to np.float64, because we use Nan for interpolation
        # ds_sw[var].values.dtype
        try:
            fillvalue = ds_sw[var]._FillValue
        except:
            fillvalue = netCDF4.default_fillvals['f8']

        da_var = xr.DataArray(np.full(da_lon.shape, fillvalue, dtype=np.float64), coords=[ds_sw.time, y, x],
                              dims=['time', 'y', 'x'])
        # fill dataarray
        da_var.values[0, y_idx, x_idx] = ds_sw[var].values[~mask_idx]

        # set fillvalue to nan for interpolation
        #da_var.values[da_var.values == fillvalue] = np.nan
        nan_mask = np.isnan(da_var.values)
        da_var.values[nan_mask] = fillvalue  # not really needed

        try:
            da_var.values[0, y_interp, x_interp] = np.nan
            da_var = da_var.ffill(dim='x', limit=2)
            da_var = da_var.ffill(dim='y', limit=2)
        except Exception as e:
            logger.warning("unable to fill nan on %s: %s" % (var,str(e)))

        # restore nan
        da_var.values[nan_mask] = np.nan
        da_var.values[da_var.values == fillvalue] = np.nan

        # set original dtype
        da_var = da_var.astype(ds_sw[var].values.dtype)
        da_var.attrs = ds_sw[var].attrs
        da_var.rio.set_spatial_dims('x', 'y', inplace=True)
        ds_gd[var] = da_var

    encoding = {}
    for var in list(set(ds_gd.coords.keys()) | set(ds_gd.keys())):
        encoding[var] = {}
        if var in meta_over:
            # python 3 only (merge dict )
            # ds_gd[var].attrs = { **ds_sw[var].attrs,**meta_over[var] }
            # python 2 compat:
            ds_gd[var].attrs.update(meta_over[var])
        if '_FillValue' not in ds_gd[var].attrs:
            # disable automatic fillvalue in fileout
            encoding[var].update({'_FillValue': None})

    ds_gd.rio.set_spatial_dims('x', 'y', inplace=True)
    ds_gd.rio.write_crs(crs, inplace=True)

    #try:
    #    ds_gd = ds_gd.interpolate_na(dim="x", method="nearest", bounds_error=False)
    #except ValueError as e:
    #    logger.warning(f"Couldn't interpolate _gd along x axis. x axis len is {ds_gd.coords['x'].values.shape}."
    #                   f"Count nan in x is {np.count_nonzero(np.isnan(ds_gd.coords['x'].values))}"
    #                   f"Count nan in wind_speed is {np.count_nonzero(np.isnan(ds_gd.wind_speed.values))}")
    #try:
    #    ds_gd = ds_gd.interpolate_na(dim="y", method="nearest", bounds_error=False)
    #except ValueError as e:
    #    logger.warning(f"Couldn't interpolate _gd along y axis. y axis len is {ds_gd.coords['y'].values.shape}."
    #                   f"Count nan in y is {np.count_nonzero(np.isnan(ds_gd.coords['y'].values))}"
    #                   f"Count nan in wind_speed is {np.count_nonzero(np.isnan(ds_gd.wind_speed.values))}")
    # encoding={'lat': {'_FillValue': None}, 'lon': {'_FillValue': None}}

    return ds_gd, x_center, y_center


def to_polar(ds, x_center, y_center, lon_center, lat_center, reduce_grid, max_polar_index_radius, theta_len,
             force_pix_dist=None):
    time_dim_val = ds["time"].values
    ds = ds.squeeze()
    data_shape = ds["wind_speed"].shape
    # max_radius = int(np.sqrt(data_shape[0] ** 2 + data_shape[1] ** 2) / 2)

    # Find indexes in data array of the lon/lat center
    yloc, xloc = find_nearest_2d(ds.x, ds.y, x_center, y_center)

    if force_pix_dist is None:
        pix_dist = ds.rio.transform()[0]  # pixel_distance(ds, xloc, yloc)
    else:
        pix_dist = force_pix_dist

    logger.debug(f"Pixel distance: {pix_dist}")
    max_radius = max_polar_index_radius
    logger.debug(f"Max_radius: {max_radius}")

    # Coordinates array
    theta = np.linspace(0, 359, num=theta_len)#num=data_shape[0])
    rads_meter = np.linspace(0, max_radius * pix_dist - pix_dist, num=max_polar_index_radius)#num=data_shape[1])
    #rads_meter = rads * pix_dist

    theta_full = np.linspace(0, 359, num=data_shape[0])
    rads_meter_full = np.linspace(0, max_radius * pix_dist - pix_dist, num=data_shape[1])
    coord_theta, coord_rad = np.meshgrid(theta_full, rads_meter_full, indexing="ij")

    logger.debug(f"lon/lat center: {lon_center}, {lat_center}")

    geod = Geod(ellps='WGS84')
    lons_polar, lats_polar, baz = geod.fwd(np.zeros_like(ds.lon.values) + lon_center,
                                           np.zeros_like(ds.lat.values) + lat_center,
                                           coord_theta, coord_rad)

    lons_polar = block_reduce(lons_polar, block_size=reduce_grid, func=np.mean, cval=np.mean(lons_polar))
    lats_polar = block_reduce(lats_polar, block_size=reduce_grid, func=np.mean, cval=np.mean(lats_polar))
    lons_polar = var_orientation(lons_polar, theta.shape, flip=True)
    lats_polar = var_orientation(lats_polar, theta.shape, flip=True)

    #lons_polar = cv2.linearPolar(ds.lon.values, (int(yloc), int(xloc)), max_radius,
    #                                cv2.INTER_NEAREST + cv2.WARP_FILL_OUTLIERS)
    #lons_polar = var_orientation(lons_polar, theta.shape)
    #lats_polar = cv2.linearPolar(ds.lat.values, (int(yloc), int(xloc)), max_radius,
    #                                cv2.INTER_NEAREST + cv2.WARP_FILL_OUTLIERS)
    #lats_polar = var_orientation(lats_polar, theta.shape)

    # print("bounds", ds.lon.isel(x=0, y=0).item(), ds.lat.isel(x=0, y=0).item(),
    #       ds.lon.isel(x=-1, y=0).item(), ds.lat.isel(x=-1, y=0).item(),
    #       ds.lon.isel(x=-1, y=-1).item(), ds.lat.isel(x=-1, y=-1).item(),
    #       ds.lon.isel(x=0, y=-1).item(), ds.lat.isel(x=0, y=-1).item())
    x_2d, y_2d = np.meshgrid(ds.x, ds.y)

    x_polar = cv2.linearPolar(x_2d, (int(yloc), int(xloc)), max_radius,
                              cv2.INTER_NEAREST + cv2.WARP_FILL_OUTLIERS)

    x_polar = block_reduce(x_polar, block_size=reduce_grid, func=np.mean, cval=np.mean(x_polar))
    x_polar = var_orientation(x_polar, theta.shape)

    y_polar = cv2.linearPolar(y_2d, (int(yloc), int(xloc)), max_radius,
                              cv2.INTER_NEAREST + cv2.WARP_FILL_OUTLIERS)
    y_polar = block_reduce(y_polar, block_size=reduce_grid, func=np.mean, cval=np.mean(y_polar))
    y_polar = var_orientation(y_polar, theta.shape)

    # Build an array which takes negative values at location where wind_speed values are valid.
    nan_mask_array = np.where(np.isnan(ds["wind_speed"]), 10, -50)
    # Transform that array to polar
    nan_mask_array_polar = cv2.linearPolar(nan_mask_array, (int(yloc), int(xloc)), max_radius,
                                           cv2.INTER_NEAREST + cv2.WARP_FILL_OUTLIERS)
    # Retrieve indices where valid data is located
    valid_indices = np.where(nan_mask_array_polar < 0)

    dims = ["theta", "rad"]

    # Computing polar projection for all variables
    vars_polar = {}
    for var_name in list(ds.data_vars.keys()):
        if var_name != "lon" and var_name != "lat":
            try:
                fillvalue = ds[var_name]._FillValue
            except:
                fillvalue = netCDF4.default_fillvals['f8']

            logger.debug(f"Fill value for {var_name} is {fillvalue}")

            var_polar = cv2.linearPolar(ds[var_name].values, (int(yloc), int(xloc)), max_radius,
                                        cv2.INTER_NEAREST + cv2.WARP_FILL_OUTLIERS)
            nan_ar = np.empty(var_polar.shape)
            nan_ar[:] = np.nan
            # Keeping only valid indices
            nan_ar[valid_indices] = var_polar[valid_indices]
            nan_ar = block_reduce(nan_ar, block_size=reduce_grid, func=np.mean, cval=np.mean(nan_ar))
            nan_ar = var_orientation(nan_ar, theta.shape)
            vars_polar[var_name] = (dims, nan_ar, ds[var_name].attrs)

    vars_polar["lon"] = (dims, lons_polar, ds.lon.attrs)
    vars_polar["lat"] = (dims, lats_polar, ds.lat.attrs)
    vars_polar["x"] = (dims, x_polar, ds.x.attrs)
    vars_polar["y"] = (dims, y_polar, ds.y.attrs)

    polar_ds = xr.Dataset(data_vars=vars_polar, coords={
        "rad": (["rad"], rads_meter),
        "theta": (["theta"], theta),
    })

    polar_ds = polar_ds.expand_dims("time").assign_coords(time=("time", time_dim_val))

    # Downsampling to get a len(theta) = 360 and len(rad) = 500
    #polar_ds = polar_ds.coarsen(theta=3, boundary="exact").mean()
    #polar_ds = polar_ds.coarsen(rad=2, boundary="exact").mean()

    polar_ds = polar_ds.transpose("time", "rad", "theta")

    return polar_ds


def polar2cart(r: np.ndarray, theta: np.ndarray, center: list):
    x = r * np.cos(theta) + center[0]
    y = r * np.sin(theta) + center[1]
    return x, y


def to_cylone_dir(polar_ds: xr.Dataset, direction_angle: float, south: bool,
                  vars_to_diff=["wind_streaks_orientation", "wind_from_direction"]):
    polar_ds = polar_ds.roll(theta=-round(direction_angle), roll_coords=False)

    for var_diff in vars_to_diff:
        if var_diff in polar_ds.data_vars:
            nan_mask = np.isnan(polar_ds[var_diff].values)
            if south:
                polar_ds[var_diff][:] = np.mod(np.where(nan_mask, np.nan,
                                                    - polar_ds[var_diff].values - round(direction_angle)), 360)
            else:
                polar_ds[var_diff][:] = np.mod(np.where(nan_mask, np.nan,
                                                    polar_ds[var_diff].values + round(direction_angle)), 360)

    return polar_ds


def rotate_dataset(ds_gd: xr.Dataset, angle: float, vars_to_diff=["wind_streaks_orientation", "wind_from_direction"],
                   south=False):
    grid_size = ds_gd.coords["x"].values.shape[0]

    time_dim_val = ds_gd["time"].values
    ds_cyclone_gd = ds_gd.copy(deep=True).squeeze()
    orig_x = ds_gd.coords["x"].values
    orig_y = ds_gd.coords["y"].values

    #Creating grid of index
    x = np.arange(grid_size)
    y = np.arange(grid_size)
    # Making 2D grid, because we can only rotate 2D grid
    x_m, y_m = np.meshgrid(x, y)


    # Rotation the index grid
    transform = ((Affine.translation(500, 500) * Affine.rotation(angle)) * Affine.translation(-500, -500))
    x_r, y_r = ~transform * (x_m, y_m)

    # Rounding to int (because indexes cannot be float)
    x_r = np.rint(x_r).astype(int)
    y_r = np.rint(y_r).astype(int)

    # Filtering out of range indexes
    mask_idx = (x_r < 0) | (x_r > grid_size - 1) | (y_r < 0) | (y_r > grid_size - 1)
    x_r = x_r[~mask_idx]
    y_r = y_r[~mask_idx]

    # Assigning data using numpy advanced indexing.
    # The new array is indexed using rotated index grid, while the data is retrieved using the non-rotated index grid.
    # This performs the data rotation
    vars = {}
    for var_name in ds_cyclone_gd.data_vars:
        ar = np.empty(x_m.shape)
        ar[:] = np.nan
        # Advanced indexing applying the rotation to the data
        ar[x_m[~mask_idx], y_m[~mask_idx]] = ds_cyclone_gd[var_name].values[x_r, y_r]

        if south:
            ar = np.flip(ar, axis=1)

        # Preparing data tuple for creating xr.Dataset
        vars[var_name] = (["x", "y"], ar, ds_cyclone_gd[var_name].attrs)

    ds_cyclone_gd = xr.Dataset(data_vars=vars, coords={
        "x": (["x"], orig_x),
        "y": (["y"], orig_y),
    })

    for var_diff in vars_to_diff:
        if var_diff not in ds_cyclone_gd.data_vars:
            continue
        nan_mask = np.isnan(ds_cyclone_gd[var_diff].values)
        if south:
            ds_cyclone_gd[var_diff][:] = np.mod(np.where(nan_mask, np.nan, - ds_cyclone_gd[var_diff].values - angle), 360)
        else:
            ds_cyclone_gd[var_diff][:] = np.mod(np.where(nan_mask, np.nan, ds_cyclone_gd[var_diff].values + angle), 360)

    ds_cyclone_gd = ds_cyclone_gd.expand_dims("time").assign_coords(time=("time", time_dim_val))

    return ds_cyclone_gd


def to_cyclone_cartesian(ds_cyclone: xr.Dataset, x_center: float, y_center: float, x_grid: np.ndarray,
                         y_grid: np.ndarray):
    theta, rad = np.meshgrid(ds_cyclone.coords["theta"].values, ds_cyclone.coords["rad"].values, indexing="ij")
    x, y = polar2cart(rad, theta, [x_center, y_center])
    # ds = ds.rename_dims({"theta": "x", "rad": "y"})

    vars = {}
    for var_name in ds_cyclone.data_vars:
        ar = np.empty(x.shape)
        ar[:] = np.na
        ar[x.flat, y.flat] = ds_cyclone[var_name].values[rad.flat, theta.flat]
        vars[var_name] = np.empty(x.shape)
        vars[var_name][:] = np.nan
        vars[var_name][x.flat, y.flat] = (["x", "y"], ar, ds_cyclone[var_name].attrs)
        # vars[var_name] = ds[var_name].assign_coords(x=(["x_dim", "y_dim"], x), y=(["x_dim", "y_dim"], y))

    ds_cyclone_cart = xr.Dataset(data_vars=vars, coords={
        "x": (["x"], x_grid),
        "y": (["y"], y_grid),
    })

    # ds_cyclone_cart = ds_cyclone_cart.sortby(["x", "y"])

    return ds_cyclone_cart


def to_north_ref(polar_ds):
    # The goal is to roll the "theta" array so that the index where the 90° value is will then contain
    # the azim_heading° value.

    # Getting azimuth to north for center pixel
    azim_heading = polar_ds.heading_angle.sel(theta=0, rad=0)

    t_shp = polar_ds.theta.shape
    # Get the index in theta array corresponding to the 90° angle. (as theta[idx_90] =~ 90)
    idx_90 = int(t_shp[0] / 4)

    # Getting the closest index to the azim_heading azimuth
    idx_azim = np.searchsorted(polar_ds.theta.values, azim_heading, side="left")

    # Compute how many rolls to make so that the value at idx_azim will then be at idx_90
    rolling = (t_shp[0] - idx_azim) + idx_90

    # Roll the theta array
    polar_ds.theta.values[:] = np.roll(polar_ds.theta.values, rolling)

    # Roll the whole ds back so that theta[0] = 0
    polar_ds = polar_ds.roll(theta=-rolling, roll_coords=True)

    return polar_ds
