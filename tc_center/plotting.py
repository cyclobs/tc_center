import numpy as np
from rasterio import features
import owi
import os
import xarray as xr
import holoviews as hv
import geoviews as gv
import copy

from matplotlib import pyplot as plt
from shapely.geometry import LineString
import geoviews.tile_sources as gts
from bokeh.resources import INLINE
import matplotlib.cm as cm
import colormap_ext
import math

matplot = False
#if matplot:
#    hv.extension('matplotlib')
#else:
#    hv.extension('bokeh')


def contour_indices(arr, lon):
    """
    Retrieve 2D indexes of first shape found in arr
    """
    arr[arr.mask] = 0

    shapes = next(features.shapes(arr))
    coords = shapes[0]['coordinates'][0]
    idx = np.array(coords, dtype='i4')

    # Deleting indices that are outside of lon/lat length
    sup = np.where(idx[:, 1] >= lon.shape[0])[0]
    idx0 = np.delete(idx[:, 1], sup, 0)
    idx1 = np.delete(idx[:, 0], sup, 0)

    sup = np.where(idx1 >= lon.shape[1])[0]
    idx0 = np.delete(idx0, sup, 0)
    idx1 = np.delete(idx1, sup, 0)

    zeros = np.where(idx0 == 0)[0]
    idx0 = np.delete(idx0, zeros, 0)
    idx1 = np.delete(idx1, zeros, 0)

    return idx0, idx1


def create_control_plot(sat_file, lon, lat, lons_eye_shape, lats_eye_shape, lon_eye_final, lat_eye_final,
                        lon_eye_old, lat_eye_old, lon_eye_fg, lat_eye_fg, lon_eye_hwnd, lat_eye_hwnd,
                        i_hwnd, j_hwnd, lwind_mask, hwind_mask, cyclone_name, atcf_vmax, track_gdf):
    sat_date = owi.getDateFromFname(os.path.basename(sat_file))
    high_cmap = cm.get_cmap("high_wind_speed")
    sat_file = owi.L2CtoLight(sat_file, mode="ll_gd")
    ds = xr.open_dataset(sat_file)
    min_max_lon = (float(ds["lon"].min()) - 1, float(ds["lon"].max()) + 1)
    min_max_lat = (float(ds["lat"].min()) - 1, float(ds["lat"].max()) + 1)

    vdim = hv.Dimension("wind_speed", range=(0, 80))
    gvds = gv.Dataset(ds["wind_speed"].squeeze(), vdims=[vdim])
    img = gvds.to(gv.Image, ['lon', 'lat']).opts(cmap=high_cmap, tools=["hover"], colorbar=True)

    fg_p = gv.Points([(lon_eye_fg, lat_eye_fg)], label="First guess (from interpolated ATCF track)")\
        .opts(size=10, color="green", line_color="black", line_width=2, tools=["hover"])
    hwnd_p = gv.Points([(lon_eye_hwnd, lat_eye_hwnd)], label="High wind speed barycenter")\
        .opts(size=10, color="#EAE8FF", line_color="black", line_width=2, tools=["hover"])
    found_p = gv.Points([(lon_eye_old, lat_eye_old)], label="Low wind speed barycenter").opts(size=10, color="#724CF9",
                                                                                              line_color="black",
                                                                                              line_width=2,
                                                                                              tools=["hover"])
    final_p = gv.Points([(lon_eye_final, lat_eye_final)], label="Center of eye shape")\
        .opts(size=10, color="black", marker="s", line_color="grey", line_width=2, tools=["hover"])

    hwnd_area = gv.Points(list(zip(lon[i_hwnd, j_hwnd], lat[i_hwnd, j_hwnd])), label="High wind speed area")\
        .opts(color="grey", alpha=0.2)
    eye = gv.Path((lons_eye_shape, lats_eye_shape), label="Eye shape")\
        .opts(line_width=4, line_color="black", show_legend=True)

    lwind_mask[lwind_mask.mask] = 0

    idx0, idx1 = contour_indices(hwind_mask, lon)

    # Getting lon/lat for hwind contour
    lon_hwind = lon[idx0, idx1]
    lat_hwind = lat[idx0, idx1]

    high_contour = gv.Path((lon_hwind, lat_hwind), label="High wind speed research area")\
        .opts(line_width=3, show_legend=True, line_color="grey")

    idx0, idx1 = contour_indices(lwind_mask, lon)

    lon_lwind = lon[idx0, idx1]
    lat_lwind = lat[idx0, idx1]

    low_contour = gv.Path((lon_lwind, lat_lwind), label="Low wind speed research area").opts(line_width=3,
                                                                                             show_legend=True,
                                                                                        line_color="#EAE8FF")
    track_plot = gv.Points(track_gdf, vdims=["wind_speed (m/s)", "date"], label="Cyclone ATCF interpolated track").opts(
        color="wind_speed (m/s)", cmap=high_cmap, size=10, tools=["hover"], clim=(0, 80), line_color="black",
        line_width=1.5)

    track_path = copy.copy(track_gdf)

    # Converting geom Points to LineString
    track_path["geometry"] = [
        LineString([val, track_gdf.loc[idx + 1, "geometry"]]) if idx < len(track_gdf["geometry"]) - 1 else LineString([val, val])
        for idx, val in track_gdf["geometry"].iteritems()]

    # Removing last line because its geom is useless
    track_path = track_path.iloc[0:-1]

    track_path_plot = gv.Path(track_path, vdims=["wind_speed (m/s)", "date"],
                              label="Cyclone ATCF interpolated track").opts(
        color="black", tools=["hover"], line_width=3, show_legend=True)

    t_plot = hv.Overlay(track_path_plot * track_plot)

    p = (gts.EsriImagery() * img * t_plot * hwnd_area * high_contour * low_contour *
         eye * fg_p * hwnd_p * found_p * final_p).opts(tools=["hover", "save"], legend_position='right',
                                                       title=f"{cyclone_name}, ATCF VMAX: {atcf_vmax} m/s. "
                                                             f"Acquisition at {sat_date.strftime('%Y-%m-%d %H:%M:%S')}",
                                                       show_grid=True, active_tools=['wheel_zoom'])

    p = p.redim.range(Longitude=min_max_lon, Latitude=min_max_lat)
    if not matplot:
        p.opts(width=900, height=500)
    else:
        p.opts(aspect=1, fig_inches=10, fig_bounds=(0, 0, 1, 1))

    return p


def save_plot(sat_file, path, plot, prepend="", backend="bokeh"):
    hv.extension(backend)
    hv.save(plot, os.path.join(path, f'{prepend}_{os.path.basename(sat_file)}.html'), resources=INLINE)
    if backend == "bokeh":
        hv.save(plot, os.path.join(path, f'{prepend}_{os.path.basename(sat_file)}.png'), resources=INLINE, fmt="png")
    elif backend == "matplotlib":
        hv.save(plot, os.path.join(path, f'{prepend}_{os.path.basename(sat_file)}.png'), resources=INLINE, fmt="png")


def degrees2meters(lon, lat):
        x = lon * 20037508.34 / 180
        y = math.log(math.tan((90 + lat) * math.pi / 360)) / (math.pi / 180)
        y = y * 20037508.34 / 180
        return x, y


def control_plot_from_shp(sat_file_ll, eye_shape, center_pt, fg_pt, hwind_pt, lwind_pt, hwind_research, lwind_research,
                          atcf_vmax, track_gdf, cyclone_name, backend, ibtracs_pt=None,
                          opts={"width": 2600, "height": 2200, "legend_position": 'right', "fontscale": 4}, step=3):
    sat_date = owi.getDateFromFname(os.path.basename(sat_file_ll))
    high_cmap = cm.get_cmap("high_wind_speed")
    #sat_file = owi.L2CtoLight(sat_file, mode="ll_gd")
    ds = xr.open_dataset(sat_file_ll)
    min_max_lon = (round(float(ds["lon"].min()), 3) - 3, round(float(ds["lon"].max()), 3) + 3)
    min_max_lat = (round(float(ds["lat"].min()), 3) - 3, round(float(ds["lat"].max()), 3) + 3)
    min_lon_meter, min_lat_meter = degrees2meters(min_max_lon[0], min_max_lat[0])
    max_lon_meter, max_lat_meter = degrees2meters(min_max_lon[1], min_max_lat[1])

    vdim = hv.Dimension("wind_speed", range=(0, 80))
    gvds = gv.Dataset(ds["wind_speed"].squeeze()[::step, ::step], vdims=[vdim])
    img = gvds.to(gv.Image, ['lon', 'lat'])
    if backend == "bokeh":
        img.opts(cmap=high_cmap, colorbar=True, tools=["hover"])
    elif backend == "matplotlib":
        img.opts(cmap=high_cmap)

    optional_plots = []

    fg_p = gv.Points([fg_pt], label="First guess (from interpolated ATCF track)")\
        .opts(size=10, color="green", line_color="black", line_width=2, tools=["hover"])
    hwnd_p = gv.Points([hwind_pt], label="High wind speed barycenter")\
        .opts(size=10, color="#EAE8FF", line_color="black", line_width=2, tools=["hover"])
    found_p = gv.Points([lwind_pt], label="Low wind speed barycenter").opts(size=10, color="#724CF9",
                                                                                              line_color="black",
                                                                                              line_width=2,
                                                                                              tools=["hover"])
    final_p = gv.Points([center_pt], label="Center of eye shape")\
        .opts(size=10, color="black", marker="s", line_color="grey", line_width=2, tools=["hover"])

    if ibtracs_pt is not None:
        ibtracs_p = gv.Points([ibtracs_pt], label="ibtracs colocated point")\
            .opts(size=12, color="orange", line_color="black", line_width=2, tools=["hover"])
        optional_plots.append(ibtracs_p)

    #hwnd_area = gv.Points(list(zip(lon[i_hwnd, j_hwnd], lat[i_hwnd, j_hwnd])), label="High wind speed area")\
    #    .opts(color="grey", alpha=0.2)
    eye = gv.Path(eye_shape, label="Eye shape")\
        .opts(line_width=4, line_color="black", show_legend=True)

    if not hwind_research.is_empty:
        high_contour = gv.Path(hwind_research, label="High wind speed research area")\
            .opts(line_width=3, show_legend=True, line_color="grey")
        optional_plots.append(high_contour)

    if not lwind_research.is_empty:
        low_contour = gv.Path(lwind_research, label="Low wind speed research area").opts(line_width=3,
                                                                                                 show_legend=True,
                                                                                            line_color="#EAE8FF")
        optional_plots.append(low_contour)

    track_plot = gv.Points(track_gdf, vdims=["wind_speed (m/s)", "date"], label="Cyclone ATCF interpolated track").opts(
        color="wind_speed (m/s)", cmap=high_cmap, size=10, tools=["hover"], clim=(0, 80), line_color="black",
        line_width=1.5)

    track_path = copy.copy(track_gdf)

    # Converting geom Points to LineString
    track_path["geometry"] = [
        LineString([val, track_gdf.loc[idx + 1, "geometry"]]) if idx < len(track_gdf["geometry"]) - 1 else LineString([val, val])
        for idx, val in track_gdf["geometry"].iteritems()]

    # Removing last line because its geom is useless
    track_path = track_path.iloc[0:-1]

    track_path_plot = gv.Path(track_path, vdims=["wind_speed (m/s)", "date"],
                              label="Cyclone ATCF interpolated track").opts(
        color="black", tools=["hover"], line_width=3, show_legend=True)

    t_plot = hv.Overlay(track_path_plot * track_plot)

    if len(optional_plots) == 0:
        p = (gts.EsriImagery() * img * t_plot *
             eye * fg_p * hwnd_p * found_p * final_p).opts(tools=["hover", "save"],
                                                           title=f"{cyclone_name}, ATCF VMAX: {atcf_vmax} m/s. "
                                                                 f"Acquisition at {sat_date.strftime('%Y-%m-%d %H:%M:%S')}",
                                                           show_grid=True, active_tools=['wheel_zoom'],
                                                           xlim=(min_lon_meter, max_lon_meter),
                                                           ylim=(min_lat_meter, max_lat_meter),
                                                           legend_position="right")
    else:
        p = (gts.EsriImagery() * img * t_plot *
             eye * fg_p * hv.Overlay(optional_plots) * hwnd_p * found_p * final_p).opts(tools=["hover", "save"],
                                                           title=f"{cyclone_name}, ATCF VMAX: {atcf_vmax} m/s. "
                                                                 f"Acquisition at {sat_date.strftime('%Y-%m-%d %H:%M:%S')}",
                                                           show_grid=True, active_tools=['wheel_zoom'],
                                                           xlim=(min_lon_meter, max_lon_meter),
                                                           ylim=(min_lat_meter, max_lat_meter),
                                                           legend_position="right")

    #p = p.redim.range(Longitude=min_max_lon, Latitude=min_max_lat)
    if not matplot:
        p.opts(**opts)
    else:
        p.opts(aspect=1, fig_inches=10, fig_bounds=(0, 0, 1, 1), legend_position='right')

    return p


def save_center_plot(path, sat_file, lon, lat, windspd_eye_masked, cm_cyc, lons_eye_shape, lats_eye_shape, lon_eye,
                     lat_eye, lon_eye_old, lat_eye_old, lon_eye_fg, lat_eye_fg, lon_eye_hwnd, lat_eye_hwnd):
    fig, ax = plt.subplots()
    ax.pcolormesh(lon, lat, windspd_eye_masked, cmap=cm_cyc)
    ax.plot(lons_eye_shape, lats_eye_shape, 'k', label='eye shape')
    ax.plot(lon_eye, lat_eye, 'og', label='center')
    ax.plot(lon_eye_old, lat_eye_old, 'or', label='center')
    ax.plot(lon_eye_hwnd, lat_eye_hwnd, 'oy', label='center')
    ax.plot(lon_eye_fg, lat_eye_fg, 'ob', label='center')
    ax.set_xlim((lon_eye - 2., lon_eye + 2.))
    ax.set_ylim((lat_eye - 2., lat_eye + 2.))

    plt.savefig(os.path.join(path, f'{os.path.basename(sat_file)}.png'))
