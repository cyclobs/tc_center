import logging
import math

from pyproj import Geod
from shapely.geometry import Polygon, LineString, MultiPoint
from shapely.wkt import loads
from shapely.validation import make_valid
from xarray import DataArray
from tc_center.functions import land_mask, shape_from_array, CenterStatus, circle_poly_from_rad
from cyclobs_utils import rmax_ck22, nmi_to_km, ms_to_knots
from shapely.ops import polygonize, nearest_points
from shapely.geometry import Point
import pandas as pd
import owi
import os
import numpy as np

logger = logging.getLogger(__name__)


def track_quality_flag(track_point, lon_center, lat_center, rmax_ck, source):
    ibcenter = track_point
    logger.debug(f"track point loc : {ibcenter.x}/{ibcenter.y}")

    geod = Geod(ellps='WGS84')

    faz, baz, dist = geod.inv(ibcenter.x, ibcenter.y, lon_center,
                              lat_center)
    dist_rmax = round(dist / 1000, 2) / rmax_ck  # row_api["atcf_rmw"]
    logger.debug(f"rmax_ck : {rmax_ck}. dist_rmax: {dist_rmax}")
    if dist_rmax >= 1.1:
        dist_rmax = 2
    elif dist_rmax >= 0.8:
        dist_rmax = 1
    else:
        dist_rmax = 0

    return {"track_flag": DataArray(data=dist_rmax, attrs={"long_name": "center quality flag relative to TC track",
                                                           "description": "Indicates the quality of the computed"
                                                                          f" eye center relative to {source}. "
                                                                          "To compute that flag, the distance (d)"
                                                                          f"between the {source} center and"
                                                                          "this product center is computed. "
                                                                          "If (d) divided by rmax_ck22 is > 1.1,"
                                                                          " the flag is set to 2. If between 1.1 "
                                                                          "and 0.8 the flag is set to 1. Below"
                                                                          " 0.8 it is set to 0. Track source used "
                                                                          "for this flag can be IBTrACS or ATCF.",
                                                           "flag_values": "0 1 2",
                                                           "flag_meanings": "good warning bad"})}


def land_research_area_flag(lon_eye_fg, lat_eye_fg, dist_search_hwind):
    land_prep, land_non_prep = land_mask()

    hwind_contour = circle_poly_from_rad(lon_eye_fg, lat_eye_fg, dist_search_hwind * 1000)
    #hwind_contour = shape_from_array(msk_hwind, lon_array, lat_array)

    # It looks like the hwind contour sometimes fails to be converted to a shape.
    # Example : https://cyclobs.ifremer.fr/static/sarwing_datarmor/processings/c39e79a/default/sentinel-1a/L2C/IW/S1A_IW_OWIH_1S/2021/300/S1A_IW_OWIH_CC_20211027T222538_20211027T222653_040311_04C6E8/s1a-iw-owi-cc-20211027t222538-20211027t222653-040311-04C6E8.nc
    #FIXME Investigate to fix this issue.
    # Meanwhile in these cases, the flag is deactivated.
    if not hwind_contour.is_empty:
        hwind_contour = Polygon(hwind_contour)
        hwind_contour = make_valid(hwind_contour)

        land_intersection = hwind_contour.intersection(land_non_prep)
        percent_land = (land_intersection.area / hwind_contour.area) * 100

        if percent_land >= 3:
            flag_val = 2
        elif percent_land >= 0.5:
            flag_val = 1
        else:
            flag_val = 0
    else:
        flag_val = 0
        logger.error("The hwind contour has failed to be converted to a shape. The land_flag will be set to 0,"
                     "but this may be incorrect.")

    return {"land_flag": DataArray(data=flag_val, attrs={"long_name": "center quality flag considering land",
                                                         "description": "Indicates the quality of the computed"
                                                                        " eye center considering land presence near"
                                                                        " the eye research area. More precisely,"
                                                                        " if 3% or more of the area inside the high "
                                                                        "wind speed"
                                                                        " research area is land, the flag is set to 2,"
                                                                        " if"
                                                                        "between 3% and 0.5%, the flag is set to 1, "
                                                                        "below 0.5% the flag is set to 0.",
                                                         "flag_values": "0 1 2",
                                                         "flag_meanings": "good warning bad"})}


def dist_low_high_barycenter(lon_eye_lwind, lat_eye_lwind, lon_eye_hwnd, lat_eye_hwnd, rmax_ck):
    geod = Geod(ellps='WGS84')
    faz, baz, dist = geod.inv(lon_eye_lwind, lat_eye_lwind, lon_eye_hwnd, lat_eye_hwnd)

    val = (dist / 1000) / rmax_ck
    if val >= 1.7:
        flag_val = 2
    elif val >= 1.6:
        flag_val = 1
    else:
        flag_val = 0

    return {"lwind_hwind_flag": DataArray(data=flag_val, attrs={"long_name": "center quality flag considering high and "
                                                                             "low wind speed barycenters",
                                                                "description": "Indicates the quality of the computed"
                                                                               "eye center considering the distance "
                                                                               "between the high and low wind speed "
                                                                               "barycenters. "
                                                                               "The distance (d) between the low and "
                                                                               "high wind "
                                                                               "speed barycenter is computed and then "
                                                                               "divided by rmax_ck22. If that value"
                                                                               " is higher than 1.7, the flag is set to"
                                                                               " 2. If between 1.6 and 1.7, the flag"
                                                                               " is set to 1. If below 1.6, it is set to"
                                                                               " 0.",
                                                                "flag_values": "0 1 2",
                                                                "flag_meanings": "good warning bad"})}


def eye_shape_max_radius_km(eye_shape):
    geod = Geod(ellps='WGS84')

    # https://gis.stackexchange.com/questions/295874/getting-polygon-breadth-in-shapely
    # get minimum bounding box around polygon
    box = eye_shape.minimum_rotated_rectangle
    # get coordinates of polygon vertices
    x, y = box.exterior.coords.xy

    pt0 = Point(x[0], y[0])
    pt1 = Point(x[1], y[1])
    pt2 = Point(x[2], y[2])
    dist1 = pt0.distance(pt1)
    dist2 = pt1.distance(pt2)

    if dist1 > dist2:
        faz, baz, eye_max_length = geod.inv(pt0.x, pt0.y, pt1.x, pt1.y)
    else:
        faz, baz, eye_max_length = geod.inv(pt1.x, pt1.y, pt2.x, pt2.y)

    # Divide by 2 to get radius
    eye_max_length = eye_max_length / 2

    return eye_max_length


def eye_length(eye_shape, rmax_ck):
    eye_max_length = eye_shape_max_radius_km(eye_shape)

    eye_length_ck22 = round((eye_max_length / 1000) / rmax_ck, 2)

    # yellow_below": 0.2, "yellow_above": 1, "red_below": 0.17, "red_above": 1.2
    if eye_length_ck22 <= 0.17 or eye_length_ck22 >= 1.2:
        flag_val = 2
    elif eye_length_ck22 <= 0.2 or eye_length_ck22 >= 1:
        flag_val = 1
    else:
        flag_val = 0

    return {"eye_length_flag": DataArray(data=flag_val,
                                         attrs={"long_name": "center quality flag considering eye shape max length.",
                                                "description": "Indicates the quality of the computed"
                                                               " eye center considering the computed eye shape "
                                                               "maximum length. More precisely,"
                                                               " if the max eye shape length divided by "
                                                               "rmax_ck22 is < to 0.17 or > 1.2, the flag"
                                                               " is set to 2, if it is "
                                                               " research area is land, the flag is set to 2,"
                                                               " if it is < 0.2 or > 1, it is set to 1. Else,"
                                                               "it is set to 0.",
                                                "flag_values": "0 1 2",
                                                "flag_meanings": "good warning bad"})}


def dist_lwind(lon_lwind, lat_lwind, lon_center, lat_center, rmax_ck):
    geod = Geod(ellps='WGS84')
    faz, baz, dist = geod.inv(lon_lwind, lat_lwind, lon_center, lat_center)

    dist = dist / 1000
    dist_rmax = dist / rmax_ck

    if dist_rmax >= 0.8:
        flag_val = 2
    elif dist_rmax >= 0.6:
        flag_val = 1
    else:
        flag_val = 0

    return {"dist_lwind_flag": DataArray(data=flag_val,
                                         attrs={"long_name": "center quality flag considering low wind barycenter.",
                                                "description": "Indicates the quality of the computed"
                                                               " eye center considering the distance (d) between the "
                                                               "low wind barycenter and the final eye center. "
                                                               "More precisely,"
                                                               " if that distance (d) / rmax_ck22 is >= 0.8 "
                                                               "the flag is set to 2. If the distance (d) is above 0.6 "
                                                               "and below 0.8, the flag is set to 1. Else, it is set "
                                                               "to 0.",
                                                "flag_values": "0 1 2",
                                                "flag_meanings": "good warning bad"})}


def eye_contained(bounding_box, lon_center, lat_center):
    eye_pt = Point(lon_center, lat_center)
    if bounding_box.contains(eye_pt):
        flag_val = 0
    else:
        flag_val = 2

    return {"eye_contained_flag": DataArray(data=flag_val,
                                            attrs={"long_name": "center quality flag considering eye location and "
                                                                "acquisition bounding box.",
                                                   "description": "If the eye center is outside the acquisition bounding "
                                                                  "box, the flag is to set 2, if it is inside, "
                                                                  "the flag is set to 0.",
                                                   "flag_values": "0 2",
                                                   "flag_meanings": "good bad"})}


def distance_center_bbox(bbox, lon_center, lat_center):
    geod = Geod(ellps='WGS84')

    eye_pt = Point(lon_center, lat_center)
    p1, p2 = nearest_points(bbox.exterior, eye_pt)
    faz_bbox, baz_bbox, distance_bbox = geod.inv(eye_pt.x, eye_pt.y, p1.x, p1.y)

    distance_bbox = distance_bbox / 1000
    # {"title": "Distance to bbox", "yellow_below": 30, "yellow_above": 1000,
    # "red_below": 15, "red_above": 1000},

    if distance_bbox <= 25:
        flag_val = 2
    elif distance_bbox <= 40:
        flag_val = 1
    else:
        flag_val = 0

    return {"distance_center_bbox_flag": DataArray(data=flag_val,
                                                   attrs={
                                                       "long_name": "center quality flag considering distance between "
                                                                    "eye_center and acquisition bounding box.",
                                                       "description": "If the distance (d) between the eye center and "
                                                                      "the closest acquisition bounding box border is "
                                                                      "<= 25, the flag is set to 2. Else, if (d) <= 40 "
                                                                      "the flag is set to 1. Else is it set to 0.",
                                                       "flag_values": "0 1 2",
                                                       "flag_meanings": "good warning bad"})}


def distance_track_bbox(bbox, lon_center, lat_center):
    geod = Geod(ellps='WGS84')

    eye_pt = Point(lon_center, lat_center)
    p1, p2 = nearest_points(bbox.exterior, eye_pt)
    faz_bbox, baz_bbox, distance_bbox = geod.inv(eye_pt.x, eye_pt.y, p1.x, p1.y)

    # Convert to km
    distance_bbox = distance_bbox / 1000

    # {"title": "Distance to bbox", "yellow_below": 30, "yellow_above": 1000,
    # "red_below": 15, "red_above": 1000},

    if distance_bbox <= 25:
        flag_val = 2
    elif distance_bbox <= 40:
        flag_val = 1
    else:
        flag_val = 0

    return {"distance_track_bbox_flag": DataArray(data=flag_val,
                                                  attrs={
                                                      "long_name": "center quality flag considering distance between "
                                                                   "track point and acquisition bounding box.",
                                                      "description": "If the distance (d) between the track point and "
                                                                     "the closest acquisition bounding box border is "
                                                                     "<= 25, the flag is set to 2. Else, if (d) <= 40 "
                                                                     "the flag is set to 1. Else is it set to 0.",
                                                      "flag_values": "0 1 2",
                                                      "flag_meanings": "good warning bad"})}


def track_point_contained(center_status):
    if center_status[0] == CenterStatus.INSIDE:
        flag_val = 0
    else:
        flag_val = 2

    return {"track_point_flag": DataArray(data=flag_val,
                                          attrs={"long_name": "center quality flag considering track point",
                                                 "flag_values": "0 2",
                                                 "flag_meanings": "good bad",
                                                 "description": "Indicates whether the closest interpolated ATCF "
                                                                "track point "
                                                                "is inside or outside the satellite acquisition"
                                                                " bounding box. 0 means inside, 2 outside."})}


def eye_thinness(eye_shape):
    thinness_ratio_eye = (4 * math.pi * eye_shape.area) / (eye_shape.length * eye_shape.length)

    if thinness_ratio_eye < 0.65:
        flag_val = 2
    elif thinness_ratio_eye < 0.75:
        flag_val = 1
    else:
        flag_val = 0

    return {"eye_circularity_flag": DataArray(data=flag_val,
                                              attrs={
                                                  "long_name": "center quality flag considering eye shape circularity",
                                                  "description": "Indicates the quality of the computed"
                                                                 " eye center considering the computed eye shape "
                                                                 "circularity. More precisely,"
                                                                 " the thinness ratio is used to compare"
                                                                 " between theorical area (computed from perimeter)"
                                                                 " and measured area. If thinness ratio (t) is "
                                                                 "< 0.65 the flag is to set 2. If (t) < 0.75"
                                                                 "then the flag is to 1, else it is set to 0.",
                                                  "flag_values": "0 1 2",
                                                  "flag_meanings": "good warning bad"})}


def percent_eye_bbox(eye_shape_polyg, bbox):
    bbox_eye = bbox.intersection(eye_shape_polyg)
    perc_bbox_eye = (bbox_eye.area / eye_shape_polyg.area) * 100

    if perc_bbox_eye < 70:
        flag_val = 2
    elif perc_bbox_eye < 80:
        flag_val = 1
    else:
        flag_val = 0

    return {"percent_eye_bbox_flag": DataArray(data=flag_val,
                                              attrs={
                                                  "long_name": "center quality flag considering area of eye shape within bbox",
                                                  "description": "Indicates the quality of the computed"
                                                                 " eye center considering the area of the eye shape "
                                                                 "contained in the acquisition bounding box. "
                                                                 "If less than 70% of the eye shape area is inside the"
                                                                 " bounding box, then the flag is set to 2. "
                                                                 "If less than 80% of the eye shape is inside the bbox,"
                                                                 "the flag is set to 1, else it is set to 0.",
                                                  "flag_values": "0 1 2",
                                                  "flag_meanings": "good warning bad"})}


def aggregated_flag(flags_data):
    # if "track_flag" in flags_data:
    agg_flag_val = flags_data["eye_length_flag"].item() * 0.3 + flags_data["track_flag"].item() * 0.7 + \
                   flags_data["lwind_hwind_flag"].item() * 0.5 + flags_data["dist_lwind_flag"].item() * 0.4 + \
                   flags_data["eye_contained_flag"].item() * 0.9 + flags_data["distance_center_bbox_flag"] * 0.25 + \
                   flags_data["distance_track_bbox_flag"] * 0.25 + flags_data["eye_circularity_flag"] * 0.3 + \
                   flags_data["track_point_flag"].item() * 0.4 + flags_data["land_flag"].item() * 0.9
    # else:
    #    agg_flag_val = flags_data["eye_length_flag"].item() * 0.5 + \
    #                   flags_data["lwind_hwind_flag"].item() * 0.7 + flags_data["dist_lwind_flag"].item() * 0.6 + \
    #                   flags_data["eye_contained_flag"].item() * 0.9 + flags_data["distance_bbox_flag"] * 0.7 + \
    #                   flags_data["track_point_flag"].item() * 0.6 + flags_data["land_flag"].item() * 0.9

    return {"center_quality_flag": DataArray(data=agg_flag_val,
                                             attrs={"long_name": "aggregated center quality flag",
                                                    "description": "Aggregated flag indicating general center quality. "
                                                                   "It combines multiple flags to "
                                                                   "give a simplified status of the center quality. "
                                                                   "It is compputed as : eye_length_flag * 0.3 + "
                                                                   "track_flag * 0.7 + lwind_hwind_flag * 0.5 + "
                                                                   "dist_lwind_flag * 0.4 + eye_contained_flag * 0.9"
                                                                   " + distance_center_bbox_flag * 0.25 + "
                                                                   " distance_track_bbox_flag * 0.25 + "
                                                                   " eye_circularity_flag * 0.4 + "
                                                                   "track_point_flag * 0.4 + land_flag * 0.9. "
                                                                   "Values between 0 and 1 means the center quality is"
                                                                   " good, between 1 and 2 the quality is medium and "
                                                                   "above 2 the quality is bad.",
                                                    "flag_values": "0 1 2",
                                                    "flag_meanings": "good warning bad"})}



def custom_rmw(ds_gd):
    ds_gd_sq = ds_gd.squeeze()
    wind_speed = ds_gd_sq.wind_speed.values
    x, y = np.meshgrid(ds_gd_sq.x.values, ds_gd_sq.y.values)
    idx_high = np.where(wind_speed >= np.nanquantile(wind_speed, 0.992))
    dists = np.sqrt(np.power(x[idx_high[0], idx_high[1]], 2) + np.power(y[idx_high[0], idx_high[1]], 2))

    rmw_km = np.average(dists, weights=wind_speed[idx_high[0], idx_high[1]])
    rmw_km = round(rmw_km / 1000, 2)

    return rmw_km


def quality_flags(sar_rmax_nmi, sar_vmax_knots,
                  lon_center_final, lat_center_final, lons_eye_shape, lats_eye_shape, lon_eye_fg, lat_eye_fg,
                  lon_eye_lwind, lat_eye_lwind, lon_eye_hwnd, lat_eye_hwnd, dist_search_hwind,
                  bounding_box, center_status, track_point, track_vmax, track_source_name, rmax_ck22):
    """
    Compute all quality flags for a TC analysis

    Parameters
    ----------
    api_data: pandas.DataFrame Cyclobs API results for current TC analysis. Only first row will be used.
    sat_file_base: str basename of current input satellite file (L2C or nclight format)
    api_url: str host to use to reach cyclobs API. (ie. https://cyclobs.ifremer.fr)
    eye_dict: dict Dictionnary containing outputs of eye center research procedure.

    Returns
    -------
    dict
        Dictionnary of xarray.DataArray containing flag values.

    """
    flags_data = {}

    # Compute shapely shapes to add them to dataset attributes
    eye_coords = []
    for c_i in range(0, len(lons_eye_shape)):
        eye_coords.append((lons_eye_shape[c_i], lats_eye_shape[c_i]))
    eye_multi = MultiPoint(eye_coords)
    eye_convex = eye_multi.convex_hull
    eye_shape = eye_convex

    rmax_ck = rmax_ck22

    # Computing all flags
    diff_rmax = nmi_to_km(sar_rmax_nmi) / rmax_ck
    rmax_flag = rmax_ck
    # If the difference between rmax is significant (rmax_ck underestimate), and we are equal or above cat-1
    # Use sar_vmax as it is more precise
    if diff_rmax > 1.4 and sar_vmax_knots >= 64 and ms_to_knots(track_vmax) >= 64:
        rmax_flag = nmi_to_km(sar_rmax_nmi)
        #rmax_flag = custom_rmw(ds_gd)

    ib_dict = track_quality_flag(loads(track_point), lon_center_final, lat_center_final, rmax_flag, track_source_name)
    flags_data.update(ib_dict)

    lh_flag = dist_low_high_barycenter(lon_eye_lwind, lat_eye_lwind, lon_eye_hwnd, lat_eye_hwnd, rmax_flag)
    flags_data.update(lh_flag)

    eye_length_flag = eye_length(eye_shape, rmax_flag)
    flags_data.update(eye_length_flag)

    eye_circularity = eye_thinness(Polygon(eye_shape))
    flags_data.update(eye_circularity)

    dist_lwind_flag = dist_lwind(lon_eye_lwind, lat_eye_lwind, lon_center_final,
                                 lat_center_final, rmax_flag)
    flags_data.update(dist_lwind_flag)

    eye_contained_flag = eye_contained(bounding_box, lon_center_final, lat_center_final)
    flags_data.update(eye_contained_flag)

    distance_bbox = distance_center_bbox(bounding_box, lon_center_final, lat_center_final)
    flags_data.update(distance_bbox)

    track_point = loads(track_point)
    distance_track = distance_track_bbox(bounding_box, track_point.x, track_point.y)
    flags_data.update(distance_track)

    land_dict = land_research_area_flag(lon_eye_fg, lat_eye_fg, dist_search_hwind)
    flags_data.update(land_dict)

    track_point_flag = track_point_contained(center_status)
    flags_data.update(track_point_flag)

    percent_bbox = percent_eye_bbox(Polygon(eye_shape), bounding_box)
    flags_data.update(percent_bbox)

    # Here, we reevaluate two flags, distance_track_bbox and distance_center_bbox if the eye_shape contains enough
    # point inside the bounding box. In that case, for these two flags, yellow flag becomes greens and red flag
    # becomes yellow.
    if percent_bbox[list(percent_bbox)[0]].item() < 2 and ms_to_knots(track_vmax) >= 64:
        distance_val = distance_track[list(distance_track)[0]].item()
        if distance_val > 0:
            distance_track[list(distance_track)[0]].values = distance_val - 1
            flags_data.update(distance_track)

        distance_bbox_val = distance_bbox[list(distance_bbox)[0]].item()
        if distance_bbox_val > 0:
            distance_bbox[list(distance_bbox)[0]].values = distance_bbox_val - 1
            flags_data.update(distance_bbox)

    agg_flag = aggregated_flag(flags_data)
    flags_data.update(agg_flag)

    return flags_data
