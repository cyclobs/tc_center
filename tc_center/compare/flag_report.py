import xarray as xr
import jinja2
import pandas as pd
import os
import glob


def report_from_path(path_in, path_save, filter_list, exclude_list):
    exclude_lines = []
    if exclude_list is not None:
        with open(exclude_list, "r") as f:
            exclude_lines = f.readlines()
        exclude_lines = [f.rstrip().split(" ")[0].replace("_cyclone_gd", "") for f in exclude_lines]

    filter_lines = []
    if filter_list is not None:
        with open(filter_list, "r") as f:
            filter_lines = f.readlines()
        filter_lines = [f.rstrip().split(" ")[0].replace("_cyclone_gd", "") for f in filter_lines]

    files = glob.glob(os.path.join(path_in, "*_geogr_polar.nc"))

    len_filter = len(filter_lines)
    len_exclude = len(exclude_lines)

    if len_exclude == 0 and len_filter == 0:
        files_filtered = files
    else:
        files_filtered = []
        for f in files:
            filtered = False
            if len_filter > 0:
                if os.path.basename(f).replace("_geogr_polar", "") in filter_lines:
                    files_filtered.append(f)
                    continue
                else:
                    filtered = True
            if len_exclude > 0:
                if not filtered and os.path.basename(f).replace("_geogr_polar", "") not in exclude_lines:
                    files_filtered.append(f)

    build_report(files_filtered, path_save)


def render_jinja_html(template_loc, file_name, **context):

    return jinja2.Environment(
        loader=jinja2.FileSystemLoader(template_loc+'/')
    ).get_template(file_name).render(context)


def build_report(files, path_save):
    df = pd.DataFrame()

    include_attrs = ["Storm name"]
    include_vars = ["cyclone_category", "vmax", "track_vmax", "rmax"]
    exclude_flag_var = ["mask_flag"]

    flag_counts = {}

    for product in files:
        row = {}
        ds = xr.open_dataset(product)
        status_path = os.path.join(os.path.dirname(product), "..", "status")
        time_acq = "-".join(os.path.basename(product).split("-")[4:6])
        status_file = glob.glob(os.path.join(status_path, f"*{time_acq}*.status"))[0]
        with open(status_file, "r") as fs:
            status_code = fs.readline().rstrip()

        row["status"] = status_code

        flag_dict = {}
        for var in ds.data_vars:
            if "flag" in var and var not in exclude_flag_var:
                flag_dict[var] = ds[var].item()
                if var not in flag_counts:
                    flag_counts[var] = {'0': 0, '1': 0, '2': 0}

                if var == "center_quality_flag":
                    if ds[var].item() < 1:
                        key = '0'
                    elif ds[var].item() < 2:
                        key = '1'
                    else:
                        key = '2'
                    flag_counts[var][key] += 1
                else:
                    flag_counts[var][str(ds[var].item())] += 1

        row["flag_dict"] = flag_dict

        for attr in include_attrs:
            if isinstance(ds.attrs[attr], float):
                val = round(ds.attrs[attr], 2)
            else:
                val = ds.attrs[attr]
            row[attr] = val
        for var in include_vars:
            if isinstance(ds[var].item(), float):
                val = round(ds[var].item(), 2)
            else:
                val = ds[var].item()
            row[var] = val

        source = ds.attrs["Source satellite file"].replace("_sw", "_ll_gd")
        row["link"] = f"<a href='https://cyclobs.ifremer.fr/app/center/center_analysis?sat_file={source}'>Link</a>"
        df = df.append(row, ignore_index=True)

    rendered = render_jinja_html(os.path.join(os.path.dirname(__file__), "templates"), "flag_report.html", df=df,
                                 flag_counts=flag_counts, total_count=len(files))
    fout = os.path.join(path_save, f"flag_report.html")
    with open(fout, 'w') as f:
        f.write(rendered)

