import pandas as pd
from shapely.geometry import Point


def ibtracs_read(file):
    df = pd.read_csv(file, keep_default_na=False)

    df = df.rename(columns={"NAME": "storm_name", "ISO_TIME": "track_date"})
    df = df.drop(0)
    df = df[df["track_date"] != ""]
    df["track_date"] = pd.to_datetime(df["date"])
    df["year"] = df["track_date"].apply(lambda x: x.year)
    df["eye_center"] = df.apply(lambda x: Point(float(x["LON"]), float(x["LAT"])), axis=1)

    return df

