import logging
import collections
import math

import numpy as np
import pandas as pd
import geopandas as gpd
import os
import xarray as xr
import owi
import holoviews as hv
import panel as pn
from shapely.geometry import Point, Polygon
from shapely.ops import nearest_points

hv.extension('bokeh')

from pyproj import Geod
import glob
from shapely.wkt import loads
from tc_center.compare.report import build_report
import pathurl
from cyclobs_utils import spd2cat, ms_to_knots
from tc_center.functions import land_mask
from tc_center.quality import custom_rmw

logger = logging.getLogger(__name__)

land_prep, land_non_prep = land_mask()

# Initialize memory monitor
mem_monitor = True
try:
    from psutil import Process
except ImportError:
    logger.warning("psutil module not found. Disabling memory monitor")
    mem_monitor = False

from functools import wraps
import time


def timing(f):
    """provide a @timing decorator for functions, that log time spent in it"""

    @wraps(f)
    def wrapper(*args, **kwargs):
        mem_str = ''
        process = None

        if mem_monitor:
            process = Process(os.getpid())
            startrss = process.memory_info().rss

        starttime = time.time()
        result = f(*args, **kwargs)
        endtime = time.time()

        if mem_monitor:
            endrss = process.memory_info().rss
            mem_str = 'mem: %+.1fMb' % ((endrss - startrss) / (1024 ** 2))

        logger.debug('timing %s : %.1fs. %s' % (f.__name__, endtime - starttime, mem_str))

        return result

    return wrapper


def build_row(cyclone_name, track_date=None, date_other=None, time_diff=None, eye_center=None, eye_center_txt=None,
              eye_center_other=None,
              eye_center_other_txt=None, distance=None, product_file=None, product_filename=None, url_analysis=None,
              vmax=None, rmax=None, track_vmax=None, track_rmax=None, rmax_ck22=None, dist_lwind_hwind=None,
              eye_max_length=None, percent_land_hwind=None, dist_lwind=None, eye_contained=None, distance_bbox=None,
              track_point_contained=None, rmax_warning=None, distance_bbox_track=None, thinness_ratio_eye=None,
              c_rmw=None, bbox_eye_perc=None, eye_nan=None):
    if rmax_warning is not None and rmax_warning == 1:
        rmax_flag = rmax
    else:
        rmax_flag = rmax_ck22

    new_row = {
        "storm_name": cyclone_name,
        "date_cyclobs": track_date,
        "date_other": date_other,
        "time_diff": time_diff,
        "center_cyclobs": eye_center,
        "center_cyclobs_txt": eye_center_txt,
        "center_other": eye_center_other,
        "center_other_txt": eye_center_other_txt,
        "distance": round(distance / 1000, 2) if isinstance(distance, (float, int)) else None,
        "product_file": product_file,
        "product_filename": product_filename,
        "url_analysis": url_analysis,
        "vmax": vmax,
        "rmax": rmax,
        "c_rmw": c_rmw,
        "rmax_ck22": rmax_ck22,
        "rmax_rmax_ck22": rmax / rmax_ck22 if rmax is not None else None,
        "rmax_warning": rmax_warning,
        "dist_lh/rmax_ck22": round((dist_lwind_hwind / 1000) / rmax_flag, 2) if dist_lwind_hwind is not None and track_rmax is not None else None,
        "eye_length/rmax_ck22": round((eye_max_length / 1000) / rmax_flag, 2) if eye_max_length is not None else None,
        "eye_thinness_ratio": round(thinness_ratio_eye, 2) if thinness_ratio_eye is not None else None,
        "dist/rmax": round((distance / 1000) / rmax_flag, 2) if distance is not None and track_rmax is not None else None,
        "percent_land_hwind": round(percent_land_hwind, 2) if percent_land_hwind is not None else None,
        "dist_lwind": round(dist_lwind / 1000, 2) if dist_lwind is not None else None,
        "dist_lwind/rmax_ck22": round((dist_lwind / 1000) / rmax_flag, 2) if dist_lwind is not None else None,
        "distance_center_bbox": round(distance_bbox / 1000) if distance_bbox is not None else None,
        "distance_track_bbox": round(distance_bbox_track / 1000) if distance_bbox_track is not None else None,
        "track_point_contained": track_point_contained if track_point_contained is not None else None,
        "eye_contained": eye_contained if eye_contained is not None else None,
        "bbox_eye_perc": bbox_eye_perc,
        "track_vmax": track_vmax,
        "track_rmax": track_rmax,
        "category": spd2cat(track_vmax, unit="m/s"),
        "eye_nan": eye_nan
    }

    return new_row


def mask_api(gdf_other, row):
    return gdf_other["data_url"] == row["data_url"]


def mask_file(gdf_other, row):
    return (gdf_other["storm_name"] == row["cyclone_name"]) & (gdf_other["year"] == row["year"])


def eye_shape_max_radius_km(eye_shape):
    geod = Geod(ellps='WGS84')

    # https://gis.stackexchange.com/questions/295874/getting-polygon-breadth-in-shapely
    # get minimum bounding box around polygon
    box = eye_shape.minimum_rotated_rectangle
    # get coordinates of polygon vertices
    x, y = box.exterior.coords.xy

    pt0 = Point(x[0], y[0])
    pt1 = Point(x[1], y[1])
    pt2 = Point(x[2], y[2])
    dist1 = pt0.distance(pt1)
    dist2 = pt1.distance(pt2)

    if dist1 > dist2:
        faz, baz, eye_max_length = geod.inv(pt0.x, pt0.y, pt1.x, pt1.y)
    else:
        faz, baz, eye_max_length = geod.inv(pt1.x, pt1.y, pt2.x, pt2.y)

    # Divide by 2 to get radius
    eye_max_length = eye_max_length / 2

    return eye_max_length


@timing
def percent_land_hwind_research(hwind_research_area):
    # hwind_contour = shape_from_array(eye_dict["msk_hwind"], eye_dict["lon"], eye_dict["lat"])
    # hwind_contour = Polygon(hwind_contour)

    land_intersection = hwind_research_area.intersection(land_non_prep)
    percent_land = (land_intersection.area / hwind_research_area.area) * 100

    return percent_land


def compare_centers_api(gdf_cyclobs, gdf_other, mask_func):
    df = pd.DataFrame()
    not_found = []

    geod = Geod(ellps='WGS84')

    for index, row in gdf_cyclobs.iterrows():
        gdf_same_storm = gdf_other.loc[mask_func(gdf_other, row)]
        if len(gdf_same_storm.index) > 0:
            diff_date = gdf_same_storm["track_date"].apply(lambda x: abs(x - row["track_date"]).total_seconds())
            idx_min = diff_date.idxmin()
            selected_row = gdf_same_storm.loc[idx_min]
            faz, baz, dist = geod.inv(row["eye_center"].x, row["eye_center"].y, selected_row["eye_center"].x,
                                      selected_row["eye_center"].y)
            sat_file = os.path.basename(row["data_url"])

            try:
                ds = xr.open_dataset(pathurl.toPath(row["analysis_product_path"], schema="dmz").replace("_polar", "_gd"))
                lwind_pt = loads(ds["low_wind_area_barycenter"].item())
                hwind_pt = loads(ds["high_wind_area_barycenter"].item())
                faz_lh, baz_lh, dist_lh = geod.inv(lwind_pt.x, lwind_pt.y, hwind_pt.x,
                                                   hwind_pt.y)
                eye_shape = loads(ds["eye_shape"].item())
                eye_max_length = eye_shape_max_radius_km(eye_shape)
                eye_shape_polyg = Polygon(eye_shape)
                thinness_ratio_eye = (4 * math.pi * eye_shape_polyg.area) / (eye_shape_polyg.length * eye_shape_polyg.length)
                hwind_research_area = Polygon(loads(ds["high_wind_research"].item()))
                percent_land_hwind = percent_land_hwind_research(hwind_research_area)

                low_wind_barycenter = loads(ds["low_wind_area_barycenter"].item())
                faz_lwind, baz_lwind, dist_lwind = geod.inv(row["eye_center"].x, row["eye_center"].y,
                                                            low_wind_barycenter.x,
                                                            low_wind_barycenter.y)

                bbox = loads(row["bounding_box"])

                bbox_eye = bbox.intersection(eye_shape_polyg)
                perc_bbox_eye = (bbox_eye.area / eye_shape_polyg.area) * 100

                if bbox.contains(row["eye_center"]):
                    eye_contained = 1
                else:
                    eye_contained = 0

                p1, p2 = nearest_points(bbox.exterior, row["eye_center"])
                faz_bbox, baz_bbox, distance_bbox = geod.inv(row["eye_center"].x, row["eye_center"].y, p1.x, p1.y)

                track_point = loads(row["track_point"])
                if bbox.contains(track_point):
                    track_point_contained = 1
                else:
                    track_point_contained = 0

                p1, p2 = nearest_points(bbox.exterior, track_point)
                faz_bbox, baz_bbox, distance_bbox_track = geod.inv(track_point.x, track_point.y, p1.x, p1.y)

                rmax = round(ds["rmax"].item() * 1.85200, 2)
                vmax = ds["vmax"].item()

                ratio_rmax = rmax / row["RMAX_CK22"]

                c_rmw = custom_rmw(ds)

                rmax_warning = 0
                # If the difference between rmax is significant (rmax_ck underestimate), and we are equal or above cat-1
                if ratio_rmax > 1.4 and vmax >= 64 and ms_to_knots(row["vmax (m/s)"]) >= 64:
                    rmax_warning = 1

                # FIXME the eye_center in API is too rounded, inacurrate
                if row["cyclone_name"] == "KARINA":
                    print(low_wind_barycenter)
                    print(rmax_warning)
                    print(rmax, row["RMAX_CK22"])
                    print(dist_lwind)
                    print(round((dist_lwind / 1000) / row["RMAX_CK22"], 2))
                    print(row["eye_center"].x, row["eye_center"].y)

                if np.count_nonzero(np.isnan(ds["wind_speed"].squeeze().values[499, :])) / ds["wind_speed"].squeeze().values.shape[1] > 0.97:
                    eye_nan = 1
                else:
                    eye_nan = 0
            except:
                vmax = None
                rmax = None
                dist_lh = None
                eye_max_length = None
                percent_land_hwind = None
                dist_lwind = None
                eye_contained = None
                distance_bbox = None
                track_point_contained = None
                rmax_warning = None
                distance_bbox_track = None
                thinness_ratio_eye = None
                c_rmw = None
                perc_bbox_eye = None
                eye_nan = None
                print("EXCEPTION !")

            new_row = build_row(row["cyclone_name"], row["track_date"], selected_row["track_date"],
                                abs(selected_row["track_date"] - row["track_date"]), row["eye_center"],
                                f"{row['eye_center'].x} {row['eye_center'].y}", selected_row["eye_center"],
                                f"{selected_row['eye_center'].x} {selected_row['eye_center'].y}", dist,
                                row["product_file"], os.path.basename(row["product_file"]),
                                f"https://cyclobs.ifremer.fr/app/center/center_analysis?sat_file={sat_file}",
                                track_vmax=row["vmax (m/s)"], track_rmax=row["rmw"], rmax=rmax, vmax=vmax,
                                rmax_ck22=row["RMAX_CK22"], dist_lwind_hwind=dist_lh, eye_max_length=eye_max_length,
                                percent_land_hwind=percent_land_hwind, dist_lwind=dist_lwind,
                                eye_contained=eye_contained, distance_bbox=distance_bbox,
                                track_point_contained=track_point_contained, rmax_warning=rmax_warning,
                                distance_bbox_track=distance_bbox_track, thinness_ratio_eye=thinness_ratio_eye,
                                c_rmw=c_rmw, bbox_eye_perc=perc_bbox_eye, eye_nan=eye_nan)

            df = df.append(new_row, ignore_index=True)
        else:
            not_found.append(row["cyclone_name"])

    print("Not found count : ", len(not_found))
    return df, not_found


def save_histogram_dist_rmax(df, path_save, col, save=True, title=None):
    df_cp = df.copy(deep=True)
    df_cp = df_cp.loc[~pd.isna(df_cp[col])]
    frequencies, edges = np.histogram(df_cp[col], 20, density=False)
    if title is None:
        title = "Count of distance between centers / rmax"
    h = hv.Histogram((edges, frequencies)).opts(width=600, height=600, title=title,
                                                xlabel="Distance or Ratio",
                                                ylabel="Count", padding=(0, (0, 0.1)))

    if save:
        hv.save(h, path_save, fmt="png")
    else:
        return h


def save_histogram_dist(df, path_save, save=True, title=None):
    frequencies, edges = np.histogram(df["distance"], 20, density=False)
    if title is None:
        title = "Count of distance between centers"
    h = hv.Histogram((edges, frequencies)).opts(width=600, height=600, title=title,
                                                xlabel="Distance between centers (km)",
                                                ylabel="Count", padding=(0, (0, 0.1)))
    if save:
        hv.save(h, path_save, fmt="png")
    else:
        return h


def save_histogram_rmax(df, path_save, col1, col2, save=True, title=None):
    df_cp = df.copy(deep=True)
    df_cp = df_cp.loc[(df_cp[col1] > 0) & (~pd.isna(df_cp[col1]))]
    df_cp = df_cp.loc[(df_cp[col2] > 0) & (~pd.isna(df_cp[col2]))]
    diff = df_cp[col2] - df_cp[col1]
    diff_high = diff.loc[diff > 150]
    for index, row in diff_high.iteritems():
        print(df.iloc[index]["url_analysis"])
    frequencies, edges = np.histogram(diff, 20, density=False)
    if title is None:
        title = "Count of difference between rmaxs"
    h = hv.Histogram((edges, frequencies)).opts(width=600, height=600, title=title,
                                                xlabel="Difference between RMW (km)",
                                                ylabel="Count", padding=(0, (0, 0.1)))
    if save:
        hv.save(h, path_save, fmt="png")
    else:
        return h


def save_histogram_per_cat(df, path_save, filename, type, col="dist/rmax"):
    maxspd = collections.OrderedDict([
        ("dep", 34),
        ("storm", 64),
        ("cat-1", 82),
        ("cat-2", 95),
        ("cat-3", 112),
        ("cat-4", 136),
        ("cat-5", 999)])

    row = pn.Row()
    plots = []
    max = len(df) / 7

    prev_min = 0
    for cat in maxspd:
        maxspd_ms = maxspd[cat] * 0.51444444444444
        df_cat = df.loc[(df["track_vmax"] > prev_min) & (df["track_vmax"] <= maxspd_ms)]
        prev_min = maxspd_ms
        print(len(df_cat.index))
        if type == "dist/rmax":
            p = save_histogram_dist_rmax(df_cat, path_save + f"/histo_cat_dist_rmax_{cat}.png", save=False,
                                         title=f"Count of distance between centers / rmax for {cat}", col=col)
            p.opts(title=f"Count of distance between centers / rmax for {cat}")
            plots.append(p)
        elif type == "dist":
            p = save_histogram_dist(df_cat, path_save + f"/histo_cat_dist_{cat}.png", save=False,
                                    title=f"Count of distance between centers (km) for {cat}")
            p.opts(title=f"Count of distance between centers (km) for {cat}")
            plots.append(p)

        # row.append(p)

    cat_plot = hv.Layout(plots)

    if type == "dist/rmax":
        hv.save(cat_plot, os.path.join(path_save, filename))
        # row.save(os.path.join(path_save, "histo_cat_dist_rmax.png"))
    elif type == "dist":
        hv.save(cat_plot, os.path.join(path_save, filename))
        # row.save(os.path.join(path_save, "histo_cat_dist.png"))


def compare_with_ref(ref_product_path, compared_product_path, path_save=None, filter_list=None):
    if filter_list is not None:
        with open(filter_list, "r") as f:
            filter_lines = f.readlines()

        filter_lines = [f.rstrip() for f in filter_lines]

    ref_files = glob.glob(os.path.join(ref_product_path, "*_cyclone_gd.nc"))
    # compared_files = glob.glob(os.path.join(compared_product_path, "*_cyclone_gd.nc"))

    api_req = "https://cyclobs.ifremer.fr/app/api/getData?include_cols=all&instrument=C-Band_SAR&track_source=atcf"
    df_cyclobs = pd.read_csv(api_req, keep_default_na=False)
    df_cyclobs = df_cyclobs.loc[df_cyclobs["eye_center"] != ""]
    df_cyclobs["analysis_product_base"] = df_cyclobs["analysis_product_path"].apply(lambda x: os.path.basename(x))

    not_found = []
    ok_files = []
    close_files = []
    too_far_files = []
    df = pd.DataFrame()

    geod = Geod(ellps='WGS84')

    for ref_file in ref_files:
        if filter_list is None or os.path.basename(ref_file).replace("_cyclone_gd", "") in filter_lines:
            check_file = os.path.join(compared_product_path, os.path.basename(ref_file))
            if os.path.isfile(check_file):
                print("LOL3", ref_file)
                print("LOOOOL", check_file)
                ref_ds_attrs = xr.open_dataset(ref_file).attrs
                ref_ds = xr.open_dataset(ref_file)
                compared_ds = xr.open_dataset(check_file)

                api_row = df_cyclobs.loc[
                    df_cyclobs["analysis_product_base"] == os.path.basename(ref_file).replace("_cyclone_gd", "")]
                atcf_vmax = None
                atcf_rmax = None
                if len(api_row.index) > 0:
                    atcf_vmax = api_row.iloc[0]["vmax (m/s)"]
                    atcf_rmax = api_row.iloc[0]["rmw"]

                vmax_compared = compared_ds["vmax"].item() * 0.514444
                rmax_compared = compared_ds["rmax"].item() * 1.852
                ref_pt = loads(ref_ds["eye_center"].item())
                compared_pt = loads(compared_ds["eye_center"].item())
                faz, baz, dist = geod.inv(ref_pt.x, ref_pt.y, compared_pt.x,
                                          compared_pt.y)
                if dist == 0:
                    ok_files.append(os.path.basename(ref_file))
                elif dist <= 2000:
                    close_files.append(os.path.basename(ref_file))
                else:
                    too_far_files.append(os.path.basename(ref_file))

                new_row = build_row(ref_ds_attrs["Storm name"], eye_center=ref_pt, eye_center_other=compared_pt,
                                    eye_center_txt=f"{ref_pt.x} {ref_pt.y}",
                                    eye_center_other_txt=f"{compared_pt.x} {compared_pt.y}", distance=dist,
                                    product_file=ref_file, product_filename=os.path.basename(ref_file),
                                    url_analysis=f"https://cyclobs.ifremer.fr/app/center"
                                                 f"/center_analysis?sat_file={ref_ds_attrs['Source satellite file'].replace('sw', 'll_gd')}",
                                    rmax=rmax_compared, vmax=vmax_compared, track_rmax=atcf_rmax, track_vmax=atcf_vmax)
                df = df.append(new_row, ignore_index=True)
            else:
                not_found.append(os.path.basename(ref_file))

    print("Distance == 0 count : ", len(ok_files))
    print("Lower than 2 km distance count : ", len(close_files))
    print("Higher than 2 km distance count : ", len(too_far_files))
    print("Not found count", len(not_found))

    # df_not_none = df.loc[df["dist/rmax"] != 'None']
    df_not_none = df.dropna(subset=["dist/rmax"])
    print(df_not_none["dist/rmax"])
    save_histogram_dist_rmax(df_not_none, path_save + "/hist_dist_rmax.png", col="dist/rmax")
    save_histogram_dist(df_not_none, path_save + "/hist_dist.png")
    save_histogram_per_cat(df_not_none, path_save, type="dist/rmax")
    save_histogram_per_cat(df_not_none, path_save, type="dist")

    if path_save is not None:
        build_report(df, "cyclobs_ref", path_save, not_found)

### OLD ###
# def compare_centers(gdf_cyclobs, gdf_other):
#    df = pd.DataFrame()
#    not_found = []
#
#    geod = Geod(ellps='WGS84')
#
#    for index, row in gdf_cyclobs.iterrows():
#        gdf_same_storm = gdf_other.loc[(gdf_other["storm_name"] == row["cyclone_name"]) & (gdf_other["year"] == row["year"])]
#        if len(gdf_same_storm.index) > 0:
#            diff_date = gdf_same_storm["date"].apply(lambda x: abs(x - row["track_date"]).total_seconds())
#            idx_min = diff_date.idxmin()
#            selected_row = gdf_same_storm.loc[idx_min]
#            faz, baz, dist = geod.inv(row["eye_center"].x, row["eye_center"].y, selected_row["eye_center"].x, selected_row["eye_center"].y)
#            sat_file = os.path.basename(row["data_url"])
#            new_row = {
#                "storm_name": row["cyclone_name"],
#                "date_cyclobs": row["track_date"],
#                "date_other": selected_row["date"],
#                "time_diff": abs(selected_row["date"] - row["track_date"]),
#                "center_cyclobs": row["eye_center"],
#                "center_cyclobs_txt": f"{row['eye_center'].x} {row['eye_center'].y}",
#                "center_other": selected_row["eye_center"],
#                "center_other_txt": f"{selected_row['eye_center'].x} {selected_row['eye_center'].y}",
#                "distance": str(round(dist / 1000, 2)) + " km",
#                "product_file": row["product_file"],
#                "product_filename": os.path.basename(row["product_file"]),
#                "url_analysis": f"https://cyclobs.ifremer.fr/app/center/center_analysis?sat_file="
#                                f"{sat_file}"
#            }
#            df = df.append(new_row, ignore_index=True)
#        else:
#            not_found.append(row["cyclone_name"])
#
#    return df
