import os.path

from tc_center.compare.compare import compare_centers_api, mask_api
from tc_center.compare.report import build_report
from tc_center.compare.read import ibtracs_read
import pandas as pd
from shapely.wkt import loads
import pathurl
import numpy as np
from .compare import save_histogram_dist, save_histogram_dist_rmax, save_histogram_per_cat, save_histogram_rmax
from tc_center.functions import get_rmax_ck22, coriolis
import xarray as xr


def compare(path_source, path_save, compare_with, filter_list):
    if compare_with == "ibtracs":
        ibtracs_compare_from_api(path_save, filter_list, ibtracs_path=path_source)


def ibtracs_compare_from_api(path_save, filter_list, ibtracs_path=None):
    if filter_list is not None:
        with open(filter_list, "r") as f:
            filter_lines = f.readlines()

        filter_lines = [f.rstrip().split(" ")[0].replace("_cyclone_gd", "") for f in filter_lines]

    if ibtracs_path is not None:
        df_cyclobs_ibtracs = ibtracs_read(ibtracs_path)
    else:
        api_req_ibtracs = "https://cyclobs.ifremer.fr/app/api/getData?include_cols=all&instrument=C-Band_SAR&track_source=ibtracs"
        df_cyclobs_ibtracs = pd.read_csv(api_req_ibtracs, keep_default_na=False)
        df_cyclobs_ibtracs = df_cyclobs_ibtracs.loc[df_cyclobs_ibtracs["eye_center"] != ""]
        df_cyclobs_ibtracs["track_date"] = pd.to_datetime(df_cyclobs_ibtracs["track_date"])
        df_cyclobs_ibtracs["year"] = df_cyclobs_ibtracs["track_date"].apply(lambda x: x.year)
        df_cyclobs_ibtracs["eye_center"] = df_cyclobs_ibtracs["track_point"].apply(lambda x: loads(x))

    api_req = "https://cyclobs.ifremer.fr/app/api/getData?include_cols=all&instrument=C-Band_SAR&track_source=atcf"
    df_cyclobs = pd.read_csv(api_req, keep_default_na=False)
    df_cyclobs = df_cyclobs.loc[df_cyclobs["eye_center"] != ""]
    df_cyclobs["analysis_product_base"] = df_cyclobs["analysis_product_path"].apply(
        lambda x: os.path.basename(x).replace("_geogr_polar", ""))
    if filter_list is not None:
        df_cyclobs = df_cyclobs.loc[df_cyclobs["analysis_product_base"].isin(filter_lines)]

    df_cyclobs["track_date"] = pd.to_datetime(df_cyclobs["track_date"])
    df_cyclobs["year"] = df_cyclobs["track_date"].apply(lambda x: x.year)
    df_cyclobs["eye_center"] = df_cyclobs["eye_center"].apply(lambda x: loads(x))
    df_cyclobs["product_file"] = df_cyclobs["analysis_product_path"].apply(lambda x: pathurl.toPath(x, schema="dmz"))
    df_cyclobs["atcf_rad34"] = df_cyclobs[['atcf_rad34_seq', 'atcf_rad34_neq', 'atcf_rad34_swq', 'atcf_rad34_nwq']].apply(pd.to_numeric, errors='coerce').mean(axis=1)
    df_cyclobs["RMAX_CK22"] = df_cyclobs.apply(lambda x:
                                               round(get_rmax_ck22(x["vmax (m/s)"], x["atcf_rad34"],
                                                             coriolis(x["eye_center"].y) if x["eye_center"].y > 0 else
                                                             abs(coriolis(x["eye_center"].y)), 0.482, 0.00309,
                                                             -0.00304) / 1000, 2), axis="columns")

    df, not_found = compare_centers_api(df_cyclobs, df_cyclobs_ibtracs, mask_api)
    save_histogram_dist_rmax(df, path_save + "/histo_dist_rmax.png", col="dist/rmax", title="SAR-derived center and IBTrACS center distance / RMW")
    save_histogram_dist_rmax(df, path_save + "/histo_dist_lh_rmax_ck22.png", col="dist_lh/rmax_ck22",
                             title="Low and high wind barycenter distance / RMW")
    save_histogram_dist_rmax(df, path_save + "/histo_eye_lenght_rmax_ck22.png", col="eye_length/rmax_ck22",
                             title="Ratio between max. eye length and RMW")
    save_histogram_dist_rmax(df, path_save + "/histo_dist_lwind_rmax_ck22.png", col="dist_lwind/rmax_ck22",
                             title="Low wind barycenter and SAR-derived center distance / RMW")
    save_histogram_dist_rmax(df, path_save + "/histo_dist_lwind.png", col="dist_lwind",
                             title="Low wind barycenter and SAR-derived center distance")
    save_histogram_dist(df, path_save + "/histo_dist.png")
    save_histogram_per_cat(df, path_save, "cat_dist_rmax.png", type="dist/rmax")
    save_histogram_per_cat(df, path_save, "cat_dist_lwind.png", type="dist/rmax", col="dist_lwind")
    save_histogram_per_cat(df, path_save, "cat_dist_lwind_rmax_ck22.png", type="dist/rmax", col="dist_lwind/rmax_ck22")

    save_histogram_per_cat(df, path_save, "cat_dist.png", type="dist")
    save_histogram_rmax(df, path_save + "/histo_trmax_ck22.png", "rmax_ck22", "track_rmax",
                        title="Count of difference between ATCF RMAX and CK22 RMAX. (atcf_rmax - rmax_ck22)")
    save_histogram_rmax(df, path_save + "/histo_rmax_ck22.png", "rmax_ck22", "rmax",
                        title="Distribution of SAR-derived RMW minus CK22 RMW")
    save_histogram_rmax(df, path_save + "/histo_trmax_rmax.png", "rmax", "track_rmax",
                        title="Count of difference between ATCF RMAX and Analysis RMAX. (atcf_rmax - analysis_rmax)")

    save_histogram_dist_rmax(df, path_save + "/histo_thinness.png", col="eye_thinness_ratio", title="SAR-derived eye shape thinness ratio")

    build_report(df, "ibtracs", path_save, not_found)

# def ibtracs_compare(ibtracs_path, path_save, filter_list):
#    if filter_list is not None:
#        with open(filter_list, "r") as f:
#            filter_lines = f.readlines()
#
#        filter_lines = [f.rstrip() for f in filter_lines]
#
#    df_ibtracs = ibtracs_read(ibtracs_path)
#
#    api_req = "https://cyclobs.ifremer.fr/app/api/getData?include_cols=all&instrument=C-Band_SAR"
#    df_cyclobs = pd.read_csv(api_req, keep_default_na=False)
#    df_cyclobs = df_cyclobs.loc[df_cyclobs["eye_center"] != ""]
#    df_cyclobs["analysis_product_base"] = df_cyclobs["analysis_product_path"].apply(lambda x: os.path.basename(x))
#    if filter_list is not None:
#        df_cyclobs = df_cyclobs.loc[df_cyclobs["analysis_product_base"].isin(filter_lines)]
#
#    df_cyclobs["track_date"] = pd.to_datetime(df_cyclobs["track_date"])
#    df_cyclobs["year"] = df_cyclobs["track_date"].apply(lambda x: x.year)
#    df_cyclobs["eye_center"] = df_cyclobs["eye_center"].apply(lambda x: loads(x))
#    df_cyclobs["product_file"] = df_cyclobs["analysis_product_path"].apply(lambda x: pathurl.toPath(x, schema="dmz"))
#
#    df = compare_centers(df_cyclobs, df_ibtracs)
#
#    build_report(df, "ibtracs", path_save)
