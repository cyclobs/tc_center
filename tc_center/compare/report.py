from flask import render_template
import flask
import os
from collections import OrderedDict


def init_flag_df(df, flag, lims, flags_lims):
    yel = ((df[flag] >= lims["yellow_above"]) & (df[flag] < lims["red_above"])) | \
          ((df[flag] <= lims["yellow_below"]) & (df[flag] > lims["red_below"]))
    df_flag = df.loc[yel]
    df.loc[yel, f"{flag}_color"] = "yellow"
    df.loc[yel, f"{flag}_val"] = 1
    flags_lims[flag]["count_yellow"] = len(df_flag.index)

    red = (df[flag] >= lims["red_above"]) | (df[flag] <= lims["red_below"])
    df_flag = df.loc[red]
    df.loc[red, f"{flag}_color"] = "red"
    df.loc[red, f"{flag}_val"] = 2
    flags_lims[flag]["count_red"] = len(df_flag.index)

    green = (df[flag] < lims["yellow_above"]) & (df[flag] > lims["yellow_below"])
    df_flag = df.loc[green]
    df.loc[green, f"{flag}_color"] = "green"
    df.loc[green, f"{flag}_val"] = 0
    flags_lims[flag]["count_green"] = len(df_flag.index)


def flag_dict(df):
    flags_lims = OrderedDict({
        "eye_length/rmax_ck22": {"title": "Eye radius / RMAX CK22",
                                 "yellow_below": 0.2, "yellow_above": 1, "red_below": 0.17, "red_above": 1.2},
        "eye_thinness_ratio": {"title": "Thinness",
                                "yellow_below": 0.75, "yellow_above": 10., "red_below": 0.65, "red_above": 10},
        "dist/rmax": {"title": "Distance between centers / RMAX CK22",
                      "yellow_below": -10, "yellow_above": 0.8, "red_below": -10, "red_above": 1.1},
        "dist_lh/rmax_ck22": {"title": "Lwind-Hwind Distance / RMAX CK22",
                              "yellow_below": -10, "yellow_above": 1.6, "red_below": -10, "red_above": 1.7},
        "percent_land_hwind": {"title": "Land percent in high wind research area",
                               "yellow_below": -10, "yellow_above": 0.5, "red_below": -10, "red_above": 3},
        "dist_lwind/rmax_ck22": {"title": "Dist Lwind-eye/rmax_ck22", "yellow_below": -10, "yellow_above": 0.6,
                                 "red_below": -10,
                                 "red_above": 0.8},
        # "dist_lwind": {"title": "Dist Lwind-eye", "yellow_below": -10, "yellow_above": 15, "red_below": -10,
        #                        "red_above": 20}
        "eye_contained": {"title": "Eye contained", "yellow_below": 0, "yellow_above": 10,
                          "red_below": 0, "red_above": 10},
        "bbox_eye_perc": {"title": "Bbox eye perc", "yellow_below": 80, "yellow_above": 110,
                          "red_below": 70, "red_above": 110},
        "rmax_rmax_ck22": {"title": "Ratio rmax", "yellow_below": 0.5, "yellow_above": 1.4,
                           "red_below": 0.4, "red_above": 1.6},
        "distance_center_bbox": {"title": "Distance center-bbox", "yellow_below": 40, "yellow_above": 1000,
                                 "red_below": 25, "red_above": 1000},
        "distance_track_bbox": {"title": "Distance track-bbox", "yellow_below": 40, "yellow_above": 1000,
                                "red_below": 25, "red_above": 1000},
        "track_point_contained": {"title": "Track point contained", "yellow_below": 0, "yellow_above": 10,
                                  "red_below": 0, "red_above": 10},
        "rmax_warning": {"title": "Rmax warning", "yellow_below": -1, "yellow_above": 0.5,
                         "red_below": -1, "red_above": 0.9},
        #"eye_nan": {"title": "Eye nan", "yellow_below": -1, "yellow_above": 0.5, "red_below": -1, "red_above": 0.9}
    })

    for flag, lims in flags_lims.items():
        init_flag_df(df, flag, lims, flags_lims)

    for index, row in df.iterrows():
        if row["bbox_eye_perc_val"] < 2:
            if row["distance_center_bbox_val"] != 0:
                df.loc[index, "distance_center_bbox_val"] -= 1
            if row["distance_track_bbox_val"] != 0:
                df.loc[index, "distance_track_bbox_val"] -= 1


    # df["final_flag"] = (df["eye_length/rmax_ck22_val"] * 0.5 + df["dist/rmax_val"] * 2 + df["dist_lh/rmax_ck22_val"] * 1 +
    #                    df["dist_lwind/rmax_ck22_val"] * 0.8 + df["eye_contained_val"] * 1) / 5.3 + df["percent_land_hwind_val"] / 2

    df["final_flag"] = (
                df["eye_length/rmax_ck22_val"] * 0.3 + df["dist/rmax_val"] * 0.7 + df["dist_lh/rmax_ck22_val"] * 0.5 +
                df["dist_lwind/rmax_ck22_val"] * 0.4 + df["eye_contained_val"] * 0.9 +
                df["distance_center_bbox_val"] * 0.25 + df["track_point_contained_val"] * 0.4 +
                df["distance_track_bbox_val"] * 0.25 + df["eye_thinness_ratio_val"] * 0.3
                + df["percent_land_hwind_val"] * 0.9)

    flags_lims["final_flag"] = {"title": "Final flag", "yellow_below": -10, "yellow_above": 1,
                                "red_below": -10, "red_above": 2}
    init_flag_df(df, "final_flag", flags_lims["final_flag"], flags_lims)

    return flags_lims


def build_report(df, other_source_name, path, not_found):
    total_count = len(df.index)
    flag_lims = flag_dict(df)
    # var_flag = "eye_length/rmax_ck22"
    # distance_yellow_below = 0.2
    # distance_yellow_above = 0.9
    #
    # distance_red_below = 0.17
    # distance_red_above = 1.1
    # df["color"] = "green"
    #
    # df_green = df.loc[(df[var_flag] < distance_yellow_above) & (df[var_flag] > distance_yellow_below)]
    # green_count = len(df_green.index)
    #
    # df.loc[((df[var_flag] >= distance_yellow_above) & (df[var_flag] < distance_red_above)) |
    #       ((df[var_flag] < distance_yellow_below) & (df[var_flag] > distance_red_below)), "color"] = "yellow"
    # df_yellow = df.loc[((df[var_flag] >= distance_yellow_above) & (df[var_flag] < distance_red_above)) |
    #       ((df[var_flag] < distance_yellow_below) & (df[var_flag] > distance_red_below))]
    # yellow_count = len(df_yellow.index)
    #
    # df.loc[(df[var_flag] >= distance_red_above) | (df[var_flag] <= distance_red_below), "color"] = "red"
    # df_red = df.loc[(df[var_flag] >= distance_red_above) | (df[var_flag] <= distance_red_below)]
    # red_count = len(df_red.index)

    app = flask.Flask('my app', template_folder="tc_center/compare/templates")

    with app.app_context():
        rendered = render_template('report_list.html',
                                   title=f"Comparison list with {other_source_name}",
                                   total_count=total_count,
                                   flag_lims=flag_lims,
                                   not_found_count=len(not_found),
                                   df=df,
                                   other_source_name=other_source_name)

        fout = os.path.join(path, f"compare_{other_source_name}.html")
        with open(fout, 'w') as f:
            f.write(rendered)
