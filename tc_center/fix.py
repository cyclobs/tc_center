import copy
import os
import atcf
import pandas as pd
import owi
import logging
import numpy as np
from cyclobs_utils import ms_to_knots, km_to_nmi
from cyclobs_utils.cyclobs_config import MISSION_LONG_TO_SHORT

logger = logging.getLogger(__name__)


def get_vmax(radii_nmi_quad: dict, percent_valid_quad: dict):
    """
    Returns the cyclone VMAX and RMAX. The VMAX is the maximum of the Vmax per quadrants. RMAX is the distance
    for that VMAX.
    If the max of VMAX is invalid or that not enough data was available for its quadrants, we take the max wind speed
    calculated from the wind mean of all quadrants.

    Parameters
    ----------
    radii_nmi_quad: dict Organized as {"NE": [vmax_knots, rmax_nmi, rad64_nmi, rad50_nmi, rad34_nmi]}
        which is dict(quadrant: list(vmax, rmax, rad64, rad50, rad34))
    percent_valid_quad: Same structure that radii_nmi_quad but contains instead the percentage of valid data used
        to compute the values that are in radii_nmi_quad

    Returns
    -------
    float, float
        Vmax value, Rmax value

    """
    max_vmax = -1
    max_vmax_quad = None
    for quad, radii in radii_nmi_quad.items():
        if radii[0] > max_vmax:
            max_vmax = radii[0]
            max_vmax_quad = quad

    # If max of vmax is invalid, taking max of mean on all quadrants.
    if max_vmax == -1 or percent_valid_quad[max_vmax_quad][0] < 75:
        return radii_nmi_quad["all"][0], radii_nmi_quad["all"][1]

    return radii_nmi_quad[max_vmax_quad][0], radii_nmi_quad[max_vmax_quad][1]


def remove_floating_part(df):
    # This to remove the floating part (.0) of the value.
    df["rad1"] = df["rad1"].apply(lambda x: str(int(x)) if not np.isnan(x) else "")
    df["rad2"] = df["rad2"].apply(lambda x: str(int(x)) if not np.isnan(x) else "")
    df["rad3"] = df["rad3"].apply(lambda x: str(int(x)) if not np.isnan(x) else "")
    df["rad4"] = df["rad4"].apply(lambda x: str(int(x)) if not np.isnan(x) else "")
    df["wind speed"] = df["wind speed"].apply(lambda x: str(int(x)) if not np.isnan(x) else "")
    df["MRD"] = df["MRD"].apply(lambda x: str(int(x)) if not np.isnan(x) else "")

    return df


def save_fix_analysis(path, sat_file, atcf_filename, lon_eye, lat_eye, sar_vmax, sar_rmax,
                      radii_nmi_quad, percent_valid_quad,
                      sid, sat_mission,
                      sub_basin, acq_time, quality_flag):
    """
    Generate a storm FIX following this format specification :
    https://www.nrlmry.navy.mil/atcf_web/docs/database/new/newfdeck.txt
    Two FIX are generated : a per-acquisition FIX and a per-storm FIX, reusing a potential previous storm FIX available
    on disk.

    Parameters
    ----------
    path: str Directory to save the FIX
    sat_file: str Source satellite file path
    atcf_filename: str ATCF filename (without path) for the observed storm
    lon_eye: float longitude of eye center
    lat_eye: float latitude of eye center
    radii_nmi_quad: dict(quadrant: list(vmax, rmax, rad64, rad50, rad34)) Contains radii for all+4 quadrants, in knots
        and nmi units.
    percent_valid_quad: dict(quadrant: list(vmax, rmax, rad64, rad50, rad34)) Percent of valid data used to compute each
        radii, vmax, rmax values.
    sid: str Storm ID
    sat_mission: str Satellite mission for current observation. SENTINEL-1 A, SENTINEL-1 B or RADARSAT-2
    sub_basin: str Sub basin. The function translates to a basin supported by the FIX format using this dict :
        {"AS": "A", "BB": "B", "CP": "C", "NEP": "E", "NA": "L", "SP": "P", "SI": "S", "NWP": "W",
                           "GM": "L", "CS": "L", "NI": "A", "WA": "S", "EA": "P"}
        Given sub_basin that is not available as key of this dictionnary will make the function crash.
    acq_time: datetime.datetime Acquisition timestamp
    quality_flag: int Quality of the computed center, 0 for good, 1 for warning, 2 for bad.


    Return
    ------
        Nothing
    """

    atcf_outfile = os.path.join(path, "SARFIX_" + atcf_filename)
    sar_outfile = os.path.join(path, f"SARFIX_{os.path.basename(sat_file).replace('_ll_gd', '').replace('.nc', '')}.dat")

    fix = pd.DataFrame()
    for emptyCol in ["BASIN", "CY", "date", "fix format", "fix type", "center/intensity", "flagged indicator", "lat",
                     "lon",
                     "height of ob", "posit confidence", "wind speed", "confidence", "pressure", "pressure confidence",
                     "pressure derivation", "rad", "windcode", "rad1", "rad2", "rad3", "rad4",
                     "radmod1", "radmod2", "radmod3", "radmod4", "radii confidence", "MRD", "eye", "subregion",
                     "fix site", "initials", "analyst initials", "start time", "end time", "distance to nearest data",
                     "SST",
                     "observation sources", "comments"]:
        fix.loc[0, emptyCol] = None

    stormid = sid[2:4]  # al092020
    basin = sid[0:2]

    mis_to_type = copy.copy(MISSION_LONG_TO_SHORT)
    mis_to_type["SENTINEL-1 A"] = "SEN1"
    mis_to_type["SENTINEL-1 B"] = "SEN1"

    # NI ok, SI ok, SP ok, NWP ok, NEP ok, NA ok, WA ok, EA ok, AS ok, BB ok, CS ok, GM ok
    # It is ok to set North Indian ocean to A (=arabian sea) because the north indian ocean is fully covered by sub basin
    # so "NI" should never be output by the API in the sub_basin field.
    sub_basin_subregion = {"AS": "A", "BB": "B", "CP": "C", "NEP": "E", "NA": "L", "SP": "P", "SI": "S", "NWP": "W",
                           "GM": "L", "CS": "L", "NI": "A", "WA": "S", "EA": "P"}

    fix.loc[0, 'BASIN'] = basin.upper()
    fix.loc[0, 'CY'] = str(stormid).zfill(2)
    fix.loc[0, 'date'] = owi.getDateFromFname(os.path.basename(sat_file)).strftime("%Y%m%d%H%M")
    fix.loc[0, "fix format"] = "70"
    fix.loc[0, "fix type"] = mis_to_type[sat_mission]
    fix.loc[0, "center/intensity"] = "CIR"
    fix.loc[0, 'lat'] = lat_eye
    fix.loc[0, 'lon'] = lon_eye
    fix.loc[0, "height of ob"] = "10"
    fix.loc[0, "posit confidence"] = str(quality_flag + 1)
    fix.loc[0, "subregion"] = sub_basin_subregion[sub_basin]
    fix.loc[0, "fix site"] = "IFR"
    fix.loc[0, "initials"] = "SAR"
    fix.loc[0, "start time"] = acq_time.strftime("%Y%m%d%H%M")
    fix.loc[0, "end time"] = acq_time.strftime("%Y%m%d%H%M")
    fix.loc[0, "distance to nearest data"] = ""
    fix.loc[0, "SST"] = ""
    fix.loc[0, "observation sources"] = sat_mission
    fix.loc[0, "comments"] = "Synthetic Aperture Radar 3KM Wind Speed Analysis"

    if np.isnan(sar_vmax):
        fix.loc[0, "wind speed"] = ""
    else:
        fix.loc[0, "wind speed"] = str(int(round(ms_to_knots(sar_vmax))))

    if np.isnan(sar_rmax):
        fix.loc[0, "MRD"] = ""
    else:
        fix.loc[0, "MRD"] = str(int(round(km_to_nmi(sar_rmax / 1000))))

    fix_radii = pd.DataFrame(columns=fix.columns)
    # quadrants = ["all", "NE", "SE", "SW", "NW"]
    wind_vals = [64, 50, 34]
    for i_wnd in range(len(wind_vals)):
        new_row = fix.iloc[0].copy(deep=True)
        new_row["windcode"] = "NEQ"
        new_row["rad"] = str(int(wind_vals[i_wnd]))
        new_row["rad1"] = ""
        new_row["rad2"] = ""
        new_row["rad3"] = ""
        new_row["rad4"] = ""
        if percent_valid_quad["NE"][i_wnd + 2] > 75 and not np.isnan(radii_nmi_quad["NE"][i_wnd + 2]):
            new_row["rad1"] = str(int(round(radii_nmi_quad["NE"][i_wnd + 2])))
        if percent_valid_quad["SE"][i_wnd + 2] > 75 and not np.isnan(radii_nmi_quad["SE"][i_wnd + 2]):
            new_row["rad2"] = str(int(round(radii_nmi_quad["SE"][i_wnd + 2])))
        if percent_valid_quad["SW"][i_wnd + 2] > 75 and not np.isnan(radii_nmi_quad["SW"][i_wnd + 2]):
            new_row["rad3"] = str(int(round(radii_nmi_quad["SW"][i_wnd + 2])))
        if percent_valid_quad["NW"][i_wnd + 2] > 75 and not np.isnan(radii_nmi_quad["NW"][i_wnd + 2]):
            new_row["rad4"] = str(int(round(radii_nmi_quad["NW"][i_wnd + 2])))
        fix_radii = pd.concat([fix_radii, new_row], ignore_index=True)

    if os.path.isfile(atcf_outfile):
        logger.info(f"ATCF output file exists ({atcf_outfile}). Reading and merging.")

        _, existing_fix = atcf.read(atcf_outfile, model="fix", multi_return=True)
        existing_fix["date"] = existing_fix["date"].apply(lambda x: x.strftime("%Y%m%d%H%M"))
        existing_fix["start time"] = existing_fix["start time"].apply(lambda x: x.strftime("%Y%m%d%H%M"))
        existing_fix["end time"] = existing_fix["end time"].apply(lambda x: x.strftime("%Y%m%d%H%M"))

        existing_fix = remove_floating_part(existing_fix)

        to_save = pd.concat([existing_fix, fix_radii], ignore_index=True)
        to_save["start time"] = to_save["start time"].astype("string")
        to_save["rad"] = to_save["rad"].astype("string")
        to_save = to_save.drop_duplicates(subset=["start time", "rad"], keep="last")
        to_save["CY"] = to_save["CY"].apply(lambda x: str(x).zfill(2))
        to_save = to_save.sort_values(by=["start time"])
    else:
        to_save = fix_radii

    df_result = to_save

    atcf.write(df_result, open(atcf_outfile, "w"), model="fix")
    logger.info("wrote %s" % atcf_outfile)

    atcf.write(fix_radii, open(sar_outfile, "w"), model="fix")
    logger.info("wrote %s" % sar_outfile)

