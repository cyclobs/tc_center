#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  5 16:25:11 2020

@author: lvinour
"""
import collections

import numpy as np
import netCDF4 as nc4
import scipy as scp
import scipy.interpolate as interp
import shapely
import shapely.geometry as shp
from polylabel import polylabel
import scipy.signal as signal
from pyproj import Geod
from scipy.spatial import ConvexHull
from scipy.stats import norm
import matplotlib as mpl
import colormap
from functools import wraps, partial
import time
import os
import logging
from numba import jit
from shapely import wkt
from enum import Enum
import cartopy
from shapely.geometry import LineString
from shapely.ops import unary_union, transform, nearest_points
from shapely.prepared import prep
import cartopy.geodesic as cgeo
import pyproj
import pandas as pd
import geopandas as gpd
import owi
import datetime

logger = logging.getLogger(__name__)

# Initialize memory monitor
mem_monitor = True
try:
    from psutil import Process
except ImportError:
    logger.warning("psutil module not found. Disabling memory monitor")
    mem_monitor = False


def timing(f):
    """provide a @timing decorator for functions, that log time spent in it"""

    @wraps(f)
    def wrapper(*args, **kwargs):
        mem_str = ''
        process = None

        if mem_monitor:
            process = Process(os.getpid())
            startrss = process.memory_info().rss

        starttime = time.time()
        result = f(*args, **kwargs)
        endtime = time.time()

        if mem_monitor:
            endrss = process.memory_info().rss
            mem_str = 'mem: %+.1fMb' % ((endrss - startrss) / (1024 ** 2))

        logger.debug('timing %s : %.1fs. %s' % (f.__name__, endtime - starttime, mem_str))

        return result

    return wrapper

def contour_indices(arr, lon):
    """
    Retrieve 2D indexes of first shape found in arr
    """
    arr[arr.mask] = 0

    shapes = next(features.shapes(arr))
    coords = shapes[0]['coordinates'][0]
    idx = np.array(coords, dtype='i4')

    # Deleting indices that are outside of lon/lat length
    sup = np.where(idx[:, 1] >= lon.shape[0])[0]
    idx0 = np.delete(idx[:, 1], sup, 0)
    idx1 = np.delete(idx[:, 0], sup, 0)

    sup = np.where(idx1 >= lon.shape[1])[0]
    idx0 = np.delete(idx0, sup, 0)
    idx1 = np.delete(idx1, sup, 0)

    zeros = np.where(idx0 == 0)[0]
    idx0 = np.delete(idx0, zeros, 0)
    idx1 = np.delete(idx1, zeros, 0)

    return idx0, idx1


def api_req(sat_file, api_url, partial=False):
    """
    Query CyclObs data API for the given sat_file.

    Parameters
    ----------
    sat_file: str uniquely identifying a satellite acquisition

    Returns
    -------
    pandas.DataFrame
        DataFrame containing API data about a satellite acquisition.

    """
    if partial is False:
        file_api = os.path.basename(owi.L2CtoLight(sat_file, resolution=3, mode="ll_gd"))
    else:
        file_api = sat_file
    api_req = f"{api_url}/app/api/getData?include_cols=all&filename={file_api}"
    logger.info(f"Getting data from Cyclobs API. Req : {str(api_req)}")

    df = pd.read_csv(api_req, keep_default_na=False)

    return df



@timing
def open_SAR_image(slice_SAR, extra_vars=[]):
    """ Open a SAR netCDF file and extract 2D fields for level-2 image processing
    Input : slice_SAR = string of NetCDF file path, extra_vars (opt) = string of additional variable to extract (ex : 'nrcs_cross')
    Outputs : 2D masked arrays. lon=Longitude in degrees, lat=Latitude in degrees, windspd=Wind Speed (m/s),\
    winddir=wind streaks directions (degrees from north), nrcs=Normalized Radar Corss Section, nd=Nice Display, filt=Homogeneity filter (i.e. mask for rain signatures)"""

    logger.info(f"file : {slice_SAR}")
    logger.info('extracting SAR image info for file ' + slice_SAR.split('/')[-2] + '...')
    # open file  and extract variables #
    nc = nc4.Dataset(slice_SAR, 'r')
    nc.set_always_mask(True)
    lon = nc.variables['lon'][0]
    lat = nc.variables['lat'][0]
    nrcs = nc.variables['nrcs_co'][0]
    FB = nc.variables['mask_flag'][0]
    #nd = nc.variables['nrcs_detrend_co'][0]
    #winddir = nc.variables['wind_streaks_orientation'][0]
    # standardize directions between 0 and 360 degrees #
    #winddir[winddir < 0] = winddir[winddir < 0] + 360
    windspd = nc.variables['wind_speed'][0]
    # set all values of the Binary filter to 0 or 1 (fix for eventual anomalous values)
    FB[FB > 0] = 1
    # handle masks of 2D arrays
    if len(np.shape(windspd.mask)) == 0:
        windspd.mask = np.zeros(np.shape(windspd))
    nrcs.mask = windspd.mask
    #nd.mask = windspd.mask
    # handle eventual NaN values in the lon/lat grid
    if len(np.where(np.isnan(lon))[0]) > 0:
        windspd.mask[np.where(np.isnan(lon))] = True
        lon[np.where(np.isnan(lon))] = np.nanmin(lon)
        lat[np.where(np.isnan(lat))] = np.nanmin(lat)

    # extract acquisition bounding box
    footprint = getattr(nc, "footprint")

    # extract additional variables if existing
    vars_extra = []
    if extra_vars != []:
        for varname in extra_vars:
            try:
                var_extra = nc.variables[varname][0]
                var_extra.mask = windspd.mask
                vars_extra.append(var_extra)
            except:
                logger.warning('Variable name does not exist in file ' + slice_SAR)
    nc.close()
    # handle outliers in the lon/lat grid, i.e. anomalous values of lon/lat : if outliers are spotted, \
    # the longitudes and latitudes are redefined on a regular grid of same dimensions as the original
    nanlon = tonan_outliers(np.gradient(lon, axis=0) ** 2 + np.gradient(lon, axis=1) ** 2, m=10)
    if len(np.where(np.isnan(nanlon) == 1)[0]) > 0:
        lon = interp.griddata((np.indices(np.shape(lon))[0][~np.isnan(nanlon)].ravel(),
                               np.indices(np.shape(lon))[1][~np.isnan(nanlon)].ravel()), lon[~np.isnan(nanlon)].ravel(), \
                              (np.indices(np.shape(lon))[0], np.indices(np.shape(lon))[1]), 'nearest')
        lat = interp.griddata((np.indices(np.shape(lat))[0][~np.isnan(nanlon)].ravel(),
                               np.indices(np.shape(lat))[1][~np.isnan(nanlon)].ravel()), lat[~np.isnan(nanlon)].ravel(), \
                              (np.indices(np.shape(lat))[0], np.indices(np.shape(lat))[1]), 'nearest')
    filt = FB

    if vars_extra != []:
        return lon, lat, windspd, nrcs, filt, footprint, vars_extra
    else:
        return lon, lat, windspd, nrcs, filt, footprint


def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees)
    input can be scalar or arrays.
    """

    # convert decimal degrees to radians 
    [lon1, lat1, lon2, lat2] = [np.radians(lon1), np.radians(lat1), np.radians(lon2), np.radians(lat2)]

    # haversine formula 
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = np.sin(dlat / 2) ** 2 + np.cos(lat1) * np.cos(lat2) * np.sin(dlon / 2) ** 2
    c = 2 * np.arcsin(np.sqrt(a))
    r = 6371  # Radius of earth in kilometers. Use 3956 for miles

    return c * r


def mask_outliers(data, m=2, window_size=10, sliding_bool=False):
    """ This function masks the outliers of an array or vector, using the Median of Absolute Deviations \
    (MAD: https://www.influxdata.com/blog/anomaly-detection-with-median-absolute-deviation/). 
    Inputs : data=2D input array. kwargs : m=threshold for outlier detection, sliding_bool=boolean, True if outlier detection should be done on a sliding window. If True, \
    optionally add "window_size" to kwargs to set the sliding window size (default value is 10).
    Output : data = the masked 2D or 1D array """

    data = data.copy()
    if not isinstance(data, np.ma.MaskedArray):
        data = np.ma.array(data)
        data.mask = data * 0

    if sliding_bool == False:
        data.mask[np.isnan(data)] = 1
        difference = np.abs(data - np.ma.median(data))
        median_difference = np.ma.median(difference)
        if median_difference == 0:
            s = 0
        else:
            s = difference / float(median_difference)
        mask = s > m
        data.mask[mask] = 1
    else:
        data.mask[np.isnan(data)] = 1
        for inds in sliding_window(data, window_size):

            data_inds = data[inds]
            difference = np.abs(data_inds - np.ma.median(data_inds))
            median_difference = np.ma.median(difference)
            if median_difference == 0:
                s = 0
            else:
                s = difference / float(median_difference)
            mask = s > m
            data.mask[inds[mask]] = 1

    return data


def tonan_outliers(data, m=2, sliding_bool=False, window_size=10):
    if not isinstance(data, np.ma.MaskedArray):
        data = np.ma.array(data, mask=data * 0)
        data.mask = data * 0
    if (data.mask.dtype == bool):
        data = np.ma.array(data, mask=data * 0)
        data.mask = data * 0
    data.mask[np.isnan(data)] = True
    if sliding_bool == False:
        data = data.copy()
        difference = np.abs(data - np.ma.median(data))
        median_difference = np.ma.median(difference)
        if median_difference == 0:
            s = difference / float(np.ma.mean(difference))
            mask = s > 1
        else:
            median_difference = np.ma.median(difference)
            s = difference / float(median_difference)
            mask = s > m
        data[mask] = np.nan
    else:
        data = data.copy()
        for inds in sliding_window(data, window_size):
            data_inds = data[inds].copy()
            difference = np.abs(data_inds - np.ma.median(data_inds))
            median_difference = np.ma.median(difference)
            if median_difference == 0:
                s = 0
            else:
                s = difference / float(median_difference)
            mask = s > m
            data[inds[mask]] = np.nan
    return data


def sliding_window(x, window_size=10):
    inds = []
    for i in range(0, len(x)):
        inds.append(
            (np.arange(max(0, i - np.floor(window_size / 2)), min(len(x), i + np.floor(window_size / 2)))).astype(int))
    return inds


@jit(nopython=True)
def sliding_outlier_detection(data, stepSize, ni, nj):
    # slide a window across the image
    data_window = 0.0
    for i in range(ni, data.shape[0] - ni, stepSize):
        for j in range(nj, data.shape[1] - nj, stepSize):
            data_window = data[i - ni:i + ni + 1, j - nj:j + nj + 1]
            # yield the current window
            yield i, j, data_window


@timing
def proc_research_eye(lon, lat, var, lon_eye_ref, lat_eye_ref, rad_research):
    """ This function runs a research procedure to find a stable low wind area inside a given radius around a given position, by \
    iteratively computing a low wind barycenter and searching for a recurring point 
    Inputs : lon,lat,var = 2D fields of longitude, latitude, variable to process. 
    lon_eye_ref,lat_eye_ref =  longitude and latitude of reference center. rad_research = research radius
    Outputs : lon_eye_lwnd_new,lat_eye_lwnd_new=new position of the low wind barycenter;
    i_lwind_new,j_lwind_new=new indices of the low wind barycenter;
    n_iter=number of iterations performed;
    lons_eye,lats_eye=list of consecutive center positions computed"""

    # compute a first low wind barycenter #

    # Retrieve indices inside search radius
    i_close, j_close = np.ma.where(haversine(lon, lat, lon_eye_ref, lat_eye_ref) <= rad_research)

    # Find indices of low wind area inside search radius
    i_lwind, j_lwind = np.ma.where((var < np.ma.min(var[i_close, j_close]) + 0.05 * (
            np.ma.max(var[i_close, j_close]) - np.ma.min(var[i_close, j_close]))) & \
                                   (haversine(lon, lat, lon_eye_ref, lat_eye_ref) <= rad_research))

    # Get barycenter point of the low wind area
    i_eye_l = int(np.floor(np.ma.median(i_lwind)))
    j_eye_l = int(np.floor(np.ma.median(j_lwind)))
    # Get lon/lat of the barycenter of the low wind area.
    lon_eye_lwnd = lon[i_eye_l, j_eye_l]
    lat_eye_lwnd = lat[i_eye_l, j_eye_l]

    # Iteratively, compute low wind barycenters around the first one, and stock them in an array. #
    # If a value is found 2 times in the array, it is kept as the final position #
    # If more than 10 iterations are necessary, the loop stops #

    lon_eye_lwnd_new = []  # lon[i_eye_l,j_eye_l]
    lat_eye_lwnd_new = []  # lat[i_eye_l,j_eye_l]
    n_iter = 0
    lons_eye = []
    lats_eye = []
    while len(lons_eye) == 0 or len(lats_eye) == 0 or \
            (not [lon_eye_lwnd_new, lat_eye_lwnd_new] in np.column_stack((lons_eye, lats_eye))):
        lons_eye.append(lon_eye_lwnd)
        lats_eye.append(lat_eye_lwnd)
        n_iter += 1
        if n_iter > 10:
            break
        if n_iter >= 2:
            lon_eye_lwnd, lat_eye_lwnd = lon_eye_lwnd_new, lat_eye_lwnd_new
        i_close, j_close = np.ma.where(haversine(lon, lat, lon_eye_lwnd, lat_eye_lwnd) <= rad_research)
        i_lwind_new, j_lwind_new = np.ma.where((var < np.ma.min(var[i_close, j_close]) + 0.05 * (
                np.ma.max(var[i_close, j_close]) - np.ma.min(var[i_close, j_close]))) & \
                                               (haversine(lon, lat, lon_eye_lwnd, lat_eye_lwnd) <= rad_research))

        i_lwind_new, j_lwind_new = i_lwind_new[
                                       ~np.isnan(tonan_outliers((i_lwind_new * j_lwind_new).astype(float), m=3))], \
                                   j_lwind_new[
                                       ~np.isnan(tonan_outliers((i_lwind_new * j_lwind_new).astype(float), m=3))]
        i_eye_l = int(np.floor(np.ma.median(i_lwind_new)))
        j_eye_l = int(np.floor(np.ma.median(j_lwind_new)))
        lon_eye_lwnd_new = lon[i_eye_l, j_eye_l]
        lat_eye_lwnd_new = lat[i_eye_l, j_eye_l]
        # print n_iter,lon_eye_lwnd,lat_eye_lwnd_new,lon_eye_lwnd_new,lat_eye_lwnd_new

    return lon_eye_lwnd_new, lat_eye_lwnd_new, i_lwind_new, j_lwind_new, n_iter, lons_eye, lats_eye


def mask2nan(A):
    B = np.ma.copy(A)
    B = B.data.astype(float)
    B[A.mask] = np.nan
    return B


def interp_nan(sig, **kwargs):
    method = kwargs.get('method', 'linear')
    if isinstance(sig, np.ma.MaskedArray):
        sig_nomask = sig.data
        sig_nomask[sig.mask == True] = np.nan
        sig = sig_nomask
    inds_nan = np.arange(0, len(sig))
    sig_int = interp.griddata(inds_nan[~np.isnan(sig)], sig[~np.isnan(sig)], inds_nan, method)
    sig = interp.griddata(inds_nan[~np.isnan(sig_int)], sig_int[~np.isnan(sig_int)], inds_nan, 'nearest')
    return sig


def smth_butter(sig, N, Wn, per_bool=True, rm_outl_bool=False, m=2, sliding_bool=False, window_size=10):
    """ Apply a Butterworth filter to a signal (sig), given its order N and cutoff frequency Wn with options depending on signal type :
        set per_bool to True if the signal is periodic (i.e. azimuthal distribution for instance).
        set rm_outl_bool to True if an additional removal of outliers is required previous to smoothing 
        (often necessary to impede smoothing artifacts)"""

    if isinstance(sig, np.ma.MaskedArray):
        sig = mask2nan(sig)

    if rm_outl_bool == True:
        sig = tonan_outliers(sig, m=m, sliding_bool=sliding_bool, window_size=window_size)
    if per_bool == True:
        # make periodic signal
        sig_per = np.hstack((sig, sig, sig))
        # if nan in array, cubic interpolation
        if len(np.where(np.isnan(sig) == True)) > 0:
            sig_per = interp_nan(sig_per)
        # Apply Butterworth filter
        bflt, aflt = signal.butter(N, Wn)
        sig_flt = signal.filtfilt(bflt, aflt, sig_per)
        # Extract 1 period to get back the original shape
        sig_flt = sig_flt[len(sig):2 * len(sig)]
    else:
        if len(np.where(np.isnan(sig) == True)) > 0:
            sig = interp_nan(sig)

        # Apply Butterworth filter
        bflt, aflt = signal.butter(N, Wn)
        sig_flt = signal.filtfilt(bflt, aflt, sig)
    return sig_flt


def butter_lowpass_filter(data, cutoff, fs, order=2, m=2,
                          sliding_bool=False, per_bool=True,
                          window_size=10, rm_outl_bool=False):
    """ Apply a Butterworth low-pass filter to a signal (data), given the cutoff frequency (cutoff) and the sampling frequency (fs) """

    # print("Cutoff freq " + str(cutoff))
    nyq = 0.5 * fs  # Nyquist Frequency
    normal_cutoff = cutoff / nyq
    if normal_cutoff >= 1:
        normal_cutoff = 0.99
    # Get the filter coefficients
    y = smth_butter(data, order, normal_cutoff, m=m, sliding_bool=sliding_bool,
                    per_bool=per_bool, window_size=window_size, rm_outl_bool=rm_outl_bool)

    return y


def smth_sig_expvar_pct(sig, m=2, sliding_bool=False, expvar_frac=0.4, per_bool=True,
                        window_size=10, rm_outl_bool=False,
                        t_vect=None, cutoff_period=0.0):
    """Advanced smoothing function to smooth a signal (sig) with empirical parameters.
    Smoothing can be based on a percentage of explained variance (argument expvar_frac=0.4 by default) : the smoothing will have
    a cutoff frequency equal to the spectral frequency above which the cumulated explained variance exceeds the threshold value 
    expvar_frac
    Smoothing can also be applied to temporal signals, provided a time vector t_vect in seconds. In this case, the cutoff 
    frequency in hours can be prescribed by cutoff_period : the curoff frequency is then 1/cutoff_period"""

    if t_vect is None:
        t_vect = np.arange(0, len(sig))
    # t_vect = kwargs.get('t_vect', np.arange(0, len(sig)))
    # expvar_pct = kwargs.get('expvar_frac', 0.4)
    # cutoff_period = kwargs.get('cutoff_period', 0.0)
    if len(np.where(np.isnan(np.array(sig)))[0]) < max(5, len(sig) - 3):

        # Interpolate NaN values linearly and detrend the signal #
        sig_nonan = interp_nan(sig, method='linear')
        sig_n = sig_nonan - np.mean(sig_nonan)
        # compute the fft and the sampling frequency #
        timestep = np.diff(t_vect)[0]
        fft = scp.fft.fft(sig_n)
        amp = 2. / t_vect.size * np.abs(fft)
        freq = np.abs(scp.fft.fftfreq(t_vect.size, d=timestep))
        amp = amp[0:int(len(amp) / 2)]
        freq = freq[0:int(len(freq) / 2)]
        fs = 1 / np.diff(t_vect)[0]
        if len(np.where(amp > 0.)[0]) > 0:

            if cutoff_period == 0.:
                # definition of cutoff period of the filter if the signal is not time-dependent #
                freq_co = np.where(np.cumsum(amp) / np.sum(amp) > expvar_frac)[0].min()
                cutoff = freq[max(freq_co, 5)]
            else:
                # definition of cutoff period of the filter if the signal is time-dependent #
                cutoff = 1. / (3600. * cutoff_period)
            # apply the Butterworth filter #
            sig_flt = butter_lowpass_filter(sig, cutoff, fs, m=m, sliding_bool=sliding_bool,
                                            per_bool=per_bool, window_size=window_size,
                                            rm_outl_bool=rm_outl_bool)
        else:
            sig_flt = 0, sig
    else:
        sig_flt = 0, sig

    return sig_flt


@timing
def maquantile(array, k):
    array_no_outl = array.copy()
    array_no_outl[array_no_outl.mask == True] = np.nan
    q = np.nanquantile(array_no_outl, k)
    return q


def sig_proc(sig, m=2, sliding_bool=False, expvar_frac=0.4, per_bool=True,
             window_size=10, rm_outl_bool=False,
             t_vect=None, cutoff_period=0.0,
             norm="std_div"):
    sig_flt = smth_sig_expvar_pct(sig, m=m, sliding_bool=sliding_bool,
                                  expvar_frac=expvar_frac, per_bool=per_bool,
                                  window_size=window_size, rm_outl_bool=rm_outl_bool,
                                  t_vect=t_vect, cutoff_period=cutoff_period)
    # norm = kwargs.get('norm', 'std_div')
    if norm == 'std_div':
        if np.nanstd(sig) == 0:
            sig_std_flt = sig_flt
        else:
            sig_std_flt = (sig_flt - np.nanmean(sig_flt)) / np.nanstd(sig_flt)
    elif norm == 'mean_div':
        if np.nanstd(sig_flt) == 0:
            sig_std_flt = sig_flt
        else:
            sig_std_flt = (sig_flt - np.nanmean(sig_flt)) / np.nanmean(sig_flt)
    elif norm == 'mean_norm':
        if np.nanstd(sig_flt) == 0:
            sig_std_flt = sig_flt
        else:
            sig_std_flt = sig_flt - np.nanmean(sig_flt)
    else:
        raise ValueError('wrong norm name. Accepted names : std_div, mean_div, mean_norm')

    return [sig_std_flt, sig_flt]


def get_avg_azim_wind(wind_pol, i, k):
    """ This funtion averages a radial signal with its azimuth neighbors\
    Inputs : wind_pol (polar projected signal), i (azimuth value), k (half number of neighbors) """
    i = int(i)
    if (i < k):
        wind_avg_azim = np.mean(wind_pol[:, 0:2 * k + 1], axis=1)
    elif (i > len(wind_pol[0]) - k - 1):
        wind_avg_azim = np.mean(wind_pol[:, -2 * k::], axis=1)
    else:
        wind_avg_azim = np.mean(wind_pol[:, i - k:i + k + 1], axis=1)
    return wind_avg_azim


def mask_interp_azim_vect(rad, vect, rad_interp, vect_interp):
    ind_mask = np.where(vect.mask == True)[0]
    vect_interp_out = vect_interp.copy()
    for ii in ind_mask:
        if ii < len(rad) - 1:
            vect_interp_out.mask[np.where((rad_interp >= rad[ii - 1]) & (rad_interp <= rad[ii + 1]))] = True
    return vect_interp_out


@jit(nopython=True)
def mask_interp_azim_vect_nb(rad, vect, rad_interp, vect_interp):
    ind_mask = np.where(np.isnan(vect))[0]
    vect_interp_out = vect_interp.copy()
    for ii in ind_mask:
        if ii < len(rad) - 1:
            vect_interp_out[np.where((rad_interp >= rad[ii - 1]) & (rad_interp <= rad[ii + 1]))] = np.nan
    return vect_interp_out


@timing
def extract_wind_contour(windval, windspd_pol, r_vect, theta_vect, r_vect_refine, az_maxwind):
    """ Extract closed wind contour in the eyewall based on a given wind value (windval) : starting at a given azimuth (az_maxwind), the retrieves radii 
    corresponding to the closest value to windval
    Inputs : winval = reference wind value; windspd_pol = polar projected wind speed; r_vect = radius vector; theta_vect = azimuth vector
    r_vect_refine = refined radius vector with finer definition; az_maxwind = reference azimuth
    Output : rads_shp = distribution of eye shape contour"""

    rads_shp = np.ma.zeros(np.shape(theta_vect)[0])
    rads_shp.mask = rads_shp + 1
    r_vect_refine = np.linspace(0, 3, 30000)

    for j in range(0, len(theta_vect)):
        # Guessing that jj is an azimutal index using the reference az_maxwind
        jj = (j + az_maxwind) % 361
        # Cmpute avg wind on the current azimutal profile using 3 neighbours
        windspd_azim = get_avg_azim_wind(windspd_pol, jj, 3)
        # Get radius of maximum wind for this azimuth
        rmaxwnd = r_vect[np.ma.where(windspd_azim == np.ma.max(windspd_azim))[0][0]]
        # Compute ratio of masked/non-masked indexes starting from the max value of wind speed on that azimuth
        q_pts_masked_j = float(len(np.where(windspd_azim[windspd_azim.argmax()::].mask == True)[0])) / float(
            len(windspd_azim[windspd_azim.argmax()::]))
        if q_pts_masked_j < 0.99:
            if (len(np.where(windspd_azim.mask == False)[0]) > 0):
                windspd_azim_interp = np.ma.array(
                    interp.griddata(r_vect[~windspd_azim.mask], windspd_azim[~windspd_azim.mask], r_vect_refine,
                                    'linear'))
                windspd_azim_interp.mask = r_vect_refine * 0
                windspd_azim_interp.mask[np.where(r_vect_refine >= rmaxwnd)] = True
                windspd_azim_interp.mask[r_vect_refine < rmaxwnd / 20.] = True
                windspd_azim_interp = mask_interp_azim_vect(r_vect, windspd_azim, r_vect_refine, windspd_azim_interp)

                # If there is at least 2 values in windspd_azim_interp near (+-1) the given windval
                if np.size(np.ma.where(np.ma.abs(np.ma.array(windspd_azim_interp - (windval))) <= 1.)) > 1:
                    # Find the minimum delta between windspd_azim_interp and windval, check if it is not masked
                    if not (windspd_azim_interp.mask[np.where(r_vect_refine == np.min(r_vect_refine[(
                            np.ma.where(np.ma.abs(np.ma.array(windspd_azim_interp - (windval))) <= 1.))]))][0]):
                        # Store that value in rads_shp
                        val_rad = np.min(
                            r_vect_refine[(np.ma.where(np.ma.abs(np.ma.array(windspd_azim_interp - (windval))) <= 1.))])
                        rads_shp[jj] = val_rad
                else:
                    # Find minimum valid minimum value for delta between windspd_azim_interp and windval, check that
                    # it is not masked and store it in rads_shp
                    if np.ma.abs(np.ma.array(windspd_azim_interp - (windval))).argmin() < len(windspd_azim_interp) - 1:
                        if not (
                                windspd_azim_interp.mask[
                                    np.ma.abs(np.ma.array(windspd_azim_interp - (windval))).argmin() + 1]):
                            val_rad = r_vect_refine[np.ma.abs(np.ma.array(windspd_azim_interp - (windval))).argmin()]
                            rads_shp[jj] = val_rad

    # Mask values that are far from the eye ?
    rads_shp.mask[rads_shp > 4 * r_vect[np.mean(windspd_pol, axis=1).argmax()]] = True
    if not len(np.where(rads_shp.mask == True)[0]) == len(rads_shp):
        remaining_outliers = np.where(
            mask_outliers(np.abs(rads_shp - smth_butter(rads_shp, 3, 0.03)), m=10).mask == True)
        rads_shp.mask[remaining_outliers] = True

    return rads_shp


def gradient_xlen(data, dx):
    """ This function computes a "smoothed gradient" of a curve : the gradient is computed at each point of data on a window which size is prescribed by data.
    Ex : if dx=5, grad_smooth[i]=data[i+5]-data[i-5]"""
    gradient = np.zeros(np.shape(data))
    for i in range(0, len(data)):
        if (i < dx) | (i >= len(data) - dx):
            gradient[i] = 0.
        else:
            gradient[i] = data[i + dx] - data[i - dx]
    return gradient


@timing
def shape_eye(r_vect, theta_vect, windspd_pol, method=1):
    """ Compute the eye shape given the polar-projected wind field (windspd_pol) and the radius (r_vect) and
    azimuth (theta_vect) vectors. Two different methods are available :
        method=1 : custom method based on the definition of several wind contours and selection among these contours based on their variance and number of points
        method=2 : method with a wind contour computed from the pdf of wind values around the eye. From Zheng et al. 2017 (https://link.springer.com/chapter/10.1007/978-981-10-2893-9_9)"""

    if method == 1:

        """ method with several contour values """

        # first, we find wind values that will be used as references for finding the eye shape contour. These values are found #
        # by retrieving the wind value at the radius of maximum radial gradient in the eyewall, and taking ten values of wind  #
        # lower than this value #

        # take azimuths of highest wind (0.995<wind<0.999 quantiles) #
        thetas_hwinds = np.array(list(set(np.ma.where(
            (windspd_pol >= maquantile(windspd_pol, 0.995)) & (windspd_pol <= maquantile(windspd_pol, 0.999)))[
                                              1].tolist())))
        az_maxwind = int(np.median(thetas_hwinds))
        contour_values = []
        inds_maxgrd = []
        for theta_vmax in thetas_hwinds:
            # for each of these high wind azimuths, retrieve the radial profile, smooth it #
            windspd_az_vmax = get_avg_azim_wind(windspd_pol.copy(), theta_vmax, 3)
            windspd_az_vmax_smth = smth_sig_expvar_pct(windspd_az_vmax, expvar_frac=0.6)
            # retrieve the smoothed "sliding" gradient of the profile #
            windspd_azim_grd = gradient_xlen(windspd_az_vmax_smth, 5)

            # retrieve radius of maximum wind on this profile #
            rmaxwnd_vmax = r_vect[np.where(windspd_az_vmax == np.max(windspd_az_vmax))[0][0]]

            # find the position of maximum gradient between 0.1*rmw_az & rmw_az, to avoid large gradient artifacts in the eye and far from the eye (rain signatures for instance). #
            i_maxgrd = np.where(windspd_azim_grd == np.max(
                windspd_azim_grd[(r_vect <= rmaxwnd_vmax) & (r_vect >= rmaxwnd_vmax / 10.)]))[0][0]
            inds_maxgrd.append(i_maxgrd)

            # take reference wind value as wind value corresponding to this max. gradient position #
            wind_value_contour = windspd_az_vmax_smth[i_maxgrd]
            contour_values.append(wind_value_contour)

        # define 10 reference wind values based on : the mean of all max. gradient matching wind values from previous
        # loop (max. threshold) and the 0.1 quantile of a smoothed high wind profile #
        wind_values_contours = np.linspace(
            np.quantile(smth_sig_expvar_pct(windspd_pol[:, az_maxwind], expvar_frac=0.3, per_bool=False)[
                        0:max(1, int(np.mean(inds_maxgrd)))], 0.1), np.mean(contour_values), 10)

        # for each of these values, we stock : the corresponding closed contour; the number of non-masked points in the retrieved contour; 
        # the standard deviation and STD of the gradient of the extracted closed contour #
        r_vect_refine = np.linspace(0, 3, 30000)
        nb_pnts = []
        list_rads_shp = []
        stds = []
        stds_grad = []
        contours_to_remove = []
        for i in range(0, len(wind_values_contours)):
            windval = wind_values_contours[i]
            # extract the closed contour with mask where the contour could not be closed with sufficient continuity in the signal #
            rads_shp = extract_wind_contour(windval, windspd_pol, r_vect, theta_vect, r_vect_refine, az_maxwind)
            if not len(np.where(rads_shp.mask == True)[0]) == len(rads_shp):
                nb_pnts.append(float(len(np.where(rads_shp.mask == False)[0])) / len(rads_shp))
                stds.append(
                    np.std(sig_proc(rads_shp, expvar_frac=0.99, t_vect=theta_vect, norm='mean_div', per_bool=True,
                                    rm_outl_bool=False, m=20, sliding_bool=True, window_size=20)[0]))
                stds_grad.append(np.std(np.gradient(
                    sig_proc(rads_shp, expvar_frac=0.99, t_vect=theta_vect, norm='mean_div', per_bool=True,
                             rm_outl_bool=False, m=20, sliding_bool=True, window_size=20)[0])))
                list_rads_shp.append(rads_shp)
            else:
                contours_to_remove.append(i)

        wind_values_contours = np.delete(wind_values_contours, contours_to_remove)
        # defined an empirical score for each contour as the sum of :
        # - its index i.e. its distance from the center (we favour countours further from the center that estimate larger eye shape)
        # - its score in terms of STD (we favour low STDs to avoid distorted signals)
        # - its score in terms of STD of the gradient (we favour low gradient STD to avoid signals with high variability that often traduce local artifacts)

        rks = np.zeros(len(wind_values_contours))
        for i in range(0, len(wind_values_contours)):
            arg_std = np.where(np.argsort(stds) == i)[0]
            arg_std_grad = np.where(np.argsort(stds_grad) == i)[0]
            rks[i] = len(wind_values_contours) - i + arg_std + arg_std_grad

        # the final contour is the one with the best rank, taken from the signal being more than 70% complete (50% if none is found)
        for frac in [0.7, 0.5]:
            if any((np.array(nb_pnts) > frac) & (rks <= np.median(rks))):
                list_rads_shp_frac = np.ma.array(list_rads_shp)[np.array(nb_pnts) > frac]
                # stds_frac = np.array(stds)[np.array(nb_pnts)>frac]
                rads_shp = list_rads_shp_frac[
                    np.max(np.where(rks[np.array(nb_pnts) > frac] == rks[np.array(nb_pnts) > frac].min())[0])]
                break

    elif method == 2:

        """ method with one contour from pdf """
        rmax_avg = np.mean(windspd_pol, axis=1).argmax()
        if rmax_avg < 10:
            rmax_avg = \
                np.where((np.gradient((smth_butter(np.mean(windspd_pol, axis=1), 3, 0.05, per_bool=0)))[1::] < 0.) & \
                         (np.gradient((smth_butter(np.mean(windspd_pol, axis=1), 3, 0.05, per_bool=0)))[0:-1] >= 0.))[
                    0][0]
        windspd_around_eye = np.ravel(windspd_pol[0:rmax_avg])
        windspd_around_eye = windspd_around_eye[windspd_around_eye.mask == False].data
        windspd_in_eye = np.ravel(windspd_pol[0:max(1, rmax_avg / 5)])
        windspd_in_eye = windspd_in_eye[windspd_in_eye.mask == False].data
        windspd_far_eye = np.ravel(windspd_pol[0:3 * rmax_avg])
        windspd_far_eye = windspd_far_eye[windspd_far_eye.mask == False].data
        x_d_around_eye = np.linspace(np.min(windspd_around_eye), np.max(windspd_around_eye), 50)
        density_around_eye = sum(norm(xi).pdf(x_d_around_eye) for xi in windspd_around_eye)
        ind_peaks = \
            np.where((np.gradient(density_around_eye)[0:-1] >= 0.) & (np.gradient(density_around_eye)[1::] < 0.))[0]
        if len(ind_peaks) >= 2:
            dtype = [('peak_ind', int), ('value', float)]
            values = zip(ind_peaks, [density_around_eye[ii] for ii in ind_peaks])
            values_peaks = np.array(values, dtype=dtype)
            ind_peaks = np.sort(values_peaks, order='value')['peak_ind'][::-1][0:2]
            windval = x_d_around_eye[ind_peaks.min():ind_peaks.max()][
                density_around_eye[ind_peaks.min():ind_peaks.max()].argmin()]
            r_vect_refine = np.linspace(0, 3, 30000)
            rads_shp = extract_wind_contour(windval, windspd_pol, r_vect, theta_vect, r_vect_refine, 0)
            nb_pnts = float(len(np.where(rads_shp.mask == False)[0])) / len(rads_shp)
            print(windval)
        else:
            print('eye shape algo failed')
            windval = np.mean(x_d_around_eye)
            r_vect_refine = np.linspace(0, 3, 30000)
            rads_shp = extract_wind_contour(windval, windspd_pol, r_vect, theta_vect, r_vect_refine, 0)
            nb_pnts = float(len(np.where(rads_shp.mask == False)[0])) / len(rads_shp)

    return rads_shp


@timing
def rmv_npts(array, n_pts, high_low='high'):
    """ This function removes the n_pts highest or lowest (high_low='high' or 'low') values from an array """
    array_no_outl = array.copy()
    array_no_outl[array_no_outl.mask == True] = np.nan
    if high_low == 'high':
        array_no_outl[np.where(array >= np.nanquantile(array_no_outl, 1. - n_pts / np.size(array)))] = np.nan
    elif high_low == 'low':
        array_no_outl[np.where(array <= np.nanquantile(array_no_outl, n_pts / np.size(array)))] = np.nan
    array = array_no_outl.copy()
    array.mask[np.isnan(array)] = True
    return array


@timing
def rmv_outside_field(lon, lat, lon2, lat2, var):
    """ Mask the points of variable var on grid lon2,lat2 that are outside of a second grid lon,lat. Masking is carried out \
    by computing the convex hull polygon of the second grid and masking points that are not contained in this polygon """
    hull = ConvexHull(np.transpose([lon.ravel(), lat.ravel()]))
    poly = shp.Polygon(hull.points[hull.vertices])
    for i in range(0, np.shape(lon2)[0]):
        for j in range(0, np.shape(lon2)[1]):
            if poly.contains(shp.Point(lon2[i, j], lat2[i, j])) == False:
                # var.mask[i,j]=True
                var[i, j] = np.ma.masked
    return var


@jit(nopython=True)
def sliding_diff(wspdcop_nan, diffs):
    for (i, j, window) in sliding_outlier_detection(wspdcop_nan, 1, ni=1, nj=1): #ni et nj =3 ? 4 ? ni = nj
        center = window[1, 1]
        window_nocenter = window.copy()
        # window_nocenter.mask[np.array([1,0,0,2,2]),np.array([1,0,2,0,2])]=True
        window_nocenter[1, 1] = np.nan
        ravel_window = np.ravel(window)
        if window.shape != () and len(ravel_window[np.isnan(ravel_window)]) < len(ravel_window):
            diffs[i, j] = np.abs(center - np.nanmean(window_nocenter))

    return diffs


@timing
def mask_outlier_points(lon, lat, windspd, variables):
    """ This function processes the SAR wind field to mask isolated outliers and subswath column signatures. 
    Inputs : lon=longitude (2D MaskedArray),lat=latitude (2D MaskedArray),windspd=Wind Speed (2D MaskedArray),variables=list of additional 2D fields to be processed 
    Outputs : vars_out=list of masked variables,i.e. [windspd+additional variables], wspdcop.mask=computed mask """

    wspdcop = windspd.copy()
    grad = np.sqrt((np.gradient(wspdcop)[0] ** 2 + np.gradient(wspdcop)[1] ** 2))
    wspdcop.mask = grad.mask

    # slide a 3x3 window through the image, computing the difference between each pixel and its closest neighbours (diffs) #
    diffs_nan = np.zeros(np.shape(wspdcop))
    diffs_nan[wspdcop.mask] = np.nan
    wspdcop_nan = np.asarray(wspdcop.copy())
    wspdcop_nan[wspdcop.mask] = np.nan
    diffs_nan = sliding_diff(wspdcop_nan, diffs_nan)
    diffs = np.ma.array(diffs_nan)
    diffs.mask = np.isnan(diffs_nan)

    # mask outliers of the distribution of differences, with a large threshold value of 50 for Median Absolute Deviation #
    diffs_msk = mask_outliers(diffs, m=50)# m=100 ? 200 ?
    wspdcop.mask = diffs_msk.mask

    # compute median values by column, and then filter columns based on the value of line-wise difference #
    mean_col = np.ma.mean(wspdcop, axis=0)
    median_col = np.ma.median(wspdcop, axis=0)
    #    diff_col = np.ma.diff(wspdcop,axis=1)
    #    diff_col = np.column_stack((diff_col,diff_col[:,-1]))
    #    diff_col.mask = wspdcop.mask
    #    columns_out = tonan_outliers(np.ma.mean(diff_col,axis=0),m=10)
    #    columns_out_mean = tonan_outliers(np.diff(mean_col),m=10)
    #    columns_out_median = tonan_outliers(np.diff(median_col),m=10)
    columns_out_mean_median = tonan_outliers(np.diff(median_col + mean_col), m=10)

    # mask the data #

    msk_test_2 = wspdcop.mask.copy() * 0
    msk_test_2[:, np.where(np.isnan(columns_out_mean_median))] = True
    wspdcop.mask[:, np.where(np.isnan(columns_out_mean_median))] = True
    vars_out = [wspdcop]
    for var in variables:
        varcop = var.copy()
        varcop.mask = wspdcop.mask
        vars_out.append(varcop)

    return vars_out, wspdcop.mask


@timing
def mask_hetero(lon, lat, windspd, variables, FB, lon_eye, lat_eye, rad):
    """This function  applies a modified heterogeneity mask to the wind field, removing the mask arounf the center raw first guess
    Inputs : lon=longitude, lat=latitude, windspd=wind speed, variables=list of other variables to be masked,\
    FB=Binary Heterogeneity Filter, lon_eye=center longitude first guess, lat_eye=center latitude first guess, \
    rad=minimum radius at which the mask is applied
    Outputs : vars_out=list of variables (first variable=wind speed) with modified mask applied, wspdcop.mask=modified mask"""

    # compute indices close to the eye first guess #
    i_near_eye, j_near_eye = np.where(haversine(lon, lat, lon_eye, lat_eye) <= rad)
    wspdcop = windspd.copy()
    msk = wspdcop.mask
    msk_hetero = FB.copy()

    # remove mask near the eye #
    msk_hetero[i_near_eye, j_near_eye] = 0
    msk[msk_hetero == 1] = 1

    # apply the mask #

    wspdcop.mask = msk
    vars_out = [wspdcop]
    for var in variables:
        varcop = var.copy()
        varcop.mask = wspdcop.mask
        vars_out.append(varcop)

    return vars_out, wspdcop.mask


@timing
def track_eye_sar(lon, lat, lon_eye_fg, lat_eye_fg, spd, rad=300., atcf_rmw=None):
    """ This procedure tracks the eye position applying a recursive method based on the research of a stable low wind area \
    close to the maximum wind area on the wind field.
    Inputs : lon=longitude, lat=latitude, lon_eye_fg=first guess of center longitude, lat_eye_fg=f.g. of center latitude, \
    spd=Wind Speed 2D field, rad=distance reference for reserach radii (km), default=300km"""

    dist_search_1 = rad

    var = spd

    # extract indices close to the eye first guess (r<rad) #
    i_close, j_close = np.where((haversine(lon, lat, lon_eye_fg, lat_eye_fg) <= dist_search_1))

    # set highest values to NaN (arbitrarily, the 200 highest values, to avoid errors in maximum area location #
    var_no_outl = var.copy()
    var_no_outl[var_no_outl.mask == True] = np.nan
    close = var_no_outl[i_close, j_close]

    if close.shape[0] == 0:
        logger.error("There is no data close the eye first guess. Eye center is probably out of the image. Exiting"
                     " with status 60.")
        return None, None, None, None, None, None, None, None, None, None, None, None, 60

    var_no_outl[
        np.where(var >= np.nanquantile(var_no_outl[i_close, j_close], 1. - 200. / np.size(var_no_outl)))] = np.nan

    # extract high wind area in the research radius, as the locations where the wind is above the 90% quantile of #
    # the wind inside this radius #

    i_hwind, j_hwind = np.ma.where((var_no_outl >= np.nanquantile(
        var_no_outl[haversine(lon, lat, lon_eye_fg, lat_eye_fg) <= dist_search_1], 0.9)) & \
                                   (haversine(lon, lat, lon_eye_fg, lat_eye_fg) <= dist_search_1))

    if i_hwind.shape[0] == 0:
        logger.error(f"Valid data could not be found in the high wind research area radius. "
                     f"Research radius is {dist_search_1}"
                     " Exiting"
                     " with status 60.")
        return None, None, None, None, None, None, None, None, None, None, None, None, 60
    # define maximum wind position as the barycenter of these high wind points #
    # Here we compute the center of the high wind points. The high wind points, in case
    # of a circular shaped cyclone, form a circle, and the barycenter is the center of that circle.
    # The barycenter seems to be calculated by meaning the high wind points indexes.
    i_eye_h = int(np.floor(np.ma.mean(i_hwind)))
    j_eye_h = int(np.floor(np.ma.mean(j_hwind)))
    lon_eye_hwnd = lon[i_eye_h, j_eye_h]
    lat_eye_hwnd = lat[i_eye_h, j_eye_h]

    # define a new and smaller research radius around the maximum wind barycenter #

    dist_search_2 = max(rad / 2, 1.5 * haversine(lon_eye_hwnd, lat_eye_hwnd, lon_eye_fg, lat_eye_fg))

    # run a research procedure for a low wind area inside this new radius #
    # The barycenter of the high wind points is given to the research procedure

    lon_eye, lat_eye, i_lwind, j_lwind, n_iter, lons_eye, lats_eye = proc_research_eye(lon, lat, spd, lon_eye_hwnd,
                                                                                       lat_eye_hwnd, dist_search_2)
    i_iter = 0

    locs_eye_proc = [[lon_eye, lat_eye, lons_eye, lats_eye, dist_search_2]]

    status = 0

    # control on the eye position : if the found barycenter is too far from the high wind area or the eye first guess, \
    # the procedure is reproduced with a smaller research radius (0.8*rad). This step is iterated up to ten times until\
    # a center is found close enough. If no point is found, the position kept is the first guess, i.e. the procedure failed.
    while (min(haversine(lon_eye, lat_eye, lon_eye_hwnd, lat_eye_hwnd),
               haversine(lon_eye, lat_eye, lon_eye_fg, lat_eye_fg)) > rad / 2):
        i_iter += 1
        if i_iter > 10:
            logger.warning("Failed to find an eye center : reached maximum number of iteration.")
            status = 20
            lon_eye, lat_eye = lon_eye_fg, lat_eye_fg
            break
        dist_search_2 = dist_search_2 * 0.8
        lon_eye, lat_eye, i_lwind, j_lwind, n_iter, lons_eye_sch, lats_eye_sch = proc_research_eye(lon, lat, spd,
                                                                                                   lon_eye_hwnd,
                                                                                                   lat_eye_hwnd,
                                                                                                   dist_search_2)
        locs_eye_proc.append([lon_eye, lat_eye, lons_eye_sch, lats_eye_sch, dist_search_2])
    msk_hwind = spd * 0
    msk_lwind = spd * 0
    msk_hwind[np.ma.where(haversine(lon, lat, lon_eye_fg, lat_eye_fg) <= dist_search_1)] = 1
    msk_lwind[np.ma.where(haversine(lon, lat, lon_eye_hwnd, lat_eye_hwnd) <= dist_search_2)] = 1

    if atcf_rmw == "" or atcf_rmw is None:
        atcf_rmw = 50

    # if haversine(lon_eye, lat_eye, lon_eye_fg,
    #            lat_eye_fg) > atcf_rmw * 3:  # 10 * haversine(lon_eye_hwnd, lat_eye_hwnd, lon_eye_fg,
    # lat_eye_fg)):  # peut-etre remplacer par >50 pour le moment
    if (haversine(lon_eye, lat_eye, lon_eye_hwnd, lat_eye_hwnd) > 10 * haversine(lon_eye_hwnd, lat_eye_hwnd, lon_eye_fg,
                                                                                 lat_eye_fg)):
        logger.warning("Failed to find an eye center : the computed center is too far from the high wind speed field.")
        status = 30

    return lon_eye, lat_eye, lon_eye_hwnd, lat_eye_hwnd, msk_hwind, dist_search_1, msk_lwind, dist_search_2,\
           i_hwind, j_hwind, i_lwind, j_lwind, status


@timing
def center_eye(lon, lat, lon_eye, lat_eye, windspd, method_shape_eye=1):
    """ This function allows to fix the TC eye location in the middle of the low wind area, so that all azimuthal signals estimated
    after polar projection are centered. For example, a circular eye should yield a constant value of radius, thus the eye should be the 
    center of the circle.
    Firstly, the eye shape is estimated after a first polar projection around the first estimation of the eye position. Then, the eye 
    location is estimated as the centroid of this shape, or as its "pole of inaccesibility" if centroid is outside the shape. The 
    pole of inaccessibility is computed using the polylabel function from https://github.com/mapbox/polylabel
    Inputs : lon,lat=longitude,latitude; lon_eye,lat_eye=original eye position; windspd=2D winf field; method_eye_shape=choice of
    eye shaping algorithm (cf. shape_eye() function)
    Outputs : lon_eye_new,lat_eye_new=new eye position after re-centering; r_vect,theta_vect = radius and azimuth vectors used for
    polar grid definition; rads_shp_flt=radius (degrees) of the computed eye shape"""

    # remove the 300 highest values to avoid wrong estimations of eye shape #

    windspd_flt = rmv_npts(windspd, 300, 'high')

    # create an arbitrary polar grid and project the wind field on it #

    r_vect = np.linspace(0, 3, 301, endpoint=True)
    theta_vect = np.linspace(0, 360, 361, endpoint=True)
    lon_pol = []
    lat_pol = []
    for r in r_vect:
        thetar = theta_vect * np.pi / 180.
        lon_pol.append(lon_eye + r * np.cos(thetar))
        lat_pol.append(lat_eye + r * np.sin(thetar))
    lon_pol = np.array(lon_pol)
    lat_pol = np.array(lat_pol)
    windspd_pol = interp.griddata((lon.ravel(), lat.ravel()), windspd_flt.ravel(), (lon_pol, lat_pol), 'nearest')
    windspd_pol = np.ma.asarray(windspd_pol)
    # mask points outside the SAR field from the polar grid #
    windspd_pol = rmv_outside_field(lon, lat, lon_pol, lat_pol, windspd_pol)
    windspd_pol.mask[np.isnan(windspd_pol)] = True

    # compute the eye shape azimuthal distribution #

    rads_shp = shape_eye(r_vect, theta_vect, windspd_pol, method=method_shape_eye)
    rads_shp[rads_shp.mask == True] = np.nan

    # smooth the eye shape with a Butterworth filter
    rads_shp_flt = smth_sig_expvar_pct(rads_shp, expvar_frac=0.4, per_bool=True)

    # compute longitudes and latitudes of the eye shape distribution #
    lons_shp = [lon_eye + rads_shp_flt[i] * np.cos(theta_vect[i] * np.pi / 180) for i in range(0, len(theta_vect))]
    lats_shp = [lat_eye + rads_shp_flt[i] * np.sin(theta_vect[i] * np.pi / 180) for i in range(0, len(theta_vect))]

    # redefine the center as the centroid of the eye shape polygon #
    poly_shp = shp.Polygon(np.column_stack([lons_shp, lats_shp]))
    lon_eye_new = poly_shp.centroid.x
    lat_eye_new = poly_shp.centroid.y

    # if centered position is outside the eye shape (i.e. distorted eye shape), define the re-centered eye as the pole of inaccessibility
    if not poly_shp.contains(poly_shp.centroid):
        lon_eye_new = polylabel(poly_shp).x
        lat_eye_new = polylabel(poly_shp).y

    return lon_eye_new, lat_eye_new, r_vect, theta_vect, rads_shp_flt


@timing
def make_cmap(color_names):
    R = []
    G = []
    B = []
    for col in color_names:
        R.append(mpl.colors.to_rgba(col)[0])
        G.append(mpl.colors.to_rgba(col)[1])
        B.append(mpl.colors.to_rgba(col)[2])
    dict_colors = {'red': R, 'green': G, 'blue': B}
    c = colormap.Colormap()
    cmap = c.cmap(dict_colors)
    return cmap


class CenterStatus(Enum):
    INSIDE = "inside"
    OUTSIDE = "outside"
    LAND = "land"


def land_mask():
    """
    Get polygon from cartopy containing worldwide land.

    Returns
    -------
    tuple
        A shapely prepared geometry of the polygone and the 'raw' polygon.

    """
    land_10m = cartopy.feature.NaturalEarthFeature('physical', 'land', '10m')
    land_polygons = list(land_10m.geometries())
    land_geom = unary_union(land_polygons)
    # land_polygons_prep = [prep(land_polygon) for land_polygon in land_polygons]

    land = prep(land_geom)

    return land, land_geom


def distance_to_polyg(point, polyg, neg_if_inside=True):
    """
    Return smallest distance in meters between a shapely point and a polygon.

    Parameters
    ----------
    point shapely.geometry.Point
    polyg shapely.geometry.Polygon
    neg_if_inside boolean: If True, returns negative value if the point is inside the polygone. If false, the
                            distance will only be positive.

    Returns
    -------
    float
        Distance in meters
    """
    # nearest points will return 0 if you search the nearest point between a point and a polygon, the point given being
    # inside the polygon. To prevent that, you can convert the Polygon to a linestring using polygon.boundary.
    pts = nearest_points(point, polyg)
    geod = Geod(ellps='WGS84')
    fas, baz, distance = geod.inv(point.x, point.y, pts[1].x, pts[1].y)

    if neg_if_inside:
        if polyg.contains(point):
            distance = -distance

    return distance


def center_status(sat_bb, atcf_pt):
    """
    Check if atcf point is inside or outisde satellite bounding box.

    Parameters
    ----------
    polyg shapely.geometry.Polygon: Satellite bounding box
    point shapely.geometry.Point: atcf point

    Returns
    -------
    list
        list containing CenterStatus flags
    """
    status = []
    if sat_bb.contains(atcf_pt):
        status.append(CenterStatus.INSIDE)
    else:
        status.append(CenterStatus.OUTSIDE)

    return status


def proportion_valid_data_around_center(sat_bb, atcf_pt, land):
    """
    Compute 3 percentages that help to know if the eye center can be found.
    % of area of 100km around atcf track is outside acquisition bounding box
    % of area of 100km around atcf track point that is inside acquisition bounding box is land.
    % of area of 100km around track point is not usable (land or outside of acquisition).

    Parameters
    ----------
    sat_bb: shapely.geometry.Polygon satellite bounding box
    atcf_pt: shapely.geometry.Point atcf point
    land: shapely.geometry.Polygon polygon of land (coastline)

    Returns
    -------
    tuple
        Contains 3 percentages (floats)
            % of area of 100km around atcf track is outside acquisition bounding box
            % of area of 100km around atcf track point that is inside acquisition bounding box is land.
            % of area of 100km around track point is not usable (land or outside of acquisition).

    """
    radius = 100000
    local_azimuthal_projection = f"+proj=aeqd +R=6371000 +units=m +lat_0={atcf_pt.y} +lon_0={atcf_pt.x}"
    wgs84_to_aeqd = partial(
        pyproj.transform,
        pyproj.Proj("+proj=longlat +datum=WGS84 +no_defs"),
        pyproj.Proj(local_azimuthal_projection),
    )
    aeqd_to_wgs84 = partial(
        pyproj.transform,
        pyproj.Proj(local_azimuthal_projection),
        pyproj.Proj("+proj=longlat +datum=WGS84 +no_defs"),
    )
    point_transformed = transform(wgs84_to_aeqd, atcf_pt)
    buffer = point_transformed.buffer(radius)
    circle_poly = transform(aeqd_to_wgs84, buffer)

    bb_intersection = circle_poly.intersection(sat_bb)
    percent_bb_intersection = 100 - (bb_intersection.area / circle_poly.area) * 100
    logger.info(f"{percent_bb_intersection}% of area of 100km around atcf track is outside acquisition bounding box")

    intersect_land = bb_intersection.intersection(land)
    percent_intersect_land = (intersect_land.area / circle_poly.area) * 100
    logger.info(f"{percent_intersect_land}% of area of 100km around atcf track point that is inside acquisition "
                f"bounding box is land.")

    percent_sum_non_usable = percent_intersect_land + percent_bb_intersection
    logger.info(f"{percent_sum_non_usable}% of area of 100km around track point is not usable (land or"
                f" outside of acquisition).")

    return percent_bb_intersection, percent_intersect_land, percent_sum_non_usable


def research_contour(mask, lon, lat):
    """
        Return lon and lat coordinates of the circular shape contour contained in mask. The three input arrays
        must be of the same size.

        Parameters
        ----------
        mask: numpy.ndarray 2D array containing a shape
        lon: numpy.ndarray 2D array containing lon coordinates
        lat: numpy.ndarry 2D array containing lat coordinates

        Returns
        -------
        tuple
            Tuple containing coordinates of shape contour. Longitude coordinates at first index and latitude coordinates
            at second index.
    """
    idx0, idx1 = contour_indices(mask, lon)
    lon_lwind = lon[idx0, idx1]
    lat_lwind = lat[idx0, idx1]

    return lon_lwind, lat_lwind


def circle_poly_from_rad(lon, lat, radius):
    """
    Generates a shapely polygon (circle) from a lon lat position and a radius in meters.

    Parameters
    ==========
    lon: float longitude position of circle center
    lat: float latitude position of circle center
    radius: int circle radius in meters

    Returns
    =======
    shapely.Polygon
        the circle polygon


    """
    circle_points = cgeo.Geodesic().circle(lon=lon, lat=lat, radius=radius)
    geom = shapely.geometry.Polygon(circle_points)
    return geom


def shape_from_array(mask, lon_array, lat_array):
    lon, lat = research_contour(mask, lon_array, lat_array)
    lwind_contour_coords = []
    for c_i in range(0, len(lon)):
        lwind_contour_coords.append((lon[c_i], lat[c_i]))

    contour = LineString(lwind_contour_coords)

    return contour


# Converts wind speed (int) (knots or m/s) to cyclone category
def spd2cat(spd, unit="knots"):
    """
    Converts wind speed (int) in m/s into cyclone category

    Parameters
    ----------
    spd : int Wind speed in meters per second.

    Returns
    -------
    str
        The cyclone category (dep, storm, cat-X)
    """
    # Dict to convert from string categories to their corresponding maximal wind_speed
    # See also function spd2cat
    maxspd = collections.OrderedDict([
        ("dep", 34),
        ("storm", 64),
        ("cat-1", 82),
        ("cat-2", 95),
        ("cat-3", 112),
        ("cat-4", 136),
        ("cat-5", 999)])

    # Convert to knot
    if unit == "m/s":
        spd = spd * 1.94384
    return list(maxspd.keys())[[i for i, m in enumerate(maxspd) if maxspd[m] - spd > 0][0]]
