import math
import sys
import logging
from typing import Optional, List

import numpy as np
from shapely import wkt

from tc_center.functions import open_SAR_image, mask_outlier_points, mask_hetero, track_eye_sar, center_eye, make_cmap, \
center_status, proportion_valid_data_around_center, land_mask, distance_to_polyg, api_req, CenterStatus 
from tc_center.analysis_product import center_products_analysis
from tc_center.fix import save_fix_analysis

import os
import os.path
import pandas as pd
import shapely.wkt
import owi

from cyclobs_utils import cyclone_speed_dir_angle_to_north

logger = logging.getLogger(__name__)


def compute_centers(sat_file: str, sat_file_format: str, resolution: int, first_guess_location_point: str,
                    rmw: float,
                    rad34: Optional[List[float]] = None):
    """
    Main function that prepares data and searches the cyclone eye center.
    This includes opening the data, removing outliers, applying an heterogeneity mask,
    finding the a high-wind-speed area barycenter, a low-wind-speed area barycenter, then
    based on that low wind speed area, finding the eye shape and finally the eye_center as the centroid of that
    eye shape.

    Parameters
    ----------
    sat_file: str The SAR acquisition file path (L2C or sw format) to process.
    sat_file_format: str Specify the given sat_file format (can be : l2c, sw)
    resolution: int The resolution to use to find the eye center. (can be 1, 3, 5, 10, 12, 25, 40, 50)
    first_guess_location_point: str WKT Point colocated to the acquisition. Used as first_guess
    rad34: list of RAD34 radius. Optional. If not given, rmw will be used (less precise).
    rmw: float Radius of max wind


    Returns
    -------
    dict
        A python dict containing various information about the found center and intermediate data used in the processing.

    """
    cm_cyc = make_cmap(['k', 'darkslategrey', 'teal', 'darkturquoise',
                        'turquoise', 'paleturquoise', 'lightcyan', 'azure', 'ivory', 'gold', 'darkorange', 'orangered',
                        'firebrick', 'm', 'magenta'])
    eye_ok = True

    # Getting the _ll_gd version of the acquisition.
    if sat_file_format == "l2c":
        if resolution == 1:
            sat_file = owi.L2CtoLight(sat_file)
        else:
            sat_file = owi.L2CtoLight(sat_file, resolution=resolution)

    lon, lat, windspd, nrcs, FB, footprint_wkt = open_SAR_image(sat_file)

    ## 2. mask outliers (subswaths, and isolated outliers) ##
    ## Supposed to exclude rain signatures from the process. To do that, a gradient and a sliding
    # windows to compute diff with neighbours of each pixel is used.
    [windspd, nrcs], mask_flt = mask_outlier_points(lon, lat, windspd, [nrcs])

    if float(len(np.where(windspd.mask)[0])) / np.size(windspd) > 0.66:
        logger.error("More than 2/3 of the image is on land. Aborting")
        return None, 12, False

    sar_maxwind_kts = np.nanmax(windspd)
    if sar_maxwind_kts * 1.944 < 20.:
        logger.error("Max. wind under 20 knots. Aborting eye research")
        return None, 105, False

    # Create shapely Point using the cyclone track point colocated to that SAR acquisition.
    atcf_point = shapely.wkt.loads(first_guess_location_point)
    # Shapely polygon of SAR acquisition bounding box
    sat_bb = shapely.wkt.loads(footprint_wkt)
    atcf_lon = atcf_point.x
    atcf_lat = atcf_point.y
    logger.info(f"Atcf lon/lat {atcf_lon}/{atcf_lat}")
    atcf_rmw = rmw

    if atcf_rmw == "" or atcf_rmw is None:
        logger.error("Given rmw is empty.")
        return None, 110, False

    # Computing extra info useful to estimate if it probable to find an eye center into that acquisition.
    land, land_non_prep = land_mask()
    center_st = center_status(sat_bb, atcf_point)
    # Get smallest distance between atcf point and nearest coast.
    dist_to_coast = distance_to_polyg(atcf_point, land_non_prep)
    # Get smallest distance between atcf point and acquisition bounding box.
    dist_to_acq = distance_to_polyg(atcf_point, sat_bb.boundary, neg_if_inside=False)
    logger.info(f"Distance to coast of track point is {dist_to_coast} meters.")
    logger.info(f"Distance between aquisition border and track point is {dist_to_acq} meters.")
    percent_outside, percent_inside_island, percent_non_usable = \
        proportion_valid_data_around_center(sat_bb, atcf_point, land_non_prep)

    if len(rad34) < 2:
        logger.warning("None or only one rad is valid. Using atcf RMW to define research radius instead.")
        r34 = 0
    else:
        r34 = np.mean(rad34)

    logger.info(f"SAR image has {windspd.count()} non-masked points.")
    # If windspeed contains only nans or not enough data return error
    if len(np.where(windspd.mask == False)[0]) == 0 or windspd.count() < 3000:
        logger.error(f"SAR image contains too few points: {windspd.count()}. Aborting.")
        return None, 10, False

    # windspd_lr.mask = mask_flt

    # Use atcf track point position as first guess
    lon_eye_fg = atcf_lon
    lat_eye_fg = atcf_lat

    # first guess of TC center is computed on the low-resolved wind field (25km resolution), as the location of the value of maximum wind
    # lon_eye_fg, lat_eye_fg = np.median(lon_lr[windspd_lr > np.quantile(windspd_lr, 0.99)]), np.median(
    #    lat_lr[windspd_lr > np.quantile(windspd_lr, 0.99)])

    ## 3. Apply heterogeneity and remove it near the eye ##
    # This will simply find indices around the first guess eye and remove the previously computed mask in this area.
    [windspd_hetero_msk], mask_h = mask_hetero(lon, lat, windspd, [], FB, lon_eye_fg, lat_eye_fg,
                                                              max(4 * atcf_rmw, r34))

    ## 4. Track the eye position ##
    lon_eye_old, lat_eye_old, lon_eye_hwnd, lat_eye_hwnd, msk_hwind, dist_search_hwind, msk_lwind, dist_search_lwind, \
    i_hwind, j_hwind, i_lwind, j_lwind, \
    status = track_eye_sar(lon, lat, lon_eye_fg, lat_eye_fg, windspd_hetero_msk, rad=max(2 * atcf_rmw, r34 / 2),
                           atcf_rmw=atcf_rmw)

    if status == 60:
        return None, status, False

    ## 5. Reapply heterogeneity mask outside the eye ##

    [windspd, nrcs], mask_h = mask_hetero(lon, lat, windspd, [nrcs], FB, lon_eye_old, lat_eye_old,
                                              max(2 * atcf_rmw, r34 / 2))

    lons_eye_shape = []
    lats_eye_shape = []
    lon_eye = lon_eye_old
    lat_eye = lat_eye_old
    if status == 0:
        ## 6. re-center the eye position and retrieve eye shape estimation ##
        lon_eye, lat_eye, rad, azim, rads_shp = center_eye(lon, lat, lon_eye_old, lat_eye_old, windspd,
                                                           method_shape_eye=1)
        # fact_eye_shp can be parametered in order to extend the eye shape #
        fact_eye_shp = 1.
        rads_shp_expanded = rads_shp * fact_eye_shp
        lons_eye_shape = [lon_eye_old + rads_shp_expanded[i] * np.cos(azim[i] * np.pi / 180) for i in
                          range(0, len(azim))]
        lats_eye_shape = [lat_eye_old + rads_shp_expanded[i] * np.sin(azim[i] * np.pi / 180) for i in
                          range(0, len(azim))]

    # Some cases where the recentered eye is outside the bounding box. There is a quality flag measuring that.
    if status == 0 and center_status(sat_bb, shapely.geometry.Point(lon_eye, lat_eye))[0] == CenterStatus.OUTSIDE:
        eye_ok = False
        logger.warning("Re-center procedure found the eye out of the image.")

    windspd_eye_masked = windspd.copy()

    if (float(len(np.where(windspd.mask)[0])) / np.size(windspd) > 0.33) & (status == 0):
        logger.warning("Procedure OK but many points are masked... TC might be on land")

    return {"center_status": center_st, "percent_outside": percent_outside,
            "percent_inside_island": percent_inside_island, "percent_non_usable": percent_non_usable,
            "dist_to_coast": dist_to_coast, "dist_to_acq": dist_to_acq,
            "lon_array": lon, "lat_array": lat, "windspd_eye_masked": windspd_eye_masked, "cm_cyc": cm_cyc,
            "lons_eye_shape": lons_eye_shape, "lats_eye_shape": lats_eye_shape, "lon_center_final": lon_eye,
            "lat_center_final": lat_eye, "lon_eye_lwind": lon_eye_old, "lat_eye_lwind": lat_eye_old,
            "lon_eye_fg": lon_eye_fg, "lat_eye_fg": lat_eye_fg, "lon_eye_hwnd": lon_eye_hwnd,
            "lat_eye_hwnd": lat_eye_hwnd, "msk_hwind": msk_hwind, "msk_lwind": msk_lwind,
            "i_hwind": i_hwind, "j_hwind": j_hwind, "i_lwind": i_lwind, "j_lwind": j_lwind,
            "bounding_box": sat_bb, "dist_search_hwind": dist_search_hwind, "dist_search_lwind":
                dist_search_lwind}, status, eye_ok


def find_center_save(path, sat_file=None, resolution=3, path_fix=None, netcdf_path=None, api_data=None,
                     api_ibtracs_data=None,
                     api_tracks_data=None,
                     api_url="https://cyclobs.ifremer.fr",
                     no_netcdf=False, no_fix=False, gen_err=False, sat_file_sw=None, sat_file_ll_gd=None, debug=False,
                     file_logging=True,
                     cyclone_direction=None,
                     cyclone_direction_std=None,
                     cyclone_speed=None,
                     cyclone_speed_std=None,
                     ):
    """
    Function that calls the eye center research procedure and that pass the result to save functions (for .dat and .nc
    files generation).

    Parameters
    ----------
    path: str Base path to which resulting data will be saved.
    sat_file: str Path of satellite file to process (L2C format, but converted to _sw and _ll_gd file path with pathUrl).
                  if usinf on files that are not generated using SARWING chain, and thus with different directory tree,
                  use sat_file_sw and sat_file_ll_gd instead.
    resolution: int Resolution to use to search for the eye center (can be 1, 3, 5, 10, 12, 25, 40, 50). Defaults to 3. Optional
    path_fix: str Path to which the fix .dat files will be saved. Optional, if not given, path_fix=path
    netcdf_path: str Path to which the .nc files will be saved. Optional, if not given, netcdf_path=path
    api_url: str Base API URL. Defaults to https://cyclobs.ifremer.fr. Optional.
    api_data: pandas.DataFrame Optional. Cyclobs API data result for the given sat_file. If not given, the function will
              itself call the API.
    no_netcdf: bool Enable (False) or disable (True) netcdf file generation.
    no_fix: bool Enable (False) or disable (True) FIX file generation.
    sat_file_sw: str Complete path to _sw file (can be used in place of sat_file arg). sat_file_ll_gd must also be given
    sat_file_ll_gd: str Complete path to _ll_gd file (can be used in place of sat_file arg). sat_file_sw must also be given
    debug: bool True : activate DEBUG logging, else it uses INFO.
    file_logging: bool If True, configures the logger to log data to file.

    Returns
    -------
    int
        Status code of the processing. Status are :
        100 = Cyclobs API could not be reached.
        10 = SAR acquisition contains too few valid pixels.
        12 = More than 2/3 of the image are on land.
        20 = Maximum number of iteration was reached when searching eye center.
        30 = The found eye center is too far from the high wind speed area barycenter
        60 = There is no data close the eye first guess. Not enough data to compute eye center.
        105 = Max. wind under 20 knots. Aborting eye research
        110 = Atcf rmw is empty.
        1 = Unmanaged error
        0 = OK

    """
    if netcdf_path is None:
        netcdf_path = path

    if path_fix is None:
        path_fix = path

    if sat_file is None and (sat_file_sw is None or sat_file_ll_gd is None):
        raise ValueError("sat_file, sat_file_sw and sat_file_ll_gd are invalid. Give at least sat_file or"
                         "both sat_file_sw and sat_file_ll_gd.")

    if sat_file:
        sat_file_for_api = "-".join(os.path.basename(sat_file).split("-")[4:6])
        sat_file_format = "l2c"
        sat_file_input = sat_file
    else:
        sat_file_for_api = "-".join(os.path.basename(sat_file_sw).split("-")[4:6])
        sat_file_format = "ll_gd"
        sat_file_input = sat_file_ll_gd

    # Remove previously defined loggers
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)

    if debug:
        level = logging.DEBUG
    else:
        level = logging.INFO
    if file_logging:
        log_path = os.path.join(path, "logs")
        os.makedirs(log_path, exist_ok=True)
        log_file = os.path.join(log_path, f"{os.path.basename(sat_file_input)}.log")

        # Config Logger
        logging.basicConfig(level=level, format="%(asctime)s [%(levelname)s] %(message)s",
                            handlers=[logging.FileHandler(log_file), logging.StreamHandler()])

        logger = logging.getLogger(os.path.basename(__file__))
    else:
        # Config Logger
        logging.basicConfig(level=level, format="%(asctime)s [%(levelname)s] %(message)s")
        logger = logging.getLogger(os.path.basename(__file__))

    try:
        if api_data is None:
            api_data = api_req(sat_file_for_api, api_url, partial=True)
        
        if api_ibtracs_data is None:
            api_r = f"{api_url}/app/api/getData?include_cols=all&filename={sat_file_for_api}&track_source=ibtracs"
            logger.info(f"Getting IBTRACS data from Cyclobs API. Req : {str(api_r)}")
            api_ibtracs_data = pd.read_csv(api_r, keep_default_na=False)

        if not sat_file:
            sat_file_input = sat_file_sw
            sat_file_format = "sw"

        track_point = api_data.loc[0]["track_point"]
        if track_point is None or track_point == "":
            raise ValueError("track_point value from API is empty.")

        rmw = api_data.loc[0]["rmw"]
        if rmw is None or rmw == "":
            raise ValueError("rmw value from API is empty.")

        # Names of atcf rad columns from CyclObs API
        atcf_rads_names = ["rad34_seq", "rad34_neq", "rad34_swq", "rad34_nwq"]
        atcf_rads = []
        for name in atcf_rads_names:
            if not api_data.iloc[0][name] is None and api_data.iloc[0][name] != "":
                atcf_rads.append(float(api_data.iloc[0][name]))

        eye_dict, status, eye_ok = compute_centers(sat_file_input, sat_file_format, resolution, track_point, rmw,
                                                   rad34=atcf_rads)

        if len(api_ibtracs_data.index) > 0:
            ibtracs_point = api_ibtracs_data["track_point"].iloc[0]
            ibtracs_vmax = api_ibtracs_data.iloc[0]["vmax (m/s)"]
            ibtracs_rmax = api_ibtracs_data.iloc[0]["rmw"]
        else:
            ibtracs_point = None
            ibtracs_vmax = None
            ibtracs_rmax = None

        sat_date = owi.getDateFromFname(os.path.basename(sat_file))

        if not cyclone_direction or not cyclone_direction_std or not cyclone_speed or not cyclone_speed_std:
            logger.info(f"Cyclone speed and directions not provided, will query track API of {api_url}")
            # Compute cyclone direction relative to north and cyclone propagation speed.
            direction, dir_std, speed, speed_std = cyclone_speed_dir_angle_to_north(api_data.iloc[0]["sid"], sat_date,
                                                                                    base_url=api_url + "/app")
        else:
            direction = cyclone_direction
            dir_std = cyclone_direction_std
            speed = cyclone_speed
            speed_std = cyclone_speed_std

        if status != 0:
            logger.error(f"Failed to find correct tc center ! Status {status}")
            if eye_dict is not None and no_netcdf is not True and gen_err:
                # Even if the research failed, if we have usable data we build the product in order to help
                # debugging.
                #
                if not sat_file:
                    sat_file_input = sat_file_sw
                    sat_file_format = "sw"
                center_products_analysis(sat_file=sat_file_input,
                                         sat_file_format=sat_file_format,
                                         path=netcdf_path,
                                         status=status,
                                         storm_name=api_data.iloc[0]["cyclone_name"],
                                         track_source_name="atcf",
                                         atcf_point=api_data["track_point"].iloc[0],
                                         atcf_vmax=api_data.iloc[0]["vmax (m/s)"],
                                         atcf_rmw=api_data.iloc[0]["rmw"],
                                         sid=api_data.iloc[0]["sid"],
                                         atcf_filename=os.path.basename(api_data.iloc[0]["track_file"]),
                                         resolution=resolution,
                                         eye_ok=eye_ok,
                                         rads34=atcf_rads, other_track_point=ibtracs_point,
                                         other_track_vmax=ibtracs_vmax, other_track_rmw=ibtracs_rmax,
                                         other_track_source_name="ibtracs",
                                         cyclone_direction=direction,
                                         cyclone_direction_std=dir_std, cyclone_speed=speed,
                                         cyclone_speed_std=speed_std, **eye_dict)
            return status
        else:
            logger.info("Starting creation of NetCDF products...")
            if not sat_file:
                sat_file_input = sat_file_sw
                sat_file_format = "sw"
            sar_vmax, sar_rmax, radii_quad, radii_nmi_quad, percent_valid_quad, final_flag = \
                center_products_analysis(sat_file=sat_file_input,
                                         sat_file_format=sat_file_format,
                                         path=netcdf_path,
                                         status=status,
                                         storm_name=api_data.iloc[0]["cyclone_name"],
                                         track_source_name="atcf",
                                         atcf_point=api_data["track_point"].iloc[0],
                                         atcf_vmax=api_data.iloc[0]["vmax (m/s)"],
                                         atcf_rmw=api_data.iloc[0]["rmw"],
                                         sid=api_data.iloc[0]["sid"],
                                         atcf_filename=os.path.basename(api_data.iloc[0]["track_file"]),
                                         resolution=resolution,
                                         eye_ok=eye_ok, rads34=atcf_rads, other_track_point=ibtracs_point,
                                         other_track_vmax=ibtracs_vmax, other_track_rmw=ibtracs_rmax,
                                         other_track_source_name="ibtracs",
                                         cyclone_direction=direction,
                                         cyclone_direction_std=dir_std, cyclone_speed=speed,
                                         cyclone_speed_std=speed_std, **eye_dict)

            if no_fix is False:
                if not sat_file:
                    sat_file_input = sat_file_ll_gd
                save_fix_analysis(path=path_fix,
                                  sat_file=sat_file_input,
                                  atcf_filename=os.path.basename(api_data.iloc[0]["track_file"]),
                                  lon_eye=eye_dict["lon_center_final"],
                                  lat_eye=eye_dict["lat_center_final"],
                                  sar_vmax=sar_vmax,
                                  sar_rmax=sar_rmax,
                                  radii_nmi_quad=radii_nmi_quad,
                                  percent_valid_quad=percent_valid_quad,
                                  sid=api_data.iloc[0]["sid"],
                                  sub_basin=api_data.iloc[0]["sub_basin"],
                                  sat_mission=api_data.iloc[0]["mission"],
                                  acq_time=pd.to_datetime(api_data.iloc[0]["acquisition_start_time"]),
                                  quality_flag=math.trunc(final_flag))

            if status == 0:
                return 0
    except Exception as e:
        logger.exception(f"Exception: {e}")
        return 1


def find_center_save_no_api(path,
                            sat_file_ll_gd,
                            sat_file_sw,
                            cyclone_direction,
                            cyclone_direction_std,
                            cyclone_speed,
                            cyclone_speed_std,
                            cyclone_name,
                            sub_basin,
                            mission,
                            acq_start_date,
                            track_sid,
                            track_vmax,
                            track_file,
                            track_point,
                            track_rmw,
                            track_source_name,
                            track_rads34=None,
                            other_track_point=None,
                            other_track_vmax=None,
                            other_track_rmw=None,
                            other_track_source_name=None,
                            resolution=3, path_fix=None, netcdf_path=None,
                            no_netcdf=False, no_fix=False, gen_err=False, debug=False,
                            file_logging=True,
                     ):
    if netcdf_path is None:
        netcdf_path = path

    if path_fix is None:
        path_fix = path

    sat_file_input = sat_file_ll_gd

    # Remove previously defined loggers
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)

    if debug:
        level = logging.DEBUG
    else:
        level = logging.INFO
    if file_logging:
        log_path = os.path.join(path, "logs")
        os.makedirs(log_path, exist_ok=True)
        log_file = os.path.join(log_path, f"{os.path.basename(sat_file_input)}.log")

        # Config Logger
        logging.basicConfig(level=level, format="%(asctime)s [%(levelname)s] %(message)s",
                            handlers=[logging.FileHandler(log_file), logging.StreamHandler()])

        logger = logging.getLogger(os.path.basename(__file__))
    else:
        # Config Logger
        logging.basicConfig(level=level, format="%(asctime)s [%(levelname)s] %(message)s")
        logger = logging.getLogger(os.path.basename(__file__))

    try:
        sat_file_input = sat_file_sw
        sat_file_format = "sw"

        if track_point is None or track_point == "":
            raise ValueError("track_point value from API is empty.")

        if track_rmw is None or track_rmw == "":
            raise ValueError("rmw value from API is empty.")

        if track_rads34 is None:
            atcf_rads = []
        else:
            atcf_rads = track_rads34

        eye_dict, status, eye_ok = compute_centers(sat_file_input, sat_file_format, resolution, track_point, track_rmw,
                                                   rad34=atcf_rads)

        direction = cyclone_direction
        dir_std = cyclone_direction_std
        speed = cyclone_speed
        speed_std = cyclone_speed_std

        if status != 0:
            logger.error(f"Failed to find correct tc center ! Status {status}")
            if eye_dict is not None and no_netcdf is not True and gen_err:
                # Even if the research failed, if we have usable data we build the product in order to help
                # debugging.
                #
                sat_file_input = sat_file_sw
                sat_file_format = "sw"
                center_products_analysis(sat_file=sat_file_input,
                                         sat_file_format=sat_file_format,
                                         path=netcdf_path,
                                         status=status,
                                         storm_name=cyclone_name,
                                         track_source_name=track_source_name,
                                         atcf_point=track_point,
                                         atcf_vmax=track_vmax,
                                         atcf_rmw=track_rmw,
                                         sid=track_sid,
                                         atcf_filename=os.path.basename(track_file),
                                         resolution=resolution,
                                         eye_ok=eye_ok,
                                         rads34=atcf_rads, other_track_point=other_track_point,
                                         other_track_vmax=other_track_vmax, other_track_rmw=other_track_rmw,
                                         other_track_source_name=other_track_source_name,
                                         cyclone_direction=direction,
                                         cyclone_direction_std=dir_std, cyclone_speed=speed,
                                         cyclone_speed_std=speed_std, **eye_dict)
            return status
        else:
            logger.info("Starting creation of NetCDF products...")
            sat_file_input = sat_file_sw
            sat_file_format = "sw"
            sar_vmax, sar_rmax, radii_quad, radii_nmi_quad, percent_valid_quad, final_flag = \
                center_products_analysis(sat_file=sat_file_input,
                                         sat_file_format=sat_file_format,
                                         path=netcdf_path,
                                         status=status,
                                         storm_name=cyclone_name,
                                         track_source_name=track_source_name,
                                         atcf_point=track_point,
                                         atcf_vmax=track_vmax,
                                         atcf_rmw=track_rmw,
                                         sid=track_sid,
                                         atcf_filename=os.path.basename(track_file),
                                         resolution=resolution,
                                         eye_ok=eye_ok,
                                         rads34=atcf_rads, other_track_point=other_track_point,
                                         other_track_vmax=other_track_vmax, other_track_rmw=other_track_rmw,
                                         other_track_source_name=other_track_source_name,
                                         cyclone_direction=direction,
                                         cyclone_direction_std=dir_std, cyclone_speed=speed,
                                         cyclone_speed_std=speed_std, **eye_dict)

            if no_fix is False:
                sat_file_input = sat_file_ll_gd
                save_fix_analysis(path=path_fix,
                                  sat_file=sat_file_input,
                                  atcf_filename=os.path.basename(track_file),
                                  lon_eye=eye_dict["lon_center_final"],
                                  lat_eye=eye_dict["lat_center_final"],
                                  sar_vmax=sar_vmax,
                                  sar_rmax=sar_rmax,
                                  radii_nmi_quad=radii_nmi_quad,
                                  percent_valid_quad=percent_valid_quad,
                                  sid=track_sid,
                                  sub_basin=sub_basin,
                                  sat_mission=mission,
                                  acq_time=pd.to_datetime(acq_start_date),
                                  quality_flag=math.trunc(final_flag))

            if status == 0:
                return 0
    except Exception as e:
        logger.exception(f"Exception: {e}")
        return 1
