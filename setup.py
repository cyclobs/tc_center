from setuptools import setup
import glob

setup(name='tc_center',
      description='Algorithm to find cyclobs center from SAR satellites image and analysis product generation.',
      url='https://gitlab.ifremer.fr/cyclobs/tc_center.git',
      author="Leo Vinour, Theo Cevaer",
      author_email="leo.vinour@ifremer.fr, Theo.Cevaer@ifremer.fr",
      license='GPL',
      use_scm_version={'write_to': '%s/_version.py' % "tc_center"},
      setup_requires=['setuptools_scm'],
      packages=['tc_center'],
      zip_safe=False,
      scripts=glob.glob('bin/**'),
      #install_requires=[
      #    'shapely', 'numpy', 'netCDF4', "matplotlib", "owi", "numba", "scikit-image", "panel",
      #    "scipy", "shapely", "python-polylabel", "colormap", "bokeh",
      #    "pandas", "holoviews", "xarray", "rioxarray", "flask",
      #    "owi>=0.0.dev0", "geoviews", "easydev", "geopandas", "opencv-python",
      #    "selenium", "rasterio", "bottleneck", 'colormap-ext>=0.0.dev0', 'atcf>=0.0.dev0',
      #    "pathurl>=0.0.dev0", "geo_shapely>=0.0.dev0", "cyclobs_utils>=0.0.dev0"
      #]
      )
