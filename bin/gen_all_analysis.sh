#!/bin/bash

# Generate the fix analysis .dat and .netcdf for all the SAR acquisitions.

# Usage #
# -p for the path under which to save plots (required)
# -r for the path under which to save reports (required)
# -n the number of line to take from the SAR acquisition report taken as input. The acquisitions in that file are
# written from the most recent to the oldest. So by taking the 10 first line, we take the 10 latest acquisition. This
# parameter is optional


GETOPT=$(getopt -n $0 -o a:n:p:r:h:: --long help -- "$@")
eval set -- "$GETOPT"

api_url="https://cyclobs.ifremer.fr"
while true; do
    case "$1" in
        -p) plot_path=$2 ; shift 2 ;;
        -r) report_path=$2 ; shift 2 ;;
        -n) line_number=$2 ; shift 2 ;;
        -a) api_url=$2 ; shift 2 ;;
        -- ) shift ; break ;;
        * ) echo "unknown opt $1" >&2 ; exit 1 ;;
    esac
done

script_dir=$(dirname $(readlink -f "$0")) # ie $CONDA_PREFIX/bin

# Retrieve the current SAR path (the commit can change the path).
proc_dir=$(dirname $(curl -s https://cerweb.ifremer.fr/shoc/shoc_dailyupdate_names.html | grep 'http-equiv="refresh"' | sed -r 's/.*URL=(.*)".*/\1/' | xargs pathUrl.py --schema=dmz -p))

# Retrieve the SAR acquisition file list from the most recent commit.
file_list="$proc_dir/sarwingL2X/ConcatprocessOk.txt"

# If line_number (-n) is not specified, updating all data
if [ -z "$line_number" ]; then
  echo "Computing analysis for ALL SAR acquisitions"
  <"$file_list" xargs -P 10 -I {} gen_analysis_wrapper.sh "$plot_path" {} "$report_path" "$api_url"
else
  echo "Computing analysis for $line_number latest acquisitions"
  first_lines=$(head -n $line_number "$file_list")
  echo "$first_lines" | xargs -P 10 -I {} gen_analysis_wrapper.sh "$plot_path" {} "$report_path" "$api_url"
fi
