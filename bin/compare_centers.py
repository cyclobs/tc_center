#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from tc_center.compare import compare
import owi
import sys
import os

import logging
logger = logging.getLogger(__name__)

if __name__ == "__main__":
    description = """
        Compare tc eye centers from cyclobs with other data source
        """
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("-s", "--data-source", action="store", required=False, default=None,
                        help="Path to folder or data of other source to compare with.")
    parser.add_argument("-p", "--path-save", action="store", required=True,
                        help="The path under which report will be saved")
    parser.add_argument("-n", "--source-name", action="store", required=True, help="Data source name")
    parser.add_argument("-f", "--filter-list", action="store", default=None, required=False,
                        help="List to use to compare on center only written in the given file.")
    parser.add_argument("--debug", action="store_true", default=False,
                        help="Enable debug logging level.")

    args = parser.parse_args()

    compare(args.data_source, args.path_save, args.source_name, args.filter_list)


