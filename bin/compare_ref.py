#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from tc_center.compare.compare import compare_with_ref
import owi
import sys
import os

import logging
logger = logging.getLogger(__name__)

if __name__ == "__main__":
    description = """
        Compare tc eye centers from cyclobs with reference cyclobs dataset
        """
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("-s", "--ref-source", action="store", required=True,
                        help="Path to folder of reference data.")
    parser.add_argument("-c", "--compared-source", action="store", required=True,
                        help="Path to other folder to compare with.")
    parser.add_argument("-p", "--path-save", action="store", required=False, default=None,
                        help="The path under which report will be saved")
    parser.add_argument("-f", "--filter-list", action="store", default=None, required=False,
                        help="List to use to compare on center only written in the given file.")
    parser.add_argument("--debug", action="store_true", default=False,
                        help="Enable debug logging level.")

    args = parser.parse_args()

    compare_with_ref(args.ref_source, args.compared_source, args.path_save, args.filter_list)


