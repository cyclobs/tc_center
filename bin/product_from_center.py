#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import argparse
import logging

if __name__ == "__main__":
    description = """
        Create TC analysis from given cyclone eye center
        4 netcdf are produced : 
        - A gridded product in azimuthal equidistant projection, centered on the given center
        - A gridded product rotated to match cyclone direction (azimuthal equidistant projection centered on this given center)
        - A polar projected product (theta, rad), with theta=0° orientated to the north
        - A polar projected product (theta, rad), with theta=0° orientated following cyclone propagation direction
        
        """
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("-f2x", "--sat_file_l2x", action="store",
                        help="Satellite file (L2X Light SW format). Example "
                             "of filename : rs2--owi-cm-20220112t065152-20220112t065415-00003-78B1E_sw.nc",
                        required=True)
    parser.add_argument("-lonc", "--lon_center", action="store", required=True, help="Cyclone eye center longitude")
    parser.add_argument("-latc", "--lat_center", action="store", required=True, help="Cyclone eye center latitude")
    parser.add_argument("-rot", "--rotation", action="store", type=float, help="Rotation to apply to dataset. If not"
                                                                               " given, the process will try to"
                                                                               "request CyclObs API, to compute it.")
    parser.add_argument("-p", "--path", action="store", required=True,
                        help="The base path under which save the generated files (fix, control plot)")

    parser.add_argument("--debug", action="store_true", default=False,
                        help="Enable debug logging level.")

    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG, format="%(asctime)s [%(levelname)s] %(message)s")
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO, format="%(asctime)s [%(levelname)s] %(message)s")
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.INFO)

    # import packages after logger configuration
    from tc_center.analysis_product import center_products_opt

    sat_file = args.sat_file_l2x
    logger.debug(f"Satellite file : {sat_file}")

    center_products_opt(sat_file, float(args.lon_center), float(args.lat_center), path=args.path,
                        direction=args.rotation)
