#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from tc_center.compare.flag_report import report_from_path
import owi
import sys
import os

import logging
logger = logging.getLogger(__name__)

if __name__ == "__main__":
    description = """
        Compare tc eye centers from cyclobs with other data source
        """
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("-s", "--data-source", action="store", required=True, default=None,
                        help="Path to folder where product are stored.")
    parser.add_argument("-p", "--path-save", action="store", required=True,
                        help="The path under which report will be saved")
    parser.add_argument("-f", "--filter-list", action="store", default=None, required=False,
                        help="List to use to filter files that are listed in the given file.")
    parser.add_argument("-e", "--exclude-list", action="store", default=None, required=False,
                        help="List to use to filter files that are not listed in the given file.")
    parser.add_argument("--debug", action="store_true", default=False,
                        help="Enable debug logging level.")

    args = parser.parse_args()

    report_from_path(args.data_source, args.path_save, args.filter_list, args.exclude_list)


