#!/usr/bin/env python

import argparse
import glob
import os
import xarray as xr
from tc_center.plotting import control_plot_from_shp, save_plot
from tc_center import track_data
import pandas as pd
import pathurl
import owi
from shapely.wkt import loads
import holoviews as hv
import datetime

import logging

logger = logging.getLogger(__name__)

if __name__ == "__main__":
    description = """
        Generate control plot from analysis NetCDF file.
        """
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("-p", "--path", action="store", required=True,
                        help="The base path where to find netcdfs")
    parser.add_argument("-o", "--outpath", action="store", required=True,
                        help="The base path where to save plot files")
    parser.add_argument("-b", "--backend", action="store", default="bokeh", help="Backend to use for plotting")
    parser.add_argument("-d", "--days", action="store", type=float, default=1,
                        help="Takes only files modified maximum n days ago. -1 means all")
    parser.add_argument("--debug", action="store_true", default=False,
                        help="Enable debug logging level.")

    args = parser.parse_args()

    if args.debug:
        logger.setLevel(logging.DEBUG)
        logging.basicConfig(level=logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
        logging.basicConfig(level=logging.INFO)

    logger.info(f"Processing files modified {args.days} day(s) ago.")

    files = glob.glob(os.path.join(args.path, "*.nc"))
    hv.extension(args.backend)

    now = datetime.datetime.now()

    for f in files:
        mod_epoch = os.path.getmtime(f)
        mod_datetime = datetime.datetime.fromtimestamp(mod_epoch)
        if args.days == -1 or (now - mod_datetime).total_seconds() < args.days * 24 * 3600:
            ds = xr.open_dataset(f)
            logger.info(f"Processing {f}...")
            source_sat = ds.attrs["Source satellite file"].replace("_sw", "_ll_gd")
            id_sat = "-".join(source_sat.split("-")[4:6])
            print(id_sat)
            api_req = f"https://cyclobs.ifremer.fr/app/api/getData?include_cols=all&filename={id_sat}"
            logger.info(f"API req : {api_req}")
            df = pd.read_csv(api_req)
            s_file = pathurl.toPath(df.iloc[0]["data_url"], schema="dmz")
            sat_date = owi.getDateFromFname(os.path.basename(s_file))
            track_gdf = track_data(ds.attrs["Storm ID"], sat_date)

            p = control_plot_from_shp(s_file, eye_shape=loads(ds["eye_shape"].item()),
                                      center_pt=loads(ds["eye_center"].item()),
                                      fg_pt=loads(ds["interpolated_track_point"].item()),
                                      hwind_pt=loads(ds["high_wind_area_barycenter"].item()),
                                      lwind_pt=loads(ds["low_wind_area_barycenter"].item()),
                                      hwind_research=loads(ds["high_wind_research"].item()),
                                      lwind_research=loads(ds["low_wind_research"].item()),
                                      atcf_vmax=ds["track_vmax"].item(),
                                      track_gdf=track_gdf,
                                      cyclone_name=ds.attrs["Storm name"],
                                      backend=args.backend
                                      )
            if args.backend == "matplotlib":
                p.opts(aspect=1, fig_inches=10, fig_bounds=(0, 0, 1, 1))
            save_plot(source_sat, args.outpath, p, prepend="control", backend=args.backend)
