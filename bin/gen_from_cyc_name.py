#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import pathurl
import owi
import sys
import os
import subprocess
import pandas as pd
from owi import lightToL2C


if __name__ == "__main__":
    description = """
        Starts eye center research from a cyclone name and a year
        A .dat fix containing the position found is saved under <path>/fix.
        A .nc fix containing the data reprojected on a polar grid is saved under <path>/product
        """
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("-c", "--cyclone_name", action="store", required=True,
                        help="Cyclone name")
    parser.add_argument("-y", "--year", action="store", required=True, help="Year of the target cyclone")
    parser.add_argument("-p", "--path", action="store", required=True,
                        help="The base path under which save the generated files (fix, control plot)")
    parser.add_argument("-r", "--resolution", action="store", default=1,
                        help="Specify resolution of SAR file to use to find tc center.")
    parser.add_argument("--debug", action="store_true", default=False,
                        help="Enable debug logging level.")

    args = parser.parse_args()

    api_req = f"https://cyclobs.ifremer.fr/app/api/getData?include_cols=all&cyclone_name={args.cyclone_name}&" \
              f"acquisition_start_time={args.year}-01-01&acquisition_stop_time={args.year}-12-31&instrument=C-Band_SAR"

    df = pd.read_csv(api_req, keep_default_na=False)

    for index, row in df.iterrows():
        data_path = lightToL2C(pathurl.toPath(row["data_url"], schema="dmz"))
        print(data_path)
        subprocess.run([f"gen_analysis_fix.py -p {args.path} -r {args.resolution} -f {data_path}"], shell=True)


