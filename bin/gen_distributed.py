#!/usr/bin/env python

# dask datarmor simple demo
# master is on 'ftp' queue, so this script can be converted to reachable python notebook
# workers are on 'sequentiel' queue, they don't have network access
#
# ssh datarmor
# qsub -I -q ftp -l walltime=01:00:00,mem=16g
# conda activate xxxx
# %./gen_distributed

from dask_jobqueue import PBSCluster
from dask.distributed import Client
import os
import socket
import fcntl
import struct
import argparse
import owi
import pandas as pd

from tc_center import find_center_save
from cyclobs_utils import track_data


def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', str.encode(ifname[:15]))
    )[20:24])


def init_cluster():
    # see https://jobqueue.dask.org/en/latest/generated/dask_jobqueue.PBSCluster.html
    memory = "6GB"
    nprocs = 1
    cluster = PBSCluster(
        cores=nprocs,
        memory=memory,
        project='tc_center',
        queue='sequentiel',
        processes=nprocs,
        resource_spec='select=1:ncpus=%d:mem=%s' % (nprocs, memory),
        local_directory=os.path.expandvars("$TMPDIR"),
        interface='ib1',  # workers interface (routable to queue ftp)
        walltime='00:30:00',
        scheduler_options={'interface': 'ib0'})  # if scheduler is on queue 'ftp'

    cluster.scale(jobs=340)  # ask for 200 jobs

    c = Client(cluster)
    # getting a working dashboard link is little tricky on datarmor
    print("Client dashboard: %s" % c.dashboard_link.replace(get_ip_address('ib0'), get_ip_address('bond1')))
    return c


def api_req(track_source="atcf"):
    req = f"https://cyclobs.ifremer.fr/app/api/getData?include_cols=all&track_source={track_source}"
    print(f"Getting data from Cyclobs API. Req : {api_req}")

    df = pd.read_csv(req, keep_default_na=False)

    return df


if __name__ == "__main__":
    description = """
        Start center plot generation using dask.distributed.
        """
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("-p", "--path-save", action="store", required=True)
    parser.add_argument("-pf", "--path-fix", action="store", default=None)

    args = parser.parse_args()

    if args.path_fix is None:
        path_fix = os.path.join(args.path_save, "fix")
        os.makedirs(path_fix, exist_ok=True)
    else:
        path_fix = args.path_fix

    report_path = os.path.join(args.path_save, "report")
    os.makedirs(report_path, exist_ok=True)

    path = "/home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/reports/shoc_dailyupdate_names"

    reportSuffix = "sarwingL2X/ConcatprocessOk.txt"
    reportFile = os.path.join(path, reportSuffix)

    netcdf_path = os.path.join(args.path_save, "product")
    os.makedirs(netcdf_path, exist_ok=True)

    sat_files = []
    api_data = []
    api_ibtracs_data = []
    track_gdfs = []
    path_save = []
    netcdf_path_save = []
    path_fixes = []
    res = []

    df = api_req()
    df["filename"] = df["data_url"].apply(lambda x: os.path.basename(x))

    df_ibtracs = api_req(track_source="ibtracs")
    df_ibtracs["filename"] = df_ibtracs["data_url"].apply(lambda x: os.path.basename(x))

    if os.path.isfile(reportFile):
        # For each path, read .txt that reference ncfiles
        with open(reportFile) as ncReport:
            files = ncReport.readlines()

            for f in files:
                f = f.rstrip('\r\n')
                ll_file = os.path.basename(owi.L2CtoLight(f, resolution=3, mode="ll_gd"))
                if len(df.loc[df["filename"] == ll_file].index) == 0:
                    continue
                api_data.append(df.loc[df["filename"] == ll_file])
                api_ibtracs_data.append(df_ibtracs.loc[df_ibtracs["filename"] == ll_file])
                sat_date = owi.getDateFromFname(os.path.basename(ll_file))
                track_gdf = track_data(api_data[-1].iloc[0]["sid"], sat_date, freq=15, days_after=2, days_before=1)
                track_gdfs.append(track_gdf)
                sat_files.append(f)
                path_save.append(args.path_save)
                path_fixes.append(path_fix)
                netcdf_path_save.append(netcdf_path)
                res.append(3)

    print(f"Number of file to process : {len(sat_files)}")
    client = init_cluster()

    #find_center_save(path_save[0], sat_files[0], res[0], path_fixes[0], netcdf_path_save[0], api_data[0],
    #                 api_ibtracs_data[0], track_gdfs[0])
    # get list of futures
    print("Starting...")
    outputs_futures = client.map(find_center_save, path_save, sat_files, res, path_fixes, netcdf_path_save,  api_data,
                                 api_ibtracs_data, track_gdfs)

    # gather futures on workers
    outputs = client.gather(outputs_futures)

    print("Saving output status...")
    for i in range(len(outputs)):
        base_file = os.path.basename(sat_files[i])
        with open(os.path.join(report_path, f"{base_file}.status"), 'w') as f_status:
            f_status.write(str(outputs[i]))
    print("Finished save output status.")

    print('outputs: %s' % outputs)
