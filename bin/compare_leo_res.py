#!/usr/bin/env python

import glob
import argparse
import os
import atcf
from shapely.geometry import Point
import csv

def retrieve_from_report(path):
    status_files = glob.glob(os.path.join(path, "report", "*.status"))
    files_status = {}
    files_eye_lon = {}
    files_eye_lat = {}
    for file_path in status_files:
        with open(file_path, mode="r") as f:
            sar_file = file_path.replace(".status", "")
            sar_file = "-".join(sar_file.split("-")[4:8]).replace(".nc", "")
            sar_id = sar_file
            sar_file = sar_file.replace("t", "T")
            status = f.readline().rstrip()
            if int(status) == 0:
                file_fix = glob.glob(os.path.join(os.path.join(path, "fix", f"*{sar_id}*.dat")))
                df = atcf.read(file_fix[0])
                files_eye_lon[sar_file] = float(df.iloc[0]["lon"])
                files_eye_lat[sar_file] = float(df.iloc[0]["lat"])
            files_status[sar_file] = int(status)

    return files_status, files_eye_lon, files_eye_lat


def retrieve_from_leo(leo_path):
    sar_infos = []
    eye_lons = []
    eye_lats = []
    files_eye_pos = glob.glob(os.path.join(leo_path, "**", "lon_lat_eye_centered*.txt"), recursive=True)
    for fname in files_eye_pos:
        # lon_lat_eye_S1A_IW_OWIH_CM_20170907T102951_20170907T103127_018268_01EB76.txt
        sar_info = ("-".join(os.path.basename(fname).split("_")[8:12])).replace(".txt", "")
        sar_infos.append(sar_info)
        with open(fname, mode="r") as file:
            lon_lat = file.readline().rstrip()
            eye_lons.append(round(float(lon_lat.split(",")[0].strip()), 1))
            eye_lats.append(round(float(lon_lat.split(",")[1].strip()), 1))

        #line_split_sar = line.split("|")[0].rstrip()
        #line_split_sar = line_split_sar.replace("_", "-")
        #line_split_sar = "-".join(line_split_sar.split("-")[4:8])
        #eye_lons.append(float(line.split("|")[2].strip().split("=")[1]))
        #eye_lats.append(float(line.split("|")[3].strip().split("=")[1]))

    return sar_infos, eye_lons, eye_lats


def compare(report_path, leo_path):
    files_status, file_lons, file_lats = retrieve_from_report(report_path)
    leo_sar, eye_lons, eye_lats = retrieve_from_leo(leo_path)

    count_leo_ok = 0
    count_leo_nok = 0
    count_leo_nok_status = 0
    ok_files = []
    nok_files = []
    nok_files_status = []
    lons_leo = []
    lats_leo = []
    lons = []
    lats = []
    distances = []
    i = 0
    for leo_file in leo_sar:
        if leo_file in files_status and int(files_status[leo_file]) == 0:
            count_leo_ok += 1
            ok_files.append(leo_file)
            lons_leo.append(eye_lons[i])
            lats_leo.append(eye_lats[i])
            leo_eye = Point(eye_lons[i], eye_lats[i])
            lons.append(file_lons[leo_file])
            lats.append(file_lats[leo_file])
            eye = Point(file_lons[leo_file], file_lats[leo_file])
            distances.append(leo_eye.distance(eye))
        elif leo_file in files_status:
            count_leo_nok_status += 1
            nok_files_status.append(leo_file)
        else:
            count_leo_nok += 1
            nok_files.append(leo_file)
        i += 1

    count_report_ok = 0
    count_report_nok = 0
    count_report_nok_status = 0
    nok_report_files = []
    nok_report_files_status = []
    for report_file in files_status:
        if int(files_status[report_file]) == 0 and report_file in leo_sar:
            count_report_ok += 1
        elif report_file in leo_sar:
            count_report_nok_status += 1
            nok_report_files_status.append(report_file)
        else:
            count_report_nok += 1
            nok_report_files.append(report_file)

    print(f"{count_leo_ok} / {len(leo_sar)} OK files comparing to leo files")
    print(f"{count_leo_nok_status} / {len(leo_sar)} STATUS NOK files comparing to leo files")
    print(f"{count_leo_nok} / {len(leo_sar)} MISSING NOT OK files comparing to leo files")
    print(f"{count_report_ok} / {len(files_status)} OK files comparing to report files")
    print(f"{count_report_nok_status} / {len(files_status)} STATUS NOK files comparing to report files")
    print(f"{count_report_nok} / {len(files_status)} MISSING NOT OK files comparing to report files")

    with open("leo_ok.csv", mode="w", newline='') as f:
        fieldnames = ["file", "leo_lon", "leo_lat", "lon", "lat", "distance"]
        csvw = csv.DictWriter(f, fieldnames=fieldnames, delimiter=',')
        csvw.writeheader()
        i = 0
        for ok_file in ok_files:
            csvw.writerow({"file": ok_file, "leo_lon": lons_leo[i], "leo_lat": lats_leo[i], "lon": lons[i],
                           "lat": lats[i], "distance": distances[i]})
            #f.write(f"{ok_file} leo_pos: {lons_leo[i]}/{lats_leo[i]} found_pos: {lons[i]}/{lats[i]} distance: {distances[i]}\n")
            i += 1

    with open("leo_nok_missing.txt", mode="w") as f:
        for nok_file in nok_files:
            f.write(f"{nok_file}\n")

    with open("leo_nok_status.txt", mode="w") as f:
        for nok_file in nok_files_status:
            f.write(f"{nok_file} {files_status[nok_file]}\n")

    with open("report_nok_missing.txt", mode="w") as f:
        for nok_file in nok_report_files:
            f.write(f"{nok_file}\n")

    with open("report_nok_status.txt", mode="w") as f:
        for nok_file in nok_report_files_status:
            f.write(f"{nok_file} {files_status[nok_file]}\n")


if __name__ == "__main__":
    description = """
        
        """
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("-p", "--leo_path", action="store",
                        default="/home/datawork-cersat-public/project/shoc/SAR_processing_leo/mean_div_signal_for_fft_allfiles_3km")
    parser.add_argument("-r", "--report_path", action="store", required=True)
    args = parser.parse_args()

    compare(args.report_path, args.leo_path)

