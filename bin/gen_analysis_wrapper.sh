#!/bin/bash

# A wrapper to write stdout to log file and to write the exit status to file too.

outDir=$1
owiFile=$2
reportDir=$3
api_url=$4

owibase=$(basename "$owiFile")
mkdir -p "$reportDir"
mkdir -p "$reportDir/logs"
mkdir -p "$reportDir/status"

gen_analysis_fix.py -p "$outDir" -f "$owiFile" -r 3 --api-url "$api_url" > "$reportDir/logs/$owibase.log" 2>&1
status=$?
echo $status > "$reportDir/status/$owibase.status"

cat "$reportDir/logs/$owibase.log"

exit $status
