## Tropical Storm analysis package

This package uses satellite cyclone acquisitions to compute additionnal
information about the cyclone.

These information are currently limited to:
- The tropical storm eye center
- The 34, 50, 64 wind speed (knots) radius and the vmax, rmax.
- Quality flags trying to estimate the quality of the found eye center

Computed data are saved as a FIX (.dat) and in netCDF files also
containing the satellite acquisition data in 4 configurations
(centered on the found center). 2 netcdf files are in polar grid, the 2 other on a cartesian grid. 
And for each grid type, one file contains data north oriented and the other TC oriented.

## Usage

The important scripts are gen_analysis_fix.py, product_from_center.py and eventually 
gen_all_analysis.sh

To run, gen_analysis_fix.py (and gen_all_analysis.sh) will need access to IFREMER paths 
(example :/home/datawork-cersat-public/cache/public/ftp/project/sarwing). Because it will load files
from there.

product_from_center.py can generate NetCDF et FIX products from given information
(center, cyclone direction). It does work outside of IFREMER. But it does not find the TC center.


## Build

    conda build . -c conda-forge -c tcevaer


## Installation
```
    #Create conda env 
    conda create -p <env_path> python=3.7 mamba -c conda-forge -c tcevaer

    # Install for usage.
    conda install tc_center -c tcevaer -c conda-forge
    
    # Install for usage, with mamba for fast install
    mamba install tc_center -c tcevaer -c conda-forge
    
    # To install from source. This will not install dependencies.
    git clone https://gitlab.ifremer.fr/cyclobs/tc_center
    cd tc_center
    pip install -e .
    
    # To install from source, with dependencies
    # Activate conda env
    conda install tc_center -c tcevaer -c conda-forge
    git clone https://gitlab.ifremer.fr/cyclobs/tc_center
    cd tc_center
    pip install -e .
```

## License

This project is licensed under the terms of the GNU Affero General Public License v3.0. See the [LICENSE](./LICENSE) file for details.
